﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;

namespace FoodDelivery.Service.Services
{
    public class ScoreService : IScoreService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ScoreService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<Score> CreateScoreAsync(Score newScore)
        {
         
            await _unitOfWork.Scores.AddAsync(newScore);
            await _unitOfWork.CommitAsync();
            return newScore;

           
        }
        public async Task CreateNewScoreAsync(int senderId, int receiverId, int orderId, Score newScore)
        {
            newScore.SenderId = senderId;
            newScore.OrderId = orderId;
            newScore.ReceiverId = receiverId;

            await _unitOfWork.Scores.AddAsync(newScore);

            await _unitOfWork.CommitAsync();
        }
        
        public async Task<Score> UpdateScoreAsync(Score scoreToBeUpdated, IScoreChanges scoreChanges)
        {
            scoreChanges.ApplyChanges(scoreToBeUpdated);

            await _unitOfWork.CommitAsync();

            return scoreToBeUpdated;
        }

        public async Task DeleteScoreAsync(Score score)
        {
            await _unitOfWork.Scores.Remove(score);

            await _unitOfWork.CommitAsync();
        }

        public Task<List<Score>> GetAllScoresAsync()
        {
            return  _unitOfWork.Scores.GetAllWithUsersAsync();
        }
        public Task<Score> GetScoreByIdAsync(int senderId, int receiverId, int orderId)
        {
            return  _unitOfWork.Scores.GetScoreByIdAsync(senderId, receiverId, orderId);
        }

        public async Task<IEnumerable<Score>> GetAllSentScoresBySenderId(int senderId)
        {
            return await _unitOfWork.Scores.GetAllSentScoresByUserIdAsync(senderId);
        }

        public async Task<IEnumerable<Score>>  GetAllRecievedScoresByRecieverId(int receiverId)
        {
            return await _unitOfWork.Scores.GetAllReceivedScoresByUserIdAsync(receiverId);
        }

        public Task<double> GetAverageScoreByUserIdAsync(int userId)
        {
            return _unitOfWork.Scores
                    .GetAverageScoreByUserIdAsync(userId);
        }

    }
}
