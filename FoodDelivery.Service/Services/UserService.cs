﻿using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Helpers;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using FoodDelivery.Core.PasswordHashing;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Service.Services
{
 
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IImageService _imageService;
        private readonly IPasswordHasher _passwordHasher;
        public UserService(IUnitOfWork unitOfWork, IImageService imageService, IPasswordHasher passwordHasher)
        { 
            _unitOfWork = unitOfWork;
            _imageService = imageService;
            _passwordHasher = passwordHasher;
        }

        public async Task<User> CreateUserAsync(User newUser, IFormFile picture)
        {
            newUser.Picture = await _imageService.UploadImageAsync(picture, StorageEnum.user);

            newUser.Password = _passwordHasher.Hash(newUser.Password);
            await _unitOfWork.Users.AddAsync(newUser);
            await _unitOfWork.CommitAsync();

            return newUser;
         }
        
        public async Task<User> UpdateUserAsync(User userToBeUpdated, IUserChanges userChanges)
        {
            
            await userChanges.ApplyChanges(userToBeUpdated, _imageService, _passwordHasher);

            if (userToBeUpdated.CityId != null)
            {
                var city = await new CityService(_unitOfWork).GetCityByIdAsync(userToBeUpdated.CityId);

                if (city == null)
                {
                    throw new InvalidCityException();
                }
            }

            await _unitOfWork.CommitAsync();

            return userToBeUpdated;
        }

        public async Task DeleteUserAsync(User user)
        {
            await _imageService.DeleteFileAsync(user.Picture);
            await _unitOfWork.Users.Remove(user);
            await _unitOfWork.CommitAsync();
        }

        public async Task<User> GetUserByIdAsync(int id) 
        {
            return await _unitOfWork.Users.GetByIdWithRoleAsync(id);
        }
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _unitOfWork.Users.GetAllWithRoleAsync();
        }
        public Task<bool> IsDeliverBusy(int deliverId)
        {
            return _unitOfWork.Users
                   .IsDeliverBusy(deliverId);
        }
       
        public  async Task<User> Authenticate(User userToAuthorize, IPasswordHasher passwordHasher)
        {
            var user = await _unitOfWork.Users
                              .SingleOrDefaultAsync(m => m.Email == userToAuthorize.Email);

            if (user == null || passwordHasher.Check(user.Password, userToAuthorize.Password).Verified != true)
                return null;



            var now = DateTime.UtcNow;
            

            //Creating jwt-token
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: GetClaims(user).Claims,
                    expires: now.Add(TimeSpan.FromHours(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            user.Token = encodedJwt;

            return user;
        }

        public ClaimsIdentity GetClaims(User user)
        {
            var claims = new List<Claim>
                {
                    new Claim("Id", user.Id.ToString()),
                    new Claim("Email", user.Email),
                    new Claim("Role", user.RoleId.ToString()),
                    new Claim("Name", user.Name.ToString()),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.RoleId.ToString())
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }
    }

 }
