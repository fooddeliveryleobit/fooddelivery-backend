﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Geography;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;


namespace FoodDelivery.Service.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<Order> CreateOrderAsync(Order order)
        {

            var newOrder = await _unitOfWork.Orders.AddAsync(order);

            await _unitOfWork.CommitAsync();

            return newOrder;
        }
        public async Task DeleteOrderAsync(Order order)
        {

            await _unitOfWork.Orders.Remove(order);

            await _unitOfWork.CommitAsync();
        }
        public async Task<IEnumerable<Order>> GetAllOrdersAsync()
        {
            return await _unitOfWork.Orders.GetAllOrdersAsync();
        }
        public Task<Order> GetOrderByIdAsync(int orderId)
        {

            return _unitOfWork.Orders
                    .GetOrderWithAdditionalInfoByIdAsync(orderId);
        }

        public async Task<Order> UpdateOrderAsync(Order orderToBeUpdated, IOrderChanges orderChanges)
        {

            orderChanges.ApplyChanges(orderToBeUpdated);

            await _unitOfWork.CommitAsync();

            return orderToBeUpdated;
        }

        public async Task<IEnumerable<OrderedDish>> GetAllOrderedDishesAsync(int orderId)
        {
            return await _unitOfWork.OrderedDishes
                          .GetAllOrderedDishesByOrderIdAsync(orderId);
        }

        #region For customer
        public async Task<Order> CreateNewOrderByCustomer(int customerId, int restaurantId, IEnumerable<OrderedDish> listOfDishes)
        {
            Order newOrder = new Order() { CustomerId = customerId, RestaurantId = restaurantId, Date = DateTime.Now };

            var customer = await _unitOfWork.Users.GetByIdAsync(customerId);
            var restaurant = await _unitOfWork.Restaurants.GetByIdAsync(restaurantId);

            if (Distance.FindDistanceMetres(customer.Location, restaurant.Location) > ValidatorProperties.MaxDistanceMeters)
            {
                throw new InvalidRoleException("Restaurant is too far !");
            }

            var order = await CreateOrderAsync(newOrder);

            var orderedDishesService = new OrderedDishService(_unitOfWork);

            var orderedDishesList = new List<OrderedDish>();

            if ((listOfDishes as List<OrderedDish>).Count == 0)
            {
                throw new InvalidOrderException("You havent't chosen anything !");
            }

            (listOfDishes as List<OrderedDish>).ForEach(m => orderedDishesList.Add(new OrderedDish() { DishId = m.DishId, OrderId = newOrder.Id, Count = m.Count }));

            try
            {
                await orderedDishesService.CreateOrderedDishAsync(orderedDishesList);
            }
            catch
            {
                await DeleteOrderAsync(newOrder);
                throw;
            }

            return order;
        }
        public async Task DeclineOrderByCustomer(int customerId, int orderId)
        {
            var order = await _unitOfWork.Orders.GetByIdAsync(orderId);

            if (order == null)
            {
                throw new InvalidOrderException("Order doen't exist !");
            }

            if (order.CustomerId != customerId)
            {
                throw new InvalidUserException($"Wrong customer : id = {customerId}");
            }

            if (order.Status != OrderStatusEnum.New)
            {
                throw new InvalidUserException("You can't delete this order !");
            }

            await DeleteOrderAsync(order);
        }
        #endregion


        #region For Deliver
        public async Task<Order> AcceptedOrderByDeliver(int deliverId, int orderId)
        {
            var orderToUpdate = await _unitOfWork.Orders.GetOrderWithAdditionalInfoByIdAsync(orderId);

            if (orderToUpdate == null)
            {
                throw new InvalidOrderException();
            }

            if (orderToUpdate.Status != OrderStatusEnum.New)
            {
                throw new InvalidOrderException("Order has already been accepeted/finished !");
            }

            var deliver = await _unitOfWork.Users.GetByIdWithRoleAsync(deliverId);

            if (deliver == null)
            {
                throw new InvalidUserException();
            }

            if (deliver.RoleId != RoleVariantEnum.Deliver)
            {
                throw new InvalidRoleException(deliver.RoleId);
            }

            if (await _unitOfWork.Users.IsDeliverBusy(deliverId) == true)
            {
                throw new InvalidUserException($"Deliver is busy !");
            }

            if (Distance.FindDistanceMetres(deliver.Location, orderToUpdate.Customer.Location) > ValidatorProperties.MaxDistanceMeters)
            {
                throw new InvalidOrderException("You're too far from customer !");
            }

            IOrderChanges orderChanges = new OrderChanges(deliverId, OrderStatusEnum.AcceptedByDeliver);

            return await UpdateOrderAsync(orderToUpdate, orderChanges);
        }
        public async Task<Order> FinishedOrderByDeliver(int deliverId, int orderId)
        {
            var orderToFinished = await _unitOfWork.Orders.GetOrderWithAdditionalInfoByIdAsync(orderId);

            if (orderToFinished == null)
            {
                throw new InvalidOrderException();
            }

            if (orderToFinished.DeliverId != deliverId)
            {
                throw new InvalidUserException($"Wrong deliver : deliverId = {deliverId} ");
            }

            if (orderToFinished.Status != OrderStatusEnum.AcceptedByDeliver)
            {
                throw new InvalidOrderException("Wrong order status !");
            }

            IOrderChanges orderChanges = new OrderChanges(deliverId, OrderStatusEnum.Finished);

            return await UpdateOrderAsync(orderToFinished, orderChanges);
        }
        public async Task<IEnumerable<Order>> GetAllAvaliableOrdersToDeliverAsync(int userId)
        {
            return await _unitOfWork.Orders.GetAllAvaliableOrdersToDeliverAsync(userId);
        }
        #endregion


        public async Task<IEnumerable<Order>> GetAllUserOrdersByStatusAsync(int userId, RoleVariantEnum role, OrderStatusEnum dishStatusEnum)
        {
            return await _unitOfWork.Orders
                         .GetAllUserOrdersByStatusAsync(userId, role, dishStatusEnum);
        }

        public async Task<IEnumerable<Order>> GetAllOrdersByRestaurantIdAsync(int restaurantId, OrderStatusEnum status)
        {
            return await _unitOfWork.Orders
                         .GetAllOrdersByRestaurantIdAsync(restaurantId, status);
        }


        public Task<decimal> GetTotalOrderCost(int orderId)
        {
            return _unitOfWork.Orders.GetTotalOrderCost(orderId);
        }

        public Task<Order> GetOrderWithrestaurantById(int orderId)
        {
            return _unitOfWork.Orders.GetOrderWithRestaurantById(orderId);
        }

    }
}
