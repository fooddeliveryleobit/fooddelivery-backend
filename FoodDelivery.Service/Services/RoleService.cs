﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Models;
using FoodDelivery.Service.Interfaces;

namespace FoodDelivery.Service.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<Role> GetRoleByIdAsync(int id)
        {   
            return await _unitOfWork.Roles.GetByIdAsync((RoleVariantEnum)id);
        }
        public async Task<Role> CreateRoleAsync(Role newRole)
        {
            await _unitOfWork.Roles.AddAsync(newRole);
            await _unitOfWork.CommitAsync();

            return newRole;
        }
        public async Task DeleteRoleAsync(Role role)
        {
            await _unitOfWork.Roles.Remove(role);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Role>> GetAllRolesAsync()
        {    
            return await _unitOfWork.Roles.GetAllAsync();
        }
        
    }
}
