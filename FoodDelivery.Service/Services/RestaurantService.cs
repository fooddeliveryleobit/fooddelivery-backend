﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Service.Services
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IImageService _imageService;

        public RestaurantService(IUnitOfWork unitOfWork, IImageService imageService)
        {
            _unitOfWork = unitOfWork;
            _imageService = imageService;
        }

        public async Task<Restaurant> CreateRestaurantAsync(int ownerId, Restaurant newRestaurant, IFormFile picture)
        {
            var owner = await _unitOfWork.Users.GetByIdAsync(ownerId);

            if (owner == null)
            {
                throw new InvalidUserException();
            }

            if (owner.RoleId != RoleVariantEnum.Owner)
            {
                throw new InvalidRoleException(owner.RoleId);
            }


            newRestaurant.Picture = await _imageService.UploadImageAsync(picture, StorageEnum.restaurant);

            newRestaurant.OwnerId = ownerId;


            await _unitOfWork.Restaurants.AddAsync(newRestaurant);
            await _unitOfWork.CommitAsync();

            return newRestaurant;
        }
        public async Task<Restaurant> UpdateRestaurantAsync(int restaurantId, IRestaurantChanges restaurantChanges)
        {
            var restaurantToBeUpdated = await _unitOfWork.Restaurants.GetByIdAsync(restaurantId);

            if (restaurantToBeUpdated == null)
            {
                throw new InvalidRestaurantException();
            }

            await restaurantChanges.ApplyChanges(restaurantToBeUpdated, _imageService);

            var city = await new CityService(_unitOfWork).GetCityByIdAsync(restaurantToBeUpdated.CityId);

            if (city == null)
            {
                throw new InvalidCityException();
            }

            await _unitOfWork.CommitAsync();

            return restaurantToBeUpdated;
        }
        public async Task DeleteRestaurantAsync(int restaurantId)
        {
            var restaurant = await _unitOfWork.Restaurants.GetByIdAsync(restaurantId);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException();
            }

            await _imageService.DeleteFileAsync(restaurant.Picture);
            await _unitOfWork.Restaurants.Remove(restaurant);
            await _unitOfWork.CommitAsync();
        }

        public async Task<Restaurant> GetRestaurantByIdAsync(int restaurantId)
        {
            return await _unitOfWork.Restaurants.GetByIdWithOwnerAsync(restaurantId);
        }

        public async Task<IEnumerable<Restaurant>> GetAllRestaurantsByCityIdAsync(string cityId)
        {
            return await _unitOfWork.Restaurants.GetAllRestaurantsByCityIdAsync(cityId);
        }

        public async Task<IEnumerable<Restaurant>> GetAllRestaurantsByOwnerIdAsync(int ownerId)
        {
            return await _unitOfWork.Restaurants.GetAllRestaurantsByOwnerIdAsync(ownerId);
        }

        public async Task<IEnumerable<Restaurant>> GetAllAvaliableRestaurantsToCustomer(int customerId)
        {
            return await _unitOfWork.Restaurants.GetAllAvaliableRestaurantsToCustomer(customerId);
        }


        public async Task<IEnumerable<Restaurant>> GetAllRestaurantsAsync()
        {
            return await _unitOfWork.Restaurants.GetAllAsync();
        }
    }
}
