﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;


namespace FoodDelivery.Service.Services
{
    public class DishCategoryService : IDishCategoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DishCategoryService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<DishCategory> CreateDishCategoryAsync(int restaurantId, DishCategory newDishCategory)
        {
            newDishCategory.IdRestaurant = restaurantId;

            await _unitOfWork.DishCategories.AddAsync(newDishCategory);
            await _unitOfWork.CommitAsync();

            return newDishCategory;
        }
        
        public async Task<DishCategory> UpdateDishCategoryAsync(int dishCategoryId, IDishCategoryChanges dishCategoryChanges)
        {
            var dishCategory = await _unitOfWork.DishCategories.GetByIdAsync(dishCategoryId);

            if (dishCategory == null)
            {
                throw new InvalidDishCategoryException();
            }

            dishCategoryChanges.ApplyChanges(dishCategory);

            await _unitOfWork.CommitAsync();

            return dishCategory;
        }

        public async Task DeleteDishCategoryAsync(int dishCategoryId)
        {
            var dishCategoryToDelete = await _unitOfWork.DishCategories.GetByIdAsync(dishCategoryId);

            if (dishCategoryToDelete == null)
            {
                throw new InvalidDishCategoryException();
            }

            await _unitOfWork.DishCategories.Remove(dishCategoryToDelete);
            await _unitOfWork.CommitAsync();
        }
      
        public Task<DishCategory> GetDishCategoryWithRestaurantByIdAsync(int categoryId)
        {
            return  _unitOfWork.DishCategories
                    .GetByIdWithRestaurantAsync(categoryId);
        }
        public async Task<IEnumerable<DishCategory>> GetAllDishCategoriesAsync(int restaurantId)
        {
            return await _unitOfWork.DishCategories.
                          GetAllDishCategoryByRestaurantId(restaurantId);
        }
       

    }
}
