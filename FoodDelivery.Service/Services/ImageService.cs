﻿using FoodDelivery.Core.Helpers;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;

namespace FoodDelivery.Service.Services
{

    public class ImageService : IImageService
    {

        private readonly string _containerPath = "food-delivery-pictures";
        private readonly string _restaurantsPath = "restaurants/";
        private readonly string _dishesPath = "dishes/";
        private readonly string _usersPath = "users/";
        private readonly CloudBlobContainer _cloudBlobContainer; 
        private readonly CloudBlobClient _cloudBlobClient;
        private readonly CloudStorageAccount _cloudStorageAccount;


        public ImageService()
        {
            _cloudStorageAccount = ConnectionString.GetConnectionString();
            _cloudBlobClient = _cloudStorageAccount.CreateCloudBlobClient();
            _cloudBlobContainer = _cloudBlobClient.GetContainerReference(_containerPath);
        }
        private string GetPath(StorageEnum storageEnum)
        {
            switch (storageEnum)
            {
                case StorageEnum.restaurant:
                        return _restaurantsPath;

                case StorageEnum.dish:
                        return _dishesPath;

                case StorageEnum.user:
                        return _usersPath;

                default:
                        throw new InvalidEnumArgumentException();

            }
        }

        public async Task<string> UploadImageAsync(IFormFile imageToUpload, StorageEnum storageEnum)
        {
            string path = GetPath(storageEnum);


            if (await _cloudBlobContainer.CreateIfNotExistsAsync())
            {
                await _cloudBlobContainer.SetPermissionsAsync(
                    new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    });
            }
            string imageName = Guid.NewGuid().ToString() + "-" + Path.GetExtension(imageToUpload.FileName);

            CloudBlockBlob cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(path + imageName);
            cloudBlockBlob.Properties.ContentType = imageToUpload.ContentType;
            await cloudBlockBlob.UploadFromStreamAsync(imageToUpload.OpenReadStream());

            var imageFullPath =  cloudBlockBlob.Uri.ToString();


            return imageFullPath;
        }


        public Task DeleteFileAsync(string fullPath)
        {
            int indexOfContainerPath = fullPath.IndexOf(_containerPath);

            if (indexOfContainerPath == -1)
                return Task.CompletedTask;

            int startIndex = indexOfContainerPath + _containerPath.Length+1;

            var relativePath = fullPath.Substring(startIndex, fullPath.Length - startIndex);


            var blob = _cloudBlobContainer.GetBlockBlobReference(relativePath);

            blob.DeleteIfExistsAsync();

            return Task.CompletedTask;
        }

        public Task<string> UpdateImageAsync(IFormFile imageToUpload, StorageEnum storageEnum, string oldFileStringPath)
        {
            DeleteFileAsync(oldFileStringPath);
            return UploadImageAsync(imageToUpload, storageEnum);
        }
    }
}
