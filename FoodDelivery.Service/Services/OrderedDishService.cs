﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;


namespace FoodDelivery.Service.Services
{
    public class OrderedDishService : IOrderedDishService
    {
        private readonly IUnitOfWork _unitOfWork;
        public OrderedDishService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;


        public async Task<IEnumerable<OrderedDish>> GetAllOrderedDishesByOrderIdAsync(int orderId)
        {
            return await _unitOfWork.OrderedDishes.GetAllOrderedDishesByOrderIdAsync(orderId);
        }
        public async Task<IEnumerable<OrderedDish>> CreateOrderedDishAsync(IEnumerable<OrderedDish> newOrderedDishes)
        {
            foreach (var val in newOrderedDishes)
            {
                await _unitOfWork.OrderedDishes.AddAsync(val);
                await _unitOfWork.CommitAsync();
            }

            return newOrderedDishes;
        }
    }
}
