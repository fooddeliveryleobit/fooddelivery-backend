﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Interfaces;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Service.Services
{
    public class DishService : IDishService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IImageService _imageService;
        public DishService(IUnitOfWork unitOfWork, IImageService imageService)
        {
            _unitOfWork = unitOfWork;
            _imageService = imageService;
        }

        public async Task<Dish> CreateDishAsync(int dishCategoryId, Dish newDish, IFormFile picture)
        {
            newDish.Picture = await _imageService.UploadImageAsync(picture, StorageEnum.dish);
            newDish.IdCategory = dishCategoryId;

            await _unitOfWork.Dishes.AddAsync(newDish);
            await _unitOfWork.CommitAsync();

            return newDish;
        }

        public async Task<Dish> UpdateDishAsync(int dishId, IDishChanges dishChanges)
        {
            var dish = await _unitOfWork.Dishes.GetByIdAsync(dishId);

            if (dish == null)
            {
                throw new InvalidDishCategoryException();
            }

            await dishChanges.ApplyChanges(dish, _imageService);
            await _unitOfWork.CommitAsync();

            return dish;
        }

        public async Task DeleteDishAsync(int dishId)
        {
            var dish = await _unitOfWork.Dishes.GetByIdAsync(dishId);

            if (dish == null)
            {
                throw new InvalidDishCategoryException();
            }


            await _imageService.DeleteFileAsync(dish.Picture);
            await _unitOfWork.Dishes.Remove(dish);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Dish>> GetAllDishesAsync()
        {
            return await _unitOfWork.Dishes.GetAllWithDishCategoryAsync();
        }

        public async Task<Dish> GetDishByIdAsync(int id)
        {
            return await _unitOfWork.Dishes.GetByIdWithCategoryAndRestaurantAsync(id);
        }

        public async Task<IEnumerable<Dish>> GetAllDishesByCategoryIdAsync(int categoryId)
        {
            return await _unitOfWork.Dishes
                         .GetAllDishesByCategoryIdAsync(categoryId);
        }

        public async Task<Dish> GetByIdWithCategoryAndRestaurantAsync(int id)
        {
            return await _unitOfWork.Dishes
                         .GetByIdWithCategoryAndRestaurantAsync(id);
        }
    }
}
