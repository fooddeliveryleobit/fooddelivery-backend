﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;


namespace FoodDelivery.Service.Services
{
    public class CityService : ICityService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CityService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;
        public async Task DeleteCityAsync(string cityId)
        {
            var cityToDelete = await _unitOfWork.Cities.GetByIdAsync(cityId);

            if (cityToDelete == null)
            {
                throw new InvalidCityException();
            }

            await _unitOfWork.Cities.Remove(cityToDelete);
            await _unitOfWork.CommitAsync();
        }
        public async Task<IEnumerable<City>> CreateCityAsync(IEnumerable<City> newCities)
        {
            foreach (var newCity in newCities)
            {
                await _unitOfWork.Cities.AddAsync(newCity);
            }

            await _unitOfWork.CommitAsync();

            return newCities;
        }

        public async Task<City> GetCityByIdAsync(string cityId)
        {
            return await _unitOfWork.Cities.GetByIdAsync(cityId);
        }

        public async Task<IEnumerable<City>> GetAllCitiesAsync()
        {
            return await _unitOfWork.Cities.GetAllAsync();
        }

    }
}
