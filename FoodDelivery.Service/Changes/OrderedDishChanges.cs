﻿using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Service.Changes
{
    public class OrderedDishChanges : IOrderedDishChanges
    {
        public int Count { get; set; }

        public OrderedDish ApplyChanges(OrderedDish orderedDishToBeUpdated)
        {
            orderedDishToBeUpdated.Count = this.Count == default(int) ? orderedDishToBeUpdated.Count : this.Count;

            return orderedDishToBeUpdated;
        }
    }
}
