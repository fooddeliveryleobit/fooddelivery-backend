﻿using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.PasswordHashing;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Http;
using NetTopologySuite.Geometries;
using System.Threading.Tasks;

namespace FoodDelivery.Service.Changes
{
    public class UserChanges : IUserChanges
    {
        public string Name { get; set; }
        public string Password{ get; set; }
        public IFormFile Picture { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Phone { get; set; }
        public async Task<User> ApplyChanges(User userToBeUpdated, IImageService imageService, IPasswordHasher passwordHasher)
        {
            userToBeUpdated.Name = this.Name ?? userToBeUpdated.Name;

            userToBeUpdated.Password =  (this.Password != default(string)) 
                                        ? passwordHasher.Hash(this.Password)
                                        : userToBeUpdated.Password;

            userToBeUpdated.Picture = (this.Picture != default(IFormFile)) 
                                        ? await imageService.UpdateImageAsync(Picture, StorageEnum.user, userToBeUpdated.Picture)
                                        : userToBeUpdated.Picture;

            userToBeUpdated.Phone = this.Phone ?? userToBeUpdated.Phone;
            userToBeUpdated.CityId = this.CityId ?? userToBeUpdated.CityId;

            userToBeUpdated.Location = (this.Latitude != default && this.Longitude != default) ?
                                        new Point(Latitude, Longitude) { SRID = 4326 } : userToBeUpdated.Location;

            return userToBeUpdated;
        }
    }
}
