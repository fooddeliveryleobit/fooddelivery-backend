﻿using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Service.Changes
{
    public class DishCategoryChanges : IDishCategoryChanges
    {
        public string Name { get; set; }

        public DishCategory ApplyChanges(DishCategory dishCategoryToBeUpdated)
        {
            dishCategoryToBeUpdated.Name = this.Name ?? dishCategoryToBeUpdated.Name;
           
            return dishCategoryToBeUpdated;
        }
    }
}
