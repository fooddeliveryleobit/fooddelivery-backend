﻿using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Http;
using NetTopologySuite.Geometries;
using System.Threading.Tasks;

namespace FoodDelivery.Service.Changes
{
    public class RestaurantChanges : IRestaurantChanges
    {
        public string Name { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public IFormFile Picture { get; set; }


        public async Task<Restaurant> ApplyChanges(Restaurant restaurantToUpdate, IImageService imageService)
        {
            restaurantToUpdate.Name = this.Name ?? restaurantToUpdate.Name;
            restaurantToUpdate.CityId = this.CityId ?? restaurantToUpdate.CityId;
            restaurantToUpdate.Location = (this.Latitude != default(double) && this.Longitude != default(double)) ?
                                            new Point(Latitude, Longitude) { SRID = 4326 } : restaurantToUpdate.Location;


            restaurantToUpdate.Picture = this.Picture != default(IFormFile) ? 
                                                         await imageService.UpdateImageAsync(Picture, StorageEnum.restaurant, restaurantToUpdate.Picture)
                                                         : restaurantToUpdate.Picture;

            return restaurantToUpdate;
        }
    }
}
