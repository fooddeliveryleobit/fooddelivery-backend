﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Service.Changes
{
    public class ScoreChanges : IScoreChanges
    {
        public double Value { get; set; }

        public Score ApplyChanges(Score scoreToBeUpdated)
        {
            if (this.Value != default(double) && (this.Value < ValidatorProperties.MinScore || this.Value > ValidatorProperties.MaxScore))
            {
                throw new InvalidScoreException($"Wrong score value : {this.Value} !");
            }

            scoreToBeUpdated.Value = this.Value == default(double) ? scoreToBeUpdated.Value : this.Value;

            return scoreToBeUpdated;
        }
    }
}
