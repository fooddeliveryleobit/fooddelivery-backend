﻿using System;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Service.Changes
{
    public class OrderChanges : IOrderChanges
    {
        public int? DeliverId { get; set; }
        public OrderStatusEnum Status { get; set; }
        public OrderChanges(int DeliverId, OrderStatusEnum Status)
        {
            this.DeliverId = DeliverId;
            this.Status = Status;
        }

        public Order ApplyChanges(Order orderToBeUpdated)
        {
            orderToBeUpdated.DeliverId = (orderToBeUpdated.DeliverId.HasValue == true) ? orderToBeUpdated.DeliverId.Value : this.DeliverId.Value;

            if (!Enum.IsDefined(typeof(OrderStatusEnum), orderToBeUpdated.Status))
            {
                throw new InvalidOrderException("This type of status doesn't exist !");
            }

            orderToBeUpdated.Status =  this.Status == default ? orderToBeUpdated.Status : this.Status;

           return orderToBeUpdated;
        }
    }
}
