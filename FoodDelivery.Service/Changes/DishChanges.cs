﻿using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FoodDelivery.Service.Changes
{
    public class DishChanges : IDishChanges
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public IFormFile Picture { get; set; }
        public async Task<Dish> ApplyChanges(Dish DishToBeUpdated, IImageService imageService)
        {
            DishToBeUpdated.Name = this.Name ?? DishToBeUpdated.Name;
            DishToBeUpdated.Price = this.Price == default(decimal) ? DishToBeUpdated.Price : this.Price;
            DishToBeUpdated.Description = this.Description ?? DishToBeUpdated.Description;

            DishToBeUpdated.Picture = this.Picture != default(IFormFile) ?
                                                    await imageService.UpdateImageAsync(Picture, StorageEnum.dish, DishToBeUpdated.Picture)
                                                    : DishToBeUpdated.Picture;

            return DishToBeUpdated;
        }
    }
}
