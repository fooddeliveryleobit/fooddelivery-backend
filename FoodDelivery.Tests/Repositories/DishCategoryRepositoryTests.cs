﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests
{
    class DishCategoryComparer : IEqualityComparer<DishCategory>
    {
        public bool Equals(DishCategory x,  DishCategory y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(DishCategory obj)
        {
            return obj.GetHashCode();
        }
    }
    public class DishCategoryRepositoryTests
    {
        IDishCategoryRepository dishCategoryRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public DishCategoryRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);

            dishCategoryRepository = new DishCategoryRepository(foodDeliveryDbContext);
        }


       [Theory]
       [InlineData(111, "Juices")]
       public async void AddAsync_RestaurantDoesNotExist_ThrowInvalidRestaurantException(int restaurantId, string dishMenuName)
       {
            //Arrange
            DishCategory dishCategoryToAdd = new DishCategory() { IdRestaurant = restaurantId, Name = dishMenuName };

            //Assert
            await Assert.ThrowsAsync<InvalidRestaurantException>(() => dishCategoryRepository.AddAsync(dishCategoryToAdd));
       }

       [Theory]
       [InlineData(1, "Juices")]
       public async void AddAsync_ValidDishCategory_AllShouldPass(int restaurantId, string dishMenuName)
        {
            //Arrange
            DishCategory dishCategoryToAdd = new DishCategory() { IdRestaurant = restaurantId,  Name = dishMenuName};

            //Act
            await dishCategoryRepository.AddAsync(dishCategoryToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(dishCategoryToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);  
        }
   




       [Theory]
       [InlineData(1212)]
       public async void GetAllDishCategoryByRestaurantId_InvalidRestaurantId_ThrowInvalidRestaurantException(int restaurantId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRestaurantException>(() => dishCategoryRepository.GetAllDishCategoryByRestaurantId(restaurantId));

        }

       [Theory]
       [InlineData(4, 7,8)]
       public async void GetAllDishCategoryByRestaurantId_ValidRestaurant_AllShouldPass(int restaurantId, params int [] dishCategoryIds)
        {
            //Arrange
            var expected = new List<DishCategory>();
            foreach(var val in dishCategoryIds)
            {
                expected.Add(new DishCategory() { Id = val });
            }

            //Act
            var actual = await dishCategoryRepository.GetAllDishCategoryByRestaurantId(restaurantId);

            //Assert
            Assert.Equal(expected, actual, new DishCategoryComparer());
        }





       [Theory]
       [InlineData(5121)]
       public async void GetByIdWithRestaurantAsync_InvalidDishCategoryId_ThrowInvalidRestaurantException(int dishCategoryId)
       {
            //Assert
            await Assert.ThrowsAsync<InvalidDishCategoryException>(() => dishCategoryRepository.GetByIdWithRestaurantAsync(dishCategoryId));
       }


    }
}
