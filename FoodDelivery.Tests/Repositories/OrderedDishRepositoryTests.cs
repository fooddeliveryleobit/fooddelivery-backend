﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Xunit;

namespace FoodDelivery.Tests
{
    class OrderedDishComparer : EqualityComparer<OrderedDish>
    {
        public override bool Equals(OrderedDish x,  OrderedDish y)
        {
            return x.DishId == y.DishId && x.OrderId == y.OrderId;
        }

        public override int GetHashCode(OrderedDish obj)
        {
            return obj.GetHashCode();
        }
    }
    public class OrderedDishRepositoryTests
    {
        IOrderedDishRepository orderedDishRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public OrderedDishRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            orderedDishRepository = new OrderedDishRepository(foodDeliveryDbContext);
        }




        [Theory]
        [InlineData(-3, 13, 5)]
        public async void AddAsync_InvalidOrderId_ThrowInvalidOrderException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidOrderException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(3, 9, 5)]   //Accepted by customer
        [InlineData(9, 23, 5)]  //Finished
        public async void AddAsync_InvalidOrderIsNotNew_ThrowInvalidOrderException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidOrderException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(13, 121212, 3)]
        public async void AddAsync_DishDoesNotExist_ThrowInvalidDishException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidDishException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(10, 18, 3)]  //Restaurant = 6 (Dishes : [24..25])
        public async void AddAsync_DishDoesNotBelongToChosenRestaurant_ThrowInvalidDishException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidDishException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(18, 26, 5)]  //OrderedDishes : 6 Dishes(id = 26)
        public async void AddAsync_OrderedDishAlreadyAdded_ThrowInvalidOrderedDishException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidOrderedDishException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(11, 24, -5)]  //OrderedDishes : 2 Dishes(id = 25)
        [InlineData(30, 3, 100000)]  //OrderedDishes : 2 Dishes(id =  1) and 5 Dishes(id = 2)
        public async void AddAsync_WrongCountOfDishes_ThrowInvalidOrderedDishException(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Assert
            await Assert.ThrowsAsync<InvalidOrderedDishException>(() => orderedDishRepository.AddAsync(orderedDishToAdd));
        }

        [Theory]
        [InlineData(11, 24, 5)]  //OrderedDishes : 2 Dishes(id = 25)
        public async void AddAsync_ValidOrderedDish_AllShouldPass(int orderId, int dishId, int count)
        {
            //Arrange
            OrderedDish orderedDishToAdd = new OrderedDish() { OrderId = orderId, DishId = dishId, Count = count };

            //Act
            await orderedDishRepository.AddAsync(orderedDishToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(orderedDishToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }





        [Theory]
        [InlineData(2)]
        public async void GetAllOrderedDishesByOrderIdAsync_OrderedDishDoesNotExist_ThrowInvalidOrderException(int orderId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidOrderException>(() => orderedDishRepository.GetAllOrderedDishesByOrderIdAsync(orderId));
        }

        [Theory]
        [InlineData(6,   11, 13, 14)]
        public async void GetAllOrderedDishesByOrderIdAsync_ValidOrderId_AllShouldPass(int orderId, params int [] dishIds)
        {
            //Arrange
            var expected = new List<OrderedDish>();

            foreach(var val in dishIds)
            {
                expected.Add(new OrderedDish() { OrderId = orderId, DishId = val });
            }

            //Act
            var actual = await orderedDishRepository.GetAllOrderedDishesByOrderIdAsync(orderId);


            //Assert
            Assert.Equal(expected, actual, new OrderedDishComparer());
        }
    }
}
