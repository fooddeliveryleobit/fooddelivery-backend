﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Xunit;

namespace FoodDelivery.Tests
{
    class OrderComparer : IEqualityComparer<Order> 
    {
        public bool Equals(Order order1, Order order2)
        {
            return order1.Id == order2.Id;
        }

        public int GetHashCode(Order obj)
        {
            return obj.GetHashCode();
        }
    }
    public class OrderRepositoryTests
    {
        IOrderRepository orderRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public OrderRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                             .UseSqlServer(connection, x => x.UseNetTopologySuite())
                             .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            orderRepository = new OrderRepository(foodDeliveryDbContext); ;
        }



        [Theory]
        [InlineData(1000, 1)]
        public async void AddAsync_UserDoesNotExist_ThrowInvalidUserException(int userId, int restaurantId)
        {
            //Arrange
            Order orderToAdd = new Order() { CustomerId = userId, RestaurantId = restaurantId };

            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => orderRepository.AddAsync(orderToAdd));
        }

        [Theory]
        [InlineData(10, 1)]
        public async void AddAsync_UserIsNotCustomer_ThrowInvalidRoleException(int userId, int restaurantId)
        {
            //Arrange
            Order orderToAdd = new Order() { CustomerId = userId, RestaurantId = restaurantId };

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => orderRepository.AddAsync(orderToAdd));
        }
       
        [Theory]
        [InlineData(3, 2231)]
        public async void AddAsync_RestaurantDoesNotExist_ThrowInvalidRestaurantException(int userId, int restaurantId)
        {
            //Arrange
            Order orderToAdd = new Order() { CustomerId = userId, RestaurantId = restaurantId };

            //Assert
            await Assert.ThrowsAsync<InvalidRestaurantException>(() => orderRepository.AddAsync(orderToAdd));
        }

        [Theory]
        [InlineData(3, 1)]
        public async void AddAsync_ValidRestaurant_AllShouldPass(int userId, int restaurantId)
        {
            //Arrange
            Order orderToAdd = new Order() { CustomerId = userId, RestaurantId = restaurantId };

            //Act
            await orderRepository.AddAsync(orderToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(orderToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }




        [Theory]
        [InlineData(-1)]
        [InlineData(100)]
        public async void GetAllAvaliableOrdersToDeliverAsync_UserDoesNsotExist_ThrowInvalidUserException(int deliverId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => orderRepository.GetAllAvaliableOrdersToDeliverAsync(deliverId));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(14)]
        public async void GetAllAvaliableOrdersToDeliverAsync_UserIsNotDeliver_ThrowInvalidRoleException(int deliverId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => orderRepository.GetAllAvaliableOrdersToDeliverAsync(deliverId));
        }

        [Theory]
        [InlineData(6)]
        [InlineData(8)]
        public async void GetAllAvaliableOrdersToDeliverAsync_DeliverAlreadyHasAnOrder_ThrowInvalidUserException(int deliverId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => orderRepository.GetAllAvaliableOrdersToDeliverAsync(deliverId));
        }

        [Theory]
        [InlineData(10,   10, 11)]
        public async void GetAllAvaliableOrdersToDeliverAsync_ValidDeliver_AllShouldPass(int deliverId, params int[] orderIds)
        {
            //Arrange
            var expected = new List<Order>();
            
            foreach(var val in orderIds)
            {
                expected.Add(new Order() { Id = val });
            }
            
            //Act
            IEnumerable<Order> actual = await orderRepository.GetAllAvaliableOrdersToDeliverAsync(deliverId);


            //Assert
            Assert.Equal(expected, actual, new OrderComparer());
        }
   




        [Theory]
        [InlineData(-5, RoleVariantEnum.Customer, OrderStatusEnum.AcceptedByDeliver)]
        public async void GetAllUserOrdersByStatusAsync_UserDoesNotExist_ThrowInvalidUserException(int userId, RoleVariantEnum roleVariantEnum, OrderStatusEnum orderStatusEnum)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => orderRepository.GetAllUserOrdersByStatusAsync(userId, roleVariantEnum, orderStatusEnum));
        }

        [Theory]
        [InlineData(11, RoleVariantEnum.Owner, OrderStatusEnum.AcceptedByDeliver)]
        public async void GetAllUserOrdersByStatusAsync_UserIsNotCustomerOrDeliver_ThrowInvalidRoleException(int userId, RoleVariantEnum roleVariantEnum, OrderStatusEnum orderStatusEnum)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => orderRepository.GetAllUserOrdersByStatusAsync(userId, roleVariantEnum, orderStatusEnum));
        }

        [Theory]
        [InlineData(1, RoleVariantEnum.Deliver, OrderStatusEnum.AcceptedByDeliver)]
        public async void GetAllUserOrdersByStatusAsync_WrongUserRole_ThrowInvalidRoleException(int userId, RoleVariantEnum roleVariantEnum, OrderStatusEnum orderStatusEnum)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => orderRepository.GetAllUserOrdersByStatusAsync(userId, roleVariantEnum, orderStatusEnum));
        }

        [Theory]
        [InlineData(1, RoleVariantEnum.Customer, OrderStatusEnum.New,   13, 30)]
        [InlineData(8, RoleVariantEnum.Deliver, OrderStatusEnum.Finished,   5, 8)]
        public async void GetAllUserOrdersByStatusAsync_ValidData_AllShouldPass(int userId, RoleVariantEnum roleVariantEnum, OrderStatusEnum orderStatusEnum, params int[] orderIds)
        {
            //Arrange
            var expected = new List<Order>();

            foreach(var val in orderIds)
            {
                expected.Add(new Order() { Id = val });
            }

            //Act
            var actual = await orderRepository.GetAllUserOrdersByStatusAsync(userId, roleVariantEnum, orderStatusEnum);

            //Assert
            Assert.Equal(expected, actual, new OrderComparer());  
        }





        [Theory]
        [InlineData(1003, OrderStatusEnum.AcceptedByDeliver)]
        public async void GetAllOrdersByRestaurantIdAsync_RestaurantDoesNotExist_ThrowInvalidRestaurantException(int restaurantId, OrderStatusEnum orderStatus)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRestaurantException>(() => orderRepository.GetAllOrdersByRestaurantIdAsync(restaurantId, orderStatus));
        }

        [Theory]
        [InlineData(1, OrderStatusEnum.Finished)]
        public async void GetAllOrdersByRestaurantIdAsync_ValidCustomer_AllShouldPass(int restaurantId, OrderStatusEnum orderStatus)
        {
            //Act
            var actual = await orderRepository.GetAllOrdersByRestaurantIdAsync(restaurantId, orderStatus);

            //Assert
            Assert.Equal(new List<Order>(), actual);
        }





        [Theory]
        [InlineData(0)]
        [InlineData(1000)]
        public async void GetTotalCost_OrderDoesNotExist_ThrowInvalidOrderException(int orderId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidOrderException>(() => orderRepository.GetTotalOrderCost(orderId));
        }

        [Theory]
        [InlineData(1, 252)]
        [InlineData(3, 185)]
        public async void GetTotalCost_ValidOrder_AllShouldPass(int orderId, decimal orderCost)
        {
            //Act
            decimal actual = await orderRepository.GetTotalOrderCost(orderId);

            //Assert
            Assert.Equal(orderCost, actual);
        }

    }

}
