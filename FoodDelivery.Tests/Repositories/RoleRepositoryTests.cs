﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace FoodDelivery.Tests
{

    class RoleComparer : IEqualityComparer<Role>
    {
        public bool Equals(Role x,  Role y)
        {
            return x.Id == x.Id;
        }

        public int GetHashCode(Role obj)
        {
            return obj.GetHashCode();
        }
    }
    public class RoleRepositoryTests
    {
        IRoleRepository roleRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public RoleRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            roleRepository = new RoleRepository(foodDeliveryDbContext);
        }




        [Theory]
        [InlineData(100)]
        public async void AddAsync_InvalidRole_ThrowInvalidRoleException(int roleId)
        {
            //Arrange
            Role roleToAdd = new Role() { Id = (RoleVariantEnum)roleId };

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => roleRepository.AddAsync(roleToAdd));
        }

        [Theory]
        [InlineData(2)]
        public async void AddAsync_RoleIsAlreadyExisted_ThrowInvalidRoleException(int roleId)
        {
            //Arrange
            Role roleToAdd = new Role() { Id = (RoleVariantEnum)roleId };

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => roleRepository.AddAsync(roleToAdd));
        }

     /*   [Theory]
        [InlineData(4)]
        public async void AddAsync_ValidRole_AllShouldPass(int roleId)
        {
            //Arrange
            var roleToDelete = new Role() { Id = (RoleVariantEnum)roleId };

            //Actual
            await roleRepository.Remove(roleToDelete);

            //Assert
            await roleRepository.AddAsync(roleToDelete);
            EntityState actual = foodDeliveryDbContext.Entry(roleToDelete).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }
    */


        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async void Remove_CustomerOrDeliverRoles_AllShouldPass(int roleId)
        {
            //Arrange
            var roleToDelete = new Role() { Id = (RoleVariantEnum)roleId };

            //Actual
            await roleRepository.Remove(roleToDelete);

            //Assert
            var users = await  foodDeliveryDbContext.Users
                                .Where(m => m.RoleId == (RoleVariantEnum)roleId)
                                .ToListAsync();

            foreach(var user in users)
            {
                if (foodDeliveryDbContext.Entry(user).State != EntityState.Deleted)
                {
                    Assert.True(false);
                }

                var orders = await foodDeliveryDbContext.Orders
                                   .Where(m => m.DeliverId == user.Id || m.CustomerId == user.Id)
                                   .ToListAsync();

                foreach (var order in orders)
                {
                    if (foodDeliveryDbContext.Entry(order).State != EntityState.Deleted)
                    {
                        Assert.True(false);
                    }
                }
            }

            var actual =  foodDeliveryDbContext.Entry(roleToDelete).State;
            Assert.Equal(EntityState.Deleted, actual);
        }

        [Theory]
        [InlineData(3)]
        public async void Remove_RestaurantOwnerRole_AllShouldPass(int roleId)
        {
            //Arrange
            var roleToDelete = new Role() { Id = (RoleVariantEnum)roleId };

            //Actual
            await roleRepository.Remove(roleToDelete);

            //Assert
            var restaurantOwners = await foodDeliveryDbContext.Users
                                         .Where(m => m.RoleId == (RoleVariantEnum)roleId)
                                         .ToListAsync();


            foreach (var owner in restaurantOwners)
            {
                if (foodDeliveryDbContext.Entry(owner).State != EntityState.Deleted)
                {
                    Assert.True(false);
                }
            }

            var actual = foodDeliveryDbContext.Entry(roleToDelete).State;
            Assert.Equal(EntityState.Deleted, actual);
        }

    }
}
