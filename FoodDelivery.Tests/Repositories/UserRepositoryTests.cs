﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace FoodDelivery.Tests
{
    class UserComparer : IEqualityComparer<User>
    {
        public bool Equals(User x,  User y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(User obj)
        {
            return obj.GetHashCode();
        }
    }
    public class UserRepositoryTests
    {
        IUserRepository userRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public UserRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            userRepository = new UserRepository(foodDeliveryDbContext);
        }



        [Theory]
        [InlineData(-1)]
        [InlineData(50)]
        public async void GetByIdWithRoleAsync_UserDoesNotExist_ThrowInvalidUserException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => userRepository.GetByIdWithRoleAsync(userId));
        }



        [Theory]
        [InlineData(-3, "Marina", "Zp", "gmailgmail@gmail.com" )]
        public async void AddAsync_InvalidRoleId_ThrowInvalidRoleException(int roleId, string name, string cityId, string email)
        {
            //Arrange
            User userToAdd = new User() { RoleId = (RoleVariantEnum)roleId, Name = name, CityId = cityId, Email = email };

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => userRepository.AddAsync(userToAdd));
        }

        [Theory]
        [InlineData(1, "Marina", "Zp", "string1@gmail.com")]
        public async void AddAsync_EmailIsAlreadyUsed_ThrowInvalidUserException(int roleId, string name, string cityId, string email)
        {
            //Arrange
            User userToAdd = new User() { RoleId = (RoleVariantEnum)roleId, Name = name, CityId = cityId, Email = email };

            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => userRepository.AddAsync(userToAdd));
        }

        [Theory]
        [InlineData(1, "Marina", "Zpppppp", "stringggg1@gmail.com")]
        public async void AddAsync_InvalidCity_ThrowInvalidCityException(int roleId, string name, string cityId, string email)
        {
            //Arrange
            User userToAdd = new User() { RoleId = (RoleVariantEnum)roleId, Name = name, CityId = cityId, Email = email };

            //Assert
            await Assert.ThrowsAsync<InvalidCityException>(() => userRepository.AddAsync(userToAdd));
        }

        [Theory]
        [InlineData(1, "Marina", "Zp", "stringggg1@gmail.com")]
        public async void AddAsync_ValidUser_AllShouldPass(int roleId, string name, string cityId, string email)
        {
            //Arrange
            User userToAdd = new User() { RoleId = (RoleVariantEnum)roleId, Name = name, CityId = cityId, Email = email };

            //Act
            await userRepository.AddAsync(userToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(userToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }




        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async void IsDeliverBusy_UserIsNotDeliver_ThrowInvalidRoleException(int deliverIndex)
        {
            //Act
            Task<bool> actual() => userRepository.IsDeliverBusy(deliverIndex);

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(actual);
        }

        [Theory]
        [InlineData(2323)]
        public async void IsDeliverBusy_DelierDoesNotExist_ThrowInvalidUserException(int deliverIndex)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => userRepository.IsDeliverBusy(deliverIndex));
        }

        [Theory]
        [InlineData(10, false)]
        [InlineData(8, true)]
        public async void IsDeliverBusy_ValidDeliver_AllShouldPass(int deliverId, bool expected)
        {
            //Act
            bool actual = await userRepository.IsDeliverBusy(deliverId);

            //Assert
            Assert.Equal(expected, actual);
        }




        [Theory]
        [InlineData("string1@gmail.com", true)]
        [InlineData("gmaaaaaaail@gmail.com", false)]
        public async void IsEmailAlreadyUsed_ValidEmail_AllShouldPass(string email, bool expected)
        {
            //Act
            bool actual = await userRepository.IsEmailAlreadyUsed(email);

            //Assert
            Assert.Equal(expected, actual);
        }
    
    }
}

//[Fact]