﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Xunit;

namespace FoodDelivery.Tests
{
    class DishComparer : IEqualityComparer<Dish>
    {
        public bool Equals(Dish x,  Dish y)
        {
            return (x.Id == y.Id && x.IdCategory == y.IdCategory);
        }

        public int GetHashCode(Dish obj)
        {
            return obj.GetHashCode();
        }
    }
    public class DishRepositoryTests
    {
        IDishRepository dishRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public DishRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            dishRepository = new DishRepository(foodDeliveryDbContext);
        }



        [Theory]
        [InlineData(-4, "Coca-cola", 20, "...")]
        public async void AddAsync_InvalidDishCategoryId_ThrowInvalidDishCategoryException(int dishCategoryId, string name, decimal price, string description)
        {
            //Arrange
            Dish dishToAdd = new Dish() { IdCategory = dishCategoryId, Name = name, Price = price, Description = description };

            //Assert
            await Assert.ThrowsAsync<InvalidDishCategoryException>( () => dishRepository.AddAsync(dishToAdd));
        }

        [Theory]
        [InlineData(1, "Coca-cola", 18281281, "...")]
        [InlineData(1, "Coca-cola", -5, "...")]
        public async void AddAsync_InvalidPrice_ThrowInvalidDishException(int dishCategoryId, string name, decimal price, string description)
        {
            //Arrange
            Dish dishToAdd = new Dish() { IdCategory = dishCategoryId, Name = name, Price = price, Description = description };

            //Assert
            await Assert.ThrowsAsync<InvalidDishException>(() => dishRepository.AddAsync(dishToAdd));
        }

        [Theory]
        [InlineData(1, "Coca-cola", 20, "...")]
        public async void AddAsync_ValidDish_AllShouldPass(int dishCategoryId, string name, decimal price, string description)
        {
            //Arrange
            Dish dishToAdd = new Dish() { IdCategory = dishCategoryId, Name = name, Price = price, Description = description };

            //Act
            await dishRepository.AddAsync(dishToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(dishToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }




        [Theory]
        [InlineData(-12)]
        public async void GetAllDishesByCategoryIdAsync_InvalidDishCategory_ThrowInvalidDishCategoryException(int dishCategoryId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidDishCategoryException>(() => dishRepository.GetAllDishesByCategoryIdAsync(dishCategoryId));
        }

        [Theory]
        [InlineData(3 , 7, 8, 9)]
        public async void GetAllDishesByCategoryIdAsync_ValidDishCategory_AllShouldPass(int dishCategoryId, params int[] dishIds)
        {
            //Arrange
            var expected = new List<Dish>();

            foreach(var val in dishIds)
            {
                expected.Add(new Dish() { Id = val, IdCategory = dishCategoryId });
            }

            //Act
            var actual = await dishRepository.GetAllDishesByCategoryIdAsync(dishCategoryId);

            //Assert
            Assert.Equal(expected, actual, new DishComparer());
        }




        [Theory]
        [InlineData(-121)]
        public async void GetByIdWithCategoryAsync_DishDoesNotExist_ThrowInvalidDishException(int dishId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidDishException>(() => dishRepository.GetByIdWithCategoryAndRestaurantAsync(dishId));
        }

        [Theory]
        [InlineData(10, 4)]
        public async void GetByIdWithCategoryAsync_ValidDish_AllShouldPass(int dishId, int dishCategoryId)
        {
            //Arrange 
            var expected = new Dish() { Id = dishId, IdCategory = dishCategoryId };

            //Act
            var actual = await dishRepository.GetByIdWithCategoryAndRestaurantAsync(dishId);

            //Assert
            Assert.Equal(expected, actual, new DishComparer());
        }
    }
}
