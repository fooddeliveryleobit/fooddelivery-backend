﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests
{
    class CityComparer : IEqualityComparer<City>
    {
        public bool Equals(City x,  City y)
        {
            return x.CityName == y.CityName;
        }

        public int GetHashCode(City obj)
        {
            return obj.GetHashCode();
        }
    }
    public class CityRepositoryTests
    {
        ICityRepository cityRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public CityRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            cityRepository = new CityRepository(foodDeliveryDbContext);
        }


        [Theory]
        [InlineData("Zp")]
        public async void AddAsync_AddExistingCity_ThrowInvalidCityException(string cityId)
        {
            //Arrange
            City city = new City() { CityName = cityId };

            //Assert
            await Assert.ThrowsAsync<InvalidCityException>(() => cityRepository.AddAsync(city));
        }

        [Theory]
        [InlineData("Dnepr")]
        public async void AddAsync_ValidNewCity_AllShouldPass(string cityId)
        {
            //Arrange
            City cityToAdd = new City() { CityName = cityId };

            //Actual
            await cityRepository.AddAsync(cityToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(cityToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }
    
    


        [Theory]
        [InlineData("Zppppp")]
        public async void GetByIdAsync_CityDoesNotExist_ThrowInvalidCityException(string cityId)
        {
            //Act
            var actual = await cityRepository.GetByIdAsync(cityId);

            //Assert
            Assert.Null(actual);
        }

        [Theory]
        [InlineData("Zp")]
        public async void GetByIdAsync_ValidCity_AllShouldPass(string cityId)
        {
            //Arrange
            var expected = new City() { CityName = cityId };

            //Act
            var actual = await cityRepository.GetByIdAsync(cityId);

            //Assert
            Assert.Equal(expected, actual, new CityComparer());
        }

    }
}
