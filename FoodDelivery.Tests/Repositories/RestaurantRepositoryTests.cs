﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Xunit;

namespace FoodDelivery.Tests
{
    class RestaurantComparer : IEqualityComparer<Restaurant>
    {
        public bool Equals(Restaurant restaurant1,  Restaurant restaurant2)
        {
            return restaurant1.Id == restaurant2.Id;
        }

        public int GetHashCode(Restaurant obj)
        {
            return obj.GetHashCode();
        }
    }

    public class RestaurantRepositoryTests
    {
        IRestaurantRepository restaurantRepository { get; set; }
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        public RestaurantRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            restaurantRepository = new RestaurantRepository(foodDeliveryDbContext);
        }


        [Theory]
        [InlineData(-4, "Zp", "Burger King")]
        public async void AddAsync_OwnerDoesNotExist_ThrowInvalidUserException(int ownerId, string cityId, string name)
        {
            //Arrange
            Restaurant restaurantToAdd = new Restaurant() { OwnerId = ownerId, CityId = cityId, Name = name };

            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => restaurantRepository.AddAsync(restaurantToAdd));
        }

        [Theory]
        [InlineData(1, "Zp", "Burger King")]
        public async void AddAsync_UserIsNotOwner_ThrowInvalidRoleException(int ownerId, string cityId, string name)
        {
            //Arrange
            Restaurant restaurantToAdd = new Restaurant() { OwnerId = ownerId, CityId = cityId, Name = name };

            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => restaurantRepository.AddAsync(restaurantToAdd));
        }

        [Theory]
        [InlineData(11, "Zppppp", "Burger King")]
        public async void AddAsync_CityDoesNotExist_ThrowInvalidCityException(int ownerId, string cityId, string name)
        {
            //Arrange
            Restaurant restaurantToAdd = new Restaurant() { OwnerId = ownerId, CityId = cityId, Name = name };

            //Assert
            await Assert.ThrowsAsync<InvalidCityException>(() => restaurantRepository.AddAsync(restaurantToAdd));
        }
        [Theory]
        [InlineData(11, "Zp", "Burger King")]
        public async void AddAsync_RestaurantValid_AllShouldPass(int ownerId, string cityId, string name)
        {
            //Arrange
            Restaurant restaurantToAdd = new Restaurant() { OwnerId = ownerId, CityId = cityId, Name = name };

            //Act
            await restaurantRepository.AddAsync(restaurantToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(restaurantToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }




        [Theory]
        [InlineData(100)]
        public async void GetAllRestaurantsByOwnerIdAsync_UserDoesNotExist_ThrowInvalidUserException(int ownerId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => restaurantRepository.GetAllRestaurantsByOwnerIdAsync(ownerId));
        }

        [Theory]
        [InlineData(1)]
        public async void GetAllRestaurantsByOwnerIdAsync_UserIsNotCustomer_ThrowInvalidRoleException(int ownerId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => restaurantRepository.GetAllRestaurantsByOwnerIdAsync(ownerId));
        }

        [Theory]
        [InlineData(12, 3, 4)]
        [InlineData(14, 6)]
        public async void GetAllRestaurantsByOwnerIdAsync_ValidOwner_ShouldAllPass(int ownerId, params int[] restaurantIds)
        {
            //Arrange
            var expected = new List<Restaurant>();

            foreach (var val in restaurantIds)
            {
                expected.Add(new Restaurant() { Id = val });
            }

            //Act
            var actual = await restaurantRepository.GetAllRestaurantsByOwnerIdAsync(ownerId);

            //Assert
            Assert.Equal(expected, actual, new RestaurantComparer());
        }





        [Theory]
        [InlineData(-1)]
        public async void GetAllAvaliableRestaurantsToCustomer_UserDoesNotExist_ThrowInvalidUserException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => restaurantRepository.GetAllAvaliableRestaurantsToCustomer(userId));
        }

        [Theory]
        [InlineData(7)]
        public async void GetAllAvaliableRestaurantsToCustomer_UserIsNotCustomer_ThrowInvalidRoleException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => restaurantRepository.GetAllAvaliableRestaurantsToCustomer(userId));
        }

        [Theory]
        [InlineData(1,  1, 6)]
        [InlineData(4,  1, 2, 3, 4, 5)]
        public async void GetAllAvaliableRestaurantsToCustomer_ValidCustomer_ShouldAllPass(int userId, params int [] restaurantIds)
        {
            //Arrange
            var expected = new List<Restaurant>();

            foreach(var val in restaurantIds)
            {
                expected.Add(new Restaurant() { Id = val });
            }

            //Act
            var actual = await restaurantRepository.GetAllAvaliableRestaurantsToCustomer(userId);

            //Assert
            Assert.Equal(expected, actual, new RestaurantComparer());
        }




        [Theory]
        [InlineData("Zppp")]
        public async void GetAllRestaurantsByCityIdAsync_CityDoesNotExist_ThrowInvalidCityException(string city)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidCityException>(() => restaurantRepository.GetAllRestaurantsByCityIdAsync(city));
        }

        [Theory]
        [InlineData("Zp")]
        public async void GetAllRestaurantsByCityIdAsync_ValidCityId_AllShouldPass(string city)
        {         
            //Act
            var actual = await restaurantRepository.GetAllRestaurantsByCityIdAsync(city);

            //Assert
            Assert.Equal(new List<Restaurant>(), actual);
        }

    }
}
