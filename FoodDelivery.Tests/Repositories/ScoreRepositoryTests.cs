﻿using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data;
using FoodDelivery.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Xunit;

namespace FoodDelivery.Tests
{
    class ScoreComparer : IEqualityComparer<Score>
    {
        public bool Equals(Score x,  Score y)
        {
            return Math.Round(x.Value,3) == Math.Round(y.Value, 3);
        }
        public int GetHashCode(Score obj)
        {
            return obj.GetHashCode();
        }
    }
   
    public class ScoreRepositoryTests
    {
        FoodDeliveryDbContext foodDeliveryDbContext { get; set; }
        IScoreRepository scoreRepository { get; set; }

        public ScoreRepositoryTests()
        {
            var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";

            var options = new DbContextOptionsBuilder<FoodDeliveryDbContext>()
                              .UseSqlServer(connection, x => x.UseNetTopologySuite())
                              .Options;

            foodDeliveryDbContext = new FoodDeliveryDbContext(options);
            scoreRepository = new ScoreRepository(foodDeliveryDbContext);
        }

        [Theory]
        [InlineData(1, 7, 1, 4)]
        [InlineData(2, 6, 1, 3)]
        public async void AddAsync_WrongOrderUsers_ThrowInvalidUserException(int senderId, int receiverId, int orderId, double value)
        {
            //Act
            Score scoreToAdd = new Score() { SenderId = senderId, ReceiverId = receiverId, OrderId = orderId, Value = value };

            //Assert
            await Assert.ThrowsAsync<InvalidOrderException>(() => scoreRepository.AddAsync(scoreToAdd));
        }

        [Theory]
        [InlineData(1, 2, 1, 3)]
        [InlineData(7, 6, 1, 4)]
        [InlineData(5, 14, 8, 5)]
        public async void AddAsync_TwoDeliversOrCustomers_ThrowInvalidUserException(int senderId, int receiverId, int orderId, double value)
        {
            //Act
            Score scoreToAdd = new Score() { SenderId = senderId, ReceiverId = receiverId, OrderId = orderId, Value = value };

            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => scoreRepository.AddAsync(scoreToAdd));
        }

        [Theory]
        [InlineData(1, 6, 1, 4)]
        [InlineData(2, 9, 3, 5)]
        public async void AddAsync_AddOneMoreScore_ThrowInvalidScoreException(int senderId, int receiverId, int orderId, double value)
        {
            //Act
            Score scoreToAdd = new Score() { SenderId = senderId, ReceiverId = receiverId, OrderId = orderId, Value = value };

            //Assert
            await Assert.ThrowsAsync<InvalidScoreException>(() => scoreRepository.AddAsync(scoreToAdd));
        }

        [Theory]
        [InlineData(9, 2, 3, 10)]
        [InlineData(4, 7, 9, 0.5)]
        public async void AddAsync_AddWrongScoreValue_ThrowInvalidScoreException(int senderId, int receiverId, int orderId, double value)
        {
            //Act
            Score scoreToAdd = new Score() { SenderId = senderId, ReceiverId = receiverId, OrderId = orderId, Value = value };

            //Assert
            await Assert.ThrowsAsync<InvalidScoreException>(() => scoreRepository.AddAsync(scoreToAdd));
        }

        [Theory]
        [InlineData(4, 7, 9, 3)]
        [InlineData(10, 5, 12, 4)]
        [InlineData(4, 10, 14, 5)]
        public async void AddAsync_AddValidScore_AllShouldPass(int senderId, int receiverId, int orderId, double value)
        {
            //Assert
            Score scoreToAdd = new Score() { SenderId = senderId, ReceiverId = receiverId, OrderId = orderId, Value = value };

            //Act
            await scoreRepository.AddAsync(scoreToAdd);
            EntityState actual = foodDeliveryDbContext.Entry(scoreToAdd).State;

            //Assert
            Assert.Equal(EntityState.Added, actual);
        }




        [Theory]
        [InlineData(-1)]
        public async void GetAllReceivedScoresByUserIdAsync_UserDoesNotExist_ThrowInvalidUserException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => scoreRepository.GetAllReceivedScoresByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(11)]
        public async void GetAllReceivedScoresByUserIdAsync_UserIsNotCustomerOrDeliver_ThrowInvalidRoleException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => scoreRepository.GetAllReceivedScoresByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(10, (double)3, (double)5)]
        public async void GetAllReceivedScoresByUserIdAsync_ValidUser_AllShouldPass(int userId, params double[] scores)
        {
            //Arrange
            var expected = new List<Score>();
            foreach (var val in scores)
            {
                expected.Add(new Score() {Value = val });
            }

            //Act
            var actual = await scoreRepository.GetAllReceivedScoresByUserIdAsync(userId);

            //Assert
            Assert.Equal(expected, actual, new ScoreComparer());
        }




        [Theory]
        [InlineData(23232)]
        public async void GetAllSentScoresByUserIdAsync_UserDoesNotExist_ThrowInvalidUserException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => scoreRepository.GetAllSentScoresByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(13)]
        public async void GetAllSentScoresByUserIdAsync_UserIsNotCustomerOrDeliver_ThrowInvalidRoleException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => scoreRepository.GetAllSentScoresByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(5,  (double)5, (double)3, (double)5)]
        public async void GetAllSentScoresByUserIdAsync_ValidUser_AllShouldPass(int userId, params double[] scores)
        {
            //Arrange
            var expected = new List<Score>();
            foreach (var val in scores)
            {
                expected.Add(new Score() { Value = val });
            }

            //Act
            var actual = await scoreRepository.GetAllSentScoresByUserIdAsync(userId);

            //Assert
            Assert.Equal(expected, actual, new ScoreComparer());
        }





        [Theory]
        [InlineData(2, 1, 5)]
        [InlineData(1, 12, 1)]
        public async void GetScoreByIdAsync_InvalidIds_ThrowInvalidScoreException(int senderId, int receiverId, int orderId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidScoreException>(() => scoreRepository.GetScoreByIdAsync(senderId, receiverId, orderId));
        }

        [Theory]
        [InlineData(6, 1, 1, 4.5)]
        [InlineData(5, 10, 12, 5)]
        public async void GetScoreByIdAsync_ValidIds_AllShouldPass(int senderId, int receiverId, int orderId, double value)
        {
            //Arrange
            Score expected = new Score() { SenderId = senderId, ReceiverId = receiverId, Value = value };

            //Act
            Score actual = await scoreRepository.GetScoreByIdAsync(senderId, receiverId, orderId);

            //Assert
            Assert.Equal(expected, actual, new ScoreComparer());
        }




        [Theory]
        [InlineData(-2)]
        public async void GetAverageScoreByUserIdAsync_UserDoesNotExist_ThrowInvalidUserException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidUserException>(() => scoreRepository.GetAverageScoreByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(12)]
        public async void GetAverageScoreByUserIdAsync_UserIsNotCustomerOrDeliver_ThrowInvalidRoleException(int userId)
        {
            //Assert
            await Assert.ThrowsAsync<InvalidRoleException>(() => scoreRepository.GetAverageScoreByUserIdAsync(userId));
        }

        [Theory]
        [InlineData(2, 4.0)]
        [InlineData(8, 3.6666)]
        public async void GetAverageScoreByUserIdAsync_ValidUser_AllShouldPass(int userId, double expected)
        {
            //Act
            var actual = await scoreRepository.GetAverageScoreByUserIdAsync(userId);

            //Assert
            Assert.Equal(Math.Round(expected, 3), Math.Round(actual, 3));
        }

     }
} 
