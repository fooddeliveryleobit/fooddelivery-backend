﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class ScoresControllerTests : ControllerTests
    {
        public ScoresControllerTests()
        { }


        [Theory]
        [InlineData("clientAnonymous")]
        public async void GetAllScores_UserIsAnonymous_StatusCode401(string user)
        {
            //Arrange
            var url = $"/api/Scores";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientOwner12")]
        [InlineData("clientCustomer2")]
        [InlineData("clientDeliver7")]
        public async void GetAllScores_UserIsNotAdmin_StatusCode403(string user)
        {
            //Arrange
            var url = $"/api/Scores";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin")]
        public async void GetAllScores_UserIsAdmin_StatusCode200(string user)
        {
            //Arrange
            var url = $"/api/Scores";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData(6, "clientAnonymous")]
        public async void GetUserScore_UserIsAnonymous_StatusCode401(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetUserScore/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, "clientCustomer2")]
        [InlineData(7, "clientDeliver6")]
        [InlineData(1, "clientOwner11")]
        public async void GetUserScore_UserIsNotAllowed_StatusCode403(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetUserScore/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientCustomer2")]
        [InlineData(6, "clientDeliver6")]
        [InlineData(1, "clientAdmin")]
        public async void GetUserScore_UserIsAllowed_StatusCode200(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetUserScore/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData(6, "clientAnonymous")]
        public async void GetAllReceivedScoresById_UserIsAnonymous_StatusCode401(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllReceivedScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, "clientCustomer2")]
        [InlineData(7, "clientDeliver6")]
        [InlineData(1, "clientOwner11")]
        public async void GetAllReceivedScoresById_UserIsNotAllowed_StatusCode403(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllReceivedScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientCustomer2")]
        [InlineData(6, "clientDeliver6")]
        [InlineData(1, "clientAdmin")]
        public async void GetAllReceivedScoresById_UserIsAllowed_StatusCode200(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllReceivedScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData(6, "clientAnonymous")]
        public async void GetAllSentScoresById_UserIsAnonymous_StatusCode401(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllSentScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, "clientCustomer2")]
        [InlineData(7, "clientDeliver6")]
        [InlineData(1, "clientOwner11")]
        public async void GetAllSentScoresById_UserIsNotAllowed_StatusCode403(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllSentScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientCustomer2")]
        [InlineData(6, "clientDeliver6")]
        [InlineData(1, "clientAdmin")]
        public async void GetAllSentScoresById_UserIsAllowed_StatusCode200(int userId, string user)
        {
            //Arrange
            var url = $"/api/Scores/GetAllSentScoresById/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData(6, 1, 1, "clientAnonymous")]
        public async void GetScoreById_UserIsAnonymous_StatusCode401(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); 

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientCustomer2")]
        [InlineData(6, 1, 1, "clientDeliver7")]
        [InlineData(6, 1, 1, "clientOwner11")]
        public async void GetScoreById_UserIsNotAllowed_StatusCode403(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientCustomer1")]
        [InlineData(6, 1, 1, "clientDeliver6")]
        [InlineData(6, 1, 1, "clientAdmin")]
        public async void GetScoreById_UserIsAllowed_StatusCode200(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url); ;

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData(6, 1, 1, "clientAnonymous")]
        public async void CreateScore_UserIsAnonymous_StatusCode401(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));
            string stringRequestBody = "{" + $"\"value\": 1" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);


            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientCustomer1")]
        [InlineData(6, 1, 1, "clientDeliver7")]
        public async void CreateScore_UserIsNotAllowed_StatusCode403(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));
            string stringRequestBody = "{" + $"\"value\": 1" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(6, 1, 1, "clientAnonymous")]
        public async void UpdateScore_UserIsAnonymous_StatusCode401(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));
            string stringRequestBody = "{" + $"\"value\": 1" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);


            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientCustomer1")]
        [InlineData(6, 1, 1, "clientDeliver7")]
        public async void UpdateScore_UserIsNotAllowed_StatusCode403(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));
            string stringRequestBody = "{" + $"\"value\": 1" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientDeliver6", 4.5)]
        [InlineData(6, 1, 1, "clientAdmin", 3.5)]
        public async void UpdateScore_UserIsAllowed_StatusCode200(int senderId, int receiverId, int orderId, string user, double score)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));
            string stringRequestBody = "{" + $"\"value\": {score}" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);

            var scoreResult = JsonConvert.DeserializeObject<Score>(await responce.Content.ReadAsStringAsync());

            Assert.Equal(score, scoreResult.Value);
        }




        [Theory]
        [InlineData(6, 1, 1, "clientAnonymous")]
        public async void DeleteScore_UserIsAnonymous_StatusCode401(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);


            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(6, 1, 1, "clientCustomer1")]
        [InlineData(6, 1, 1, "clientDeliver7")]
        public async void DeleteScore_UserIsNotAllowed_StatusCode403(int senderId, int receiverId, int orderId, string user)
        {
            //Arrange
            var url = $"/api/Scores/{senderId}/{receiverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

    }
}
