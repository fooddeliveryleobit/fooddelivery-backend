﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class UserControllerTests : ControllerTests
    {
        public UserControllerTests()
        {  }

        [Theory]
        [InlineData("clientAdmin")]
        public async void GetAllUsers_UserIsAdmin_StatusCode200(string user)
        {
            //Arrange
            var url = $"/api/Users";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAnonymous")]
        public async void GetAllUsers_UserIsAnonymous_StatusCode401(string user)
        {
            //Arrange
            var url = $"/api/Users";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientDeliver6")]
        public async void GetAllUsers_UserIsNotAdmin_StatusCode403(string user)
        {
            //Arrange
            var url = $"/api/Users";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(1, "clientAnonymous")]
        public async void GetUserById_UserIsAnonymous_StatusCode401(int userId, string user)
        {
            //Arrange
            var url =  $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientCustomer1")]
        public async void GetUserById_UserIsNotSelectedUser_StatusCode403(int userId, string user)
        {
            //Arrange
            var url = $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }
       
        [Theory]
        [InlineData(2, "clientCustomer2")]
        [InlineData(2, "clientAdmin")]
        public async void GetUserById_CorrectUser_StatusCode200(int userId, string user)
        {
            //Arrange
            var url = $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData(1, "clientAnonymous",  "Liza-Customerrr")]
        public async void UpdateUser_UserIsAnonymous_StatusCode401(int userId, string user, string userName)
        {
            //Arrange
            var url = $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" + $"\"name\": \"{userName}\"," +
                                $"\"password\": null," +
                                $"\"cityId\": null," +
                                $"\"latitude\": 0," +
                                $"\"longitude\": 0," +
                                $"\"picture\": null" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);


            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(1, "clientCustomer2", "Liza-Customerrr")]
        public async void UpdateUser_UserIsNotSelectedUser_StatusCode403(int userId, string user,  string userName)
        {
            //Arrange
            var url = $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" + $"\"name\": \"{userName}\"," +
                                $"\"password\": null," +
                                $"\"cityId\": null," +
                                $"\"latitude\": 0," +
                                $"\"longitude\": 0," +
                                $"\"picture\": null" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);


            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(1, "clientCustomer1", "Liza-Customerrr")]
        [InlineData(1, "clientCustomer1", "Liza-Customerrrrr")]
        [InlineData(2, "clientAdmin", "Masya-Custommmmmmer")]
        [InlineData(2, "clientAdmin", "Masya-Custommmer")]
        public async void UpdateUser_CorrectUser_StatusCode200(int userId, string user, string userName)
        {
            //Arrange
            var url = $"/api/Users/{userId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" + $"\"name\": \"{userName}\"," +
                                $"\"password\": null," +
                                $"\"cityId\": null," +
                                $"\"latitude\": 0," +
                                $"\"longitude\": 0," +
                                $"\"picture\": null" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);


            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
            
            var userResult = JsonConvert.DeserializeObject<User>(await responce.Content.ReadAsStringAsync());

            Assert.Equal(userName, userResult.Name);
        }
    }
}
