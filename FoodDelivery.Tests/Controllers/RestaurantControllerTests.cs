﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class RestaurantControllerTests : ControllerTests
    {
        public RestaurantControllerTests() 
        {  }

        [Theory]
        [InlineData("clientAnonymous")]
        [InlineData("clientOwner12")]
        public async void GetAllRestaurants_CorrectUser_StatusCode200(string user)
        {
            //Arrange
            var url = $"/api/Restaurants";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData(11, "clientAnonymous")]
        public async void CreateRestaurant_UserIsAnonymous_StatusCode401(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" + 
                                $"\"name\": \"Bla-bla-bla\"," +
                                $"\"cityId\": \"Lviv\"," +
                                $"\"longitude\": 49.811598," +
                                $"\"longitude\": 23.986025," + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(11, "clientOwner12")]
        public async void CreateRestaurant_UserIsNotSelectedOwner_StatusCode403(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"name\": \"Bla-bla-bla\"," +
                                $"\"cityId\": \"Lviv\"," +
                                $"\"longitude\": 49.811598," +
                                $"\"longitude\": 23.986025" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(3, "clientAnonymous")]
        public async void DeleteRestaurant_UserIsAnonymous_StatusCode401(int restaurantId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(3, "clientOwner11")]
        public async void DeleteRestaurant_UserIsNotSelectedOwner_StatusCode403(int restaurantId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));


            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }





        [Theory]
        [InlineData(2, "clientAnonymous", "Olivochka =)")]
        public async void UpdateRestaurant_UserIsAnonymous_StatusCode401(int restaurantId, string user, string newRestaurantName)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"name\": \"{newRestaurantName}\"," +
                                $"\"cityId\": \"Lviv\"," +
                                $"\"longitude\": 49.811598," +
                                $"\"longitude\": 23.986025," + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(3, "clientOwner11", "Olivochka =)")]
        public async void UpdateRestaurant_UserIsNotSelectedOwner_StatusCode403(int restaurantId, string user, string newRestaurantName)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"name\": \"{newRestaurantName}\"," +
                                $"\"cityId\": \"Lviv\"," +
                                $"\"longitude\": 49.811598," +
                                $"\"longitude\": 23.986025" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(3, "clientOwner12", "Olivochka =D 1.0")]
        [InlineData(3, "clientAdmin", "Olivochka =D 2.0")]
        public async void UpdateRestaurant_CorrectUser_StatusCode200(int restaurantId, string user, string newRestaurantName)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"name\": \"{newRestaurantName}\"," +
                                $"\"cityId\": \"Lviv\"," +
                                $"\"longitude\": 49.811598," +
                                $"\"longitude\": 23.986025" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
            
            var restaurantResult = JsonConvert.DeserializeObject<Restaurant>(await responce.Content.ReadAsStringAsync());

            Assert.Equal(newRestaurantName, restaurantResult.Name);
        }

     





        [Theory]
        [InlineData(3, "clientAnonymous")]
        public async void GetAllRestaurantsByOwnerId_UserIsAnonymous_StatusCode200(int restaurantId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData(11, "clientAnonymous")]
        public async void GetAllRestaurantsByOwnerId_UserIsAnonymous_StatusCode401(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/GetAllRestaurantsByOwnerId/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(11, "clientDeliver7")]
        public async void GetAllRestaurantsByOwnerId_UserIsNotSelectedOwner_StatusCode403(int ownerId, string user)
        {
            // Arrange
            var url = $"/api/Restaurants/GetAllRestaurantsByOwnerId/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(11, "clientAdmin")]
        [InlineData(11, "clientOwner11")]
        public async void GetAllRestaurantsByOwnerId_CorrectUser_StatusCode200(int ownerId, string user)
        {
            // Arrange
            var url = $"/api/Restaurants/GetAllRestaurantsByOwnerId/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

    



        [Theory]
        [InlineData(2, "clientAnonymous")]
        public async void GetAllAvaliableRestaurantsToCustomer_UserIsAnonymous_StatusCode401(int customerId, string user)
        {
            // Arrange
            var url = $"/api/Restaurants/GetAllAvaliableRestaurantsToCustomer/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientDeliver6")]
        public async void GetAllAvaliableRestaurantsToCustomer_UserIsNotSelectedCustomer_StatusCode403(int customerId, string user)
        {
            // Arrange
            var url = $"/api/Restaurants/GetAllAvaliableRestaurantsToCustomer/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData(2, "clientAdmin")]
        [InlineData(2, "clientCustomer2")]
        public async void GetAllAvaliableRestaurantsToCustomer_CorrectUser_StatusCode200(int customerId, string user)
        {
            // Arrange
            var url = $"/api/Restaurants/GetAllAvaliableRestaurantsToCustomer/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData("Lviv", "clientAnonymous")]
        public async void GetAllRestaurantsByCityId_UserIsAnonymous_StatusCode200(string cityId, string user)
        {
            //Arrange
            var url = $"/api/Restaurants/GetAllRestaurantsByCityId/{cityId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

    }
}

