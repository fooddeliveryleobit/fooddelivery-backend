﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class OrdersControllerTests : ControllerTests
    {
        public OrdersControllerTests()
        { }

        #region  Default actions
        [Theory]
        [InlineData("clientAnonymous")]
        public async void GetAllOrders_UserIsAnonymous_StatusCode401(string user)
        {
            //Arrange
            var url = $"/api/Orders";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientOwner11")]
        public async void GetAllOrders_UserIsNotAllowed_StatusCode403(string user)
        {
            //Arrange
            var url = $"/api/Orders";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin")]
        public async void GetAllOrders_UserIsAdmin_StatusCode200(string user)
        {
            //Arrange
            var url = $"/api/Orders";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 1)]
        public async void GetOrderById_UserIsAnonymous_StatusCode401(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetOrderById_UserIsNotAllowed_StatusCode403(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 1)]
        [InlineData("clientCustomer1", 1)]
        [InlineData("clientDeliver6", 1)]
        [InlineData("clientOwner11", 1)]
        public async void GetOrderById_UserBelongsToOrder_StatusCode200(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 1)]
        public async void GetTotalOrderCost_UserIsAnonymous_StatusCode401(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/GetTotalOrderCost/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetTotalOrderCost_UserIsNotAllowed_StatusCode403(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/GetTotalOrderCost/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 1)]
        [InlineData("clientCustomer1", 1)]
        [InlineData("clientDeliver6", 1)]
        [InlineData("clientOwner11", 1)]
        public async void GetTotalOrderCost_UserBelongsToOrder_StatusCode200(string user, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/GetTotalOrderCost/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

        #endregion

        #region For Owners
        [Theory]
        [InlineData("clientAnonymous", 1)]
        public async void GetAllFinishedOrdersRestaurantOrders_UserIsAnonymous_StatusCode401(string user, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedOrdersRestaurantOrders/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetAllFinishedOrdersRestaurantOrders_UserIsNotAllowed_StatusCode403(string user, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedOrdersRestaurantOrders/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 1)]
        [InlineData("clientOwner11", 1)]
        public async void GetAllFinishedOrdersRestaurantOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedOrdersRestaurantOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous", 1)]
        public async void GetAllAcceptedByDeliverOrdersRestaurantOrders_UserIsAnonymous_StatusCode401(string user, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverOrdersRestaurantOrders/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetAllAcceptedByDeliverOrdersRestaurantOrders_UserIsNotAllowed_StatusCode403(string user, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverOrdersRestaurantOrders/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 1)]
        [InlineData("clientOwner11", 1)]
        public async void GetAllAcceptedByDeliverOrdersRestaurantOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverOrdersRestaurantOrders/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }
        #endregion

        #region For Customers

        #region AllCustomerOrdes by orderStatus
        [Theory]
        [InlineData("clientAnonymous", 1)]
        public async void GetAllNewCustomerOrders_UserIsAnonymous_StatusCode401(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllNewCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetAllNewCustomerOrders_UserIsNotAllowed_StatusCode403(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllNewCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 1)]
        [InlineData("clientCustomer1", 1)]
        public async void GetAllNewCustomerOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllNewCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 2)]
        public async void GetAllAcceptedByDeliverCustomerOrders_UserIsAnonymous_StatusCode401(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer1", 2)]
        [InlineData("clientDeliver7", 2)]
        [InlineData("clientOwner12", 2)]
        public async void GetAllAcceptedByDeliverCustomerOrders_UserIsNotAllowed_StatusCode403(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 2)]
        [InlineData("clientCustomer2", 2)]
        public async void GetAllAcceptedByDeliverCustomerOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAcceptedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous", 2)]
        public async void GetAllFinishedByDeliverCustomerOrders_UserIsAnonymous_StatusCode401(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer1", 2)]
        [InlineData("clientDeliver7", 2)]
        [InlineData("clientOwner12", 2)]
        public async void GetAllFinishedByDeliverCustomerOrders_UserIsNotAllowed_StatusCode403(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 2)]
        [InlineData("clientCustomer2", 2)]
        public async void GetAllFinishedByDeliverCustomerOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int customerId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverCustomerOrders/{customerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }
        #endregion




        [Theory]
        [InlineData("clientAnonymous", 1, 1)]
        public async void CreateOrderByCustomer_UserIsAnonymous_StatusCode401(string user, int customerId, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/CreateOrderByCustomer/{customerId}/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "[ {" +
                                $"\"dishId\": 2," +
                                $"\"count\": 5" + "} ]";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1, 1)]
        [InlineData("clientDeliver7", 1, 1)]
        [InlineData("clientOwner12", 1, 1)]
        public async void CreateOrderByCustomer_UserIsNotAllowed_StatusCode403(string user, int customerId, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/CreateOrderByCustomer/{customerId}/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "[ {" +
                                $"\"dishId\": 2," +
                                $"\"count\": 5" + "} ]";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 1, 1)]
        public async void DeclineOrderByCustomer_UserIsAnonymous_StatusCode401(string user, int customerId, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/DeclineOrderByCustomer/{customerId}/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "[ {" +
                                $"\"dishId\": 2," +
                                $"\"count\": 5" + "} ]";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1, 1)]
        [InlineData("clientDeliver7", 1, 1)]
        [InlineData("clientOwner12", 1, 1)]
        public async void DeclineOrderByCustomer_UserIsNotAllowed_StatusCode403(string user, int customerId, int restaurantId)
        {
            //Arrange
            var url = $"/api/Orders/DeclineOrderByCustomer/{customerId}/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }


        #endregion


        #region For Delivers
        #region AllDeliverOrders by orderStatus

        [Theory]
        [InlineData("clientAnonymous", 6)]
        public async void GetAcceptedByDeliverDeliverOrder_UserIsAnonymous_StatusCode401(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAcceptedByDeliverDeliverOrder/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 6)]
        [InlineData("clientDeliver7", 6)]
        [InlineData("clientOwner12", 6)]
        public async void GetAcceptedByDeliverDeliverOrder_UserIsNotAllowed_StatusCode403(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAcceptedByDeliverDeliverOrder/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 6)]
        [InlineData("clientDeliver6", 6)]
        public async void GetAcceptedByDeliverDeliverOrder_UserIsOwnerOrAdmin_StatusCode200(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAcceptedByDeliverDeliverOrder/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 6)]
        public async void GetAllFinishedByDeliverDeliverOrders_UserIsAnonymous_StatusCode401(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverDeliverOrders/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 6)]
        [InlineData("clientDeliver7", 6)]
        [InlineData("clientOwner12", 6)]
        public async void GetAllFinishedByDeliverDeliverOrders_UserIsNotAllowed_StatusCode403(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverDeliverOrders/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 6)]
        [InlineData("clientDeliver6", 6)]
        public async void GetAllFinishedByDeliverDeliverOrders_UserIsOwnerOrAdmin_StatusCode200(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllFinishedByDeliverDeliverOrders/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }

        #endregion




        [Theory]
        [InlineData("clientAnonymous", 6)]
        public async void GetAllAvaliableOrdersToDeliver_UserIsAnonymous_StatusCode401(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAvaliableOrdersToDeliver/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 6)]
        [InlineData("clientDeliver7", 6)]
        [InlineData("clientOwner12", 6)]
        public async void GetAllAvaliableOrdersToDeliver_UserIsNotAllowed_StatusCode403(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAvaliableOrdersToDeliver/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientAdmin", 7)]
        [InlineData("clientDeliver7", 7)]
        public async void GetAllAvaliableOrdersToDeliver_UserIsOwnerOrAdmin_StatusCode200(string user, int deliverId)
        {
            //Arrange
            var url = $"/api/Orders/GetAllAvaliableOrdersToDeliver/{deliverId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous", 10, 9)]
        public async void AcceptOrderByDeliver_UserIsAnonymous_StatusCode401(string user, int deliverId, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/AcceptOrderByDeliver/{deliverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 10, 9)]
        [InlineData("clientDeliver7", 10, 9)]
        [InlineData("clientOwner12", 10, 9)]
        public async void AcceptOrderByDeliver_UserIsNotAllowed_StatusCode403(string user, int deliverId, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/AcceptOrderByDeliver/{deliverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous", 6, 1)]
        public async void FinishedOrderByDeliver_UserIsAnonymous_StatusCode401(string user, int deliverId, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/FinishedOrderByDeliver/{deliverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 6, 1)]
        [InlineData("clientDeliver7", 6, 1)]
        [InlineData("clientOwner12", 6, 1)]
        public async void FinishedOrderByDeliver_UserIsNotAllowed_StatusCode403(string user, int deliverId, int orderId)
        {
            //Arrange
            var url = $"/api/Orders/FinishedOrderByDeliver/{deliverId}/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }


        #endregion
    }
}
