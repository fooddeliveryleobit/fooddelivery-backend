﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
   /* public class CitiesControllerTests : ControllerTests
    {
        public CitiesControllerTests()
        { }


        [Theory]
        [InlineData(11, "clientAnonymous")]
        public async void CreateCity_UserIsAnonymous_StatusCode401(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Cities/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"cityName\": \"Bla-bla-bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(11, "clientOwner12")]
        public async void CreateCity_UserIsNotSelectedOwner_StatusCode403(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Cities/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                                $"\"cityName\": \"Bla-bla-bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(11, "clientAnonymous")]
        public async void DeleteCity_UserIsAnonymous_StatusCode401(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Cities/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));


            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(11, "clientOwner12")]
        public async void DeleteCity_UserIsNotSelectedOwner_StatusCode403(int ownerId, string user)
        {
            //Arrange
            var url = $"/api/Cities/{ownerId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

         
            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous")]
        [InlineData("clientOwner12")]
        public async void GetAllCitiesAsync_CorrectUser_StatusCode200(string user)
        {
            //Arrange
            var url = $"/api/Cities";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }


    }*/
}
