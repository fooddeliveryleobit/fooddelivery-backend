﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class OrderedDishesControllerTests : ControllerTests
    {
        public OrderedDishesControllerTests()
        { }

        [Theory]
        [InlineData("clientAnonymous", 5)]
        public async void GetAllOrderedDishesByOrderId_UserIsAnonymous_StatusCode401(string user, int orderId)
        {
            //Arrange
            var url = $"/api/OrderedDishes/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer2", 1)]
        [InlineData("clientDeliver7", 1)]
        [InlineData("clientOwner12", 1)]
        public async void GetAllOrderedDishesByOrderId_UserIsNotAllowed_StatusCode403(string user, int orderId)
        {
            //Arrange
            var url = $"/api/OrderedDishes/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer1", 1)]
        [InlineData("clientDeliver6", 1)]
        [InlineData("clientOwner11", 1)]
        [InlineData("clientAdmin", 1)]
        public async void GetAllOrderedDishesByOrderId_UserIsAllowed_StatusCode200(string user, int orderId)
        {
            //Arrange
            var url = $"/api/OrderedDishes/{orderId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }
    }
}
