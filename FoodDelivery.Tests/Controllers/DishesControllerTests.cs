﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class DishesControllerTests : ControllerTests
    {
        public DishesControllerTests()
        { }

        [Theory]
        [InlineData("clientAnonymous", 5)]
        [InlineData("clientDeliver6", 6)]
        public async void GetDishById_CorrectUser_StatusCode200(string user, int dishId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);

        }


        [Theory]
        [InlineData("clientAnonymous", 5)]
        public async void CreateDish_UserIsAnonymous_StatusCode401(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                    $"\"price\": 100," +
                    $"\"name\": \"Name\"," +
                    $"\"description\": \"Bla-Bla-Bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }


        [Theory]
        [InlineData("clientCustomer1", 5)]
        [InlineData("clientDeliver6", 6)]
        public async void CreateDish_UserIsNotAllowed_StatusCode403(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                    $"\"price\": 100," +
                    $"\"name\": \"Name\"," +
                    $"\"description\": \"Bla-Bla-Bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData("clientAnonymous", 13)]
        public async void DeleteDish_UserIsAnonymous_StatusCode401(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer1", 11)]
        [InlineData("clientDeliver6", 8)]
        public async void DeleteDish_UserIsNotAllowed_StatusCode403(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }





        [Theory]
        [InlineData("clientAnonymous", 5)]
        public async void UpdateDish_UserIsAnonymous_StatusCode401(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                    $"\"price\": 100," +
                    $"\"name\": \"Name\"," +
                    $"\"description\": \"Bla-Bla-Bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData("clientCustomer1", 5, "______")]
        [InlineData("clientDeliver6", 6, "______")]
        public async void UpdateDish_UserIsNotAllowed_StatusCode403(string user, int dishCategoryId, string newDishName)
        {
            //Arrange
            var url = $"/api/Dishes/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                    $"\"price\": 10," +
                    $"\"name\": \"{newDishName}\"," +
                    $"\"description\": \"Bla-Bla-Bla\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }
   
        [Theory]
        [InlineData("clientAdmin", 5, "______")]
        [InlineData("clientOwner11", 6, "@@@@@")]
        public async void UpdateDish_UserIsCorrect_StatusCode200(string user, int dishId, string newDishName)
        {
            //Arrange
            var url = $"/api/Dishes/{dishId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                    $"\"price\": 0," +
                    $"\"name\": \"{newDishName}\"," +
                    $"\"description\": null" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");

            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);

            var dishResult = JsonConvert.DeserializeObject<Dish>(await responce.Content.ReadAsStringAsync());

            Assert.Equal(newDishName, dishResult.Name);
        }


   
        [Theory]
        [InlineData("clientAnonymous", 3)]
        [InlineData("clientDeliver7", 2)]
        public async void GetAllDishesByDishCategory_CorrectUser_StatusCode200(string user, int dishCategoryId)
        {
            //Arrange
            var url = $"/api/Dishes/GetAllDishesByDishCategory/{dishCategoryId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }
       }
}
