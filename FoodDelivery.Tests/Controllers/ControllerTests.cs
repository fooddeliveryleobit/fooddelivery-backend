﻿using FoodDelivery.Api;
using FoodDelivery.Core.Models;
using IdentityModel.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Tests.Controllers
{
    public abstract class ControllerTests
    {
        private readonly HttpClient client;

        private string[] usersName = { "clientAnonymous" , "clientAdmin" ,
                                       "clientCustomer1" , "clientCustomer2" ,
                                        "clientDeliver6", "clientDeliver7", "clientDeliver10",
                                         "clientOwner11", "clientOwner12" };

        protected readonly Dictionary<string, HttpClient> allUsers;

        public  ControllerTests()
        {
            var server = new TestServer(new WebHostBuilder()
                   .UseEnvironment("Development")
                   .UseStartup<Startup>());

            allUsers = new Dictionary<string, HttpClient>();


            client = server.CreateClient();
            allUsers.Add(usersName[0], client);


            client = server.CreateClient();
            client.SetBearerToken(GetToken("admin@gmail.com", "admin123").Result);
            allUsers.Add(usersName[1], client);



            client = server.CreateClient();
            client.SetBearerToken(GetToken("string1@gmail.com", "string1234").Result);
            allUsers.Add(usersName[2], client);

            client = server.CreateClient();
            client.SetBearerToken(GetToken("string2@gmail.com", "string123").Result);
            allUsers.Add(usersName[3], client);



            client = server.CreateClient();
            client.SetBearerToken(GetToken("string6@gmail.com", "string123").Result);
            allUsers.Add(usersName[4], client);

            client = server.CreateClient();
            client.SetBearerToken(GetToken("string7@gmail.com", "string123").Result);
            allUsers.Add(usersName[5], client);

            client = server.CreateClient();
            client.SetBearerToken(GetToken("string10@gmail.com", "string123").Result);
            allUsers.Add(usersName[6], client);



            client = server.CreateClient();
            client.SetBearerToken(GetToken("string11@gmail.com", "string123").Result);
            allUsers.Add(usersName[7], client);

            client = server.CreateClient();
            client.SetBearerToken(GetToken("string12@gmail.com", "string123").Result);
            allUsers.Add(usersName[8], client);
        }

        public async Task<string> GetToken(string email, string password)
        {
            var requestString =  $"/api/Account";
            
            var content = new StringContent("{" + $"\"Email\": \"{email}\",\"Password\": \"{password}\"" + "}", Encoding.UTF8, "application/json");


            var responce = await client.PostAsync(requestString, content);

            var answer = await responce.Content.ReadAsStringAsync();

            var user = JsonConvert.DeserializeObject<User>(answer);

            return user.Token;
        }
    }
    
}
