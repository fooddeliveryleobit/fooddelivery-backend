﻿using FoodDelivery.Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace FoodDelivery.Tests.Controllers
{
    public class DishCategoriesControllerTests : ControllerTests
    {
        public DishCategoriesControllerTests()
        { }

        [Theory]
        [InlineData(4, "clientAnonymous")]
        [InlineData(4, "clientDeliver7")]
        public async void GetAllRestaurants_CorrectUser_StatusCode200(int restaurantId, string user)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);
        }




        [Theory]
        [InlineData(4, "clientAnonymous", "NewDishCategory")]
        public async void CreateDishCategory_UserIsAnonymous_StatusCode401(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                               $"\"name\": \"{newDishCategoryName}\"," + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(4, "clientCustomer2", "NewDishCategory")]
        public async void CreateDishCategory_UserIsNotOwnerOrAdmin_StatusCode403(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                               $"\"name\": \"{newDishCategoryName}\"," + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PostAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(4, "clientAnonymous", "NewDishCategory")]
        public async void DeleteDishCategory_UserIsAnonymous_StatusCode401(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(4, "clientCustomer2", "NewDishCategory")]
        public async void DeleteDishCategory_UserIsNotOwnerOrAdmin_StatusCode403(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            //Act
            var responce = await client.DeleteAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }




        [Theory]
        [InlineData(4, "clientAnonymous", "NewDishCategory")]
        public async void UpdateDishCategory_UserIsAnonymous_StatusCode401(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                               $"\"name\": \"{newDishCategoryName}\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, responce.StatusCode);
        }

        [Theory]
        [InlineData(4, "clientCustomer2", "NewDishCategory")]
        public async void UpdateDishCategory_UserIsNotOwnerOrAdmin_StatusCode403(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                               $"\"name\": \"{newDishCategoryName}\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.Forbidden, responce.StatusCode);
        }
/*
        [Theory]
        [InlineData(7, "clientOwner12", "Bla-Bla-Bla")] //Restaurant # 3
        [InlineData(7, "clientAdmin", "Bla-Bla-DishCategory")] //Restaurant # 3
        public async void UpdateDishCategory_UserIsAdminOrOwner_StatusCode200(int restaurantId, string user, string newDishCategoryName)
        {
            //Arrange
            var url = $"/api/DishCategories/{restaurantId}";
            HttpClient client;
            Assert.True(allUsers.TryGetValue(user, out client));

            string stringRequestBody = "{" +
                               $"\"name\": \"{newDishCategoryName}\"" + "}";

            var content = new StringContent(stringRequestBody, Encoding.UTF8, "application/json");


            //Act
            var responce = await client.PutAsync(url, content);

            //Assert
            Assert.Equal(HttpStatusCode.OK, responce.StatusCode);

            var dishCategory = JsonConvert.DeserializeObject<DishCategory>(await responce.Content.ReadAsStringAsync());

            Assert.Equal(newDishCategoryName, dishCategory.Name);
        }
        */
    }
}
