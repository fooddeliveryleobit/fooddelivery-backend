﻿using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class OrderedDishConfiguration : IEntityTypeConfiguration<OrderedDish>
    {
        public void Configure(EntityTypeBuilder<OrderedDish> builder)
        {
            builder
                .ToTable("Order_2_Dish");

            builder
                .HasKey(m => new { m.OrderId, m.DishId });

            builder
                .HasOne(m => m.Order)
                .WithMany(t => t.OrderedDish)
                .HasForeignKey(m => m.OrderId)
                .OnDelete(DeleteBehavior.Restrict); 

            builder
                .HasOne(m => m.Dish)
                .WithMany(t => t.Orders)
                .HasForeignKey(m => m.DishId)
                .OnDelete(DeleteBehavior.Cascade);  

            builder
                .Property(m => m.Count)
                .IsRequired();
        }
    }
}
