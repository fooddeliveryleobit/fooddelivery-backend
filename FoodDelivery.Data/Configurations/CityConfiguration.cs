﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder
                .HasKey(m => m.CityName);

            builder
                .Property(m => m.CityName)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);
        }
    }
}
