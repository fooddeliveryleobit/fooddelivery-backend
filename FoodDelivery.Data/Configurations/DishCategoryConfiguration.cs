﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class DishCategoryConfiguration : IEntityTypeConfiguration<DishCategory>
    {
        public void Configure(EntityTypeBuilder<DishCategory> builder)
        {
            builder
                 .ToTable("DishCategory");

            builder
                .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);

            builder
                .HasOne(m => m.Restaurant)
                .WithMany(t => t.DishCategories)
                .HasForeignKey(m => m.IdRestaurant)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
