﻿using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder
                .ToTable("Orders");

            builder
                .HasKey(m => new { m.Id});

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
                .Property(m => m.Date)
                .IsRequired();

            builder
                .Property(m => m.Status)
                .IsRequired()
                .HasConversion<int>();

            builder
                .Property(m => m.Location)
                .IsRequired()
                .HasColumnType("geography");

            builder
                .Ignore(m => m.Latitude);

            builder
                .Ignore(m => m.Longitude);



            builder
                .HasOne(m => m.Customer)
                .WithMany(t => t.CustomerOrders)
                .HasForeignKey(m => m.CustomerId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(m => m.Deliver)
                .WithMany(t => t.DeliverOrders)
                .HasForeignKey(m => m.DeliverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(m => m.Restaurant)
                .WithMany(t => t.Orders)
                .HasForeignKey(m => m.RestaurantId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
