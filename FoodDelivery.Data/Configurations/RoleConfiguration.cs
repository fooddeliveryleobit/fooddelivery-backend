﻿using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace FoodDelivery.Data.Configurations
{
    class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder
               .ToTable("Roles");

            builder
                .HasKey(m => m.Id);


            builder
                .Property(m => m.Id)
                .ValueGeneratedNever()
                .HasConversion<int>();

        }
    }
}
