﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class RestaurantConfiguration : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> builder)
        {
            builder
               .ToTable("Restaurants");

            builder
                .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();

            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);

            builder
               .Property(m => m.CityId)
               .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);

            builder
                .Property(m => m.Location)
                .HasColumnType("geography")
                .IsRequired();


            builder
                 .Ignore(m => m.Latitude);

            builder
                .Ignore(m => m.Longitude);

            builder
                .Property(m => m.OwnerId)
                .IsRequired();

            builder
                .Ignore(m => m.DishCategories);

            builder
                .HasOne(m => m.Owner)
                .WithMany(t => t.Restaurants)
                .HasForeignKey(m => m.OwnerId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(m => m.City)
                .WithMany(m => m.Restaurants)
                .HasForeignKey(m => m.CityId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
