﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .ToTable("Users");

            builder
                .HasKey(m => m.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();


            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);

            builder
                .Property(m => m.Email)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);


            builder
                .Property(m => m.Password)
                .IsRequired();


            builder
                .Property(m => m.Phone)
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForPhoneNumber)
                .IsRequired();

            builder
                .Property(m => m.Picture)
                .IsRequired();

            builder
                .Property(m => m.CityId)
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);

            builder
                .Property(m => m.Location)
                .HasColumnType("geography");

            builder
                .Ignore(m => m.Latitude);

            builder
                .Ignore(m => m.Longitude);

            builder
                .Ignore(m => m.Token);


            builder
                .Property(m => m.RoleId)
                .IsRequired()
                .HasConversion<int>();

            builder
                .HasOne(t => t.Role)
                .WithMany(m => m.Users)
                .HasForeignKey(t => t.RoleId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(m => m.City)
                .WithMany(m => m.Users)
                .HasForeignKey(m => m.CityId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
