﻿using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class ScoreConfiguration : IEntityTypeConfiguration<Score>
    {
        public void Configure(EntityTypeBuilder<Score> builder)
        {
            builder
                 .ToTable("Scores");

            builder
                .HasKey(m => new { m.ReceiverId, m.SenderId, m.OrderId });

            builder
                .Property(m => m.Value)
                .IsRequired();

            builder
                .HasOne(m => m.Receiver)
                .WithMany(t => t.RecievedScores)
                .HasForeignKey(m => m.ReceiverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(m => m.Sender)
                .WithMany(t => t.SentScores)
                .HasForeignKey(m => m.SenderId)
                .OnDelete(DeleteBehavior.Restrict);

             builder
                 .HasOne(m => m.Order)
                 .WithMany(t => t.Scores)
                 .HasForeignKey(m => m.OrderId)
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
