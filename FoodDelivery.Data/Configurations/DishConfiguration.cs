﻿using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.Data.Configurations
{
    class DishConfiguration : IEntityTypeConfiguration<Dish>
    {
        public void Configure(EntityTypeBuilder<Dish> builder)
        {
            builder
                .ToTable("Dishes");

            builder
                .HasKey(m => m.Id);


            builder
                .Property(m => m.Id)
                .UseIdentityColumn();


            builder
                .Property(m => m.IdCategory)
                .IsRequired();


            builder
                .Property(m => m.Price)
                .IsRequired()
                .HasColumnType("decimal(18,4)");


            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForString);


            builder
                .Property(m => m.Description)
                .IsRequired()
                .HasMaxLength(ValidatorProperties.MaxCountSymbolsForText);


            builder
                .HasOne(m => m.Category)
                .WithMany(t => t.Dishes)
                .HasForeignKey(m => m.IdCategory)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

}
