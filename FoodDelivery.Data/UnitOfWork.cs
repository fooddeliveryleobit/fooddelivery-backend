﻿using System.Threading.Tasks;
using FoodDelivery.Core;
using FoodDelivery.Core.Models.Repositories;
using FoodDelivery.Data.Repositories;

namespace FoodDelivery.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FoodDeliveryDbContext _context;

        private UserRepository _user;
        private RoleRepository _role;
        private RestaurantRepository _restaurant;
        private DishCategoryRepository _dishCategory;
        private DishRepository _dish;
        private OrderRepository _order;
        private OrderedDishRepository _orderedDish;
        private ScoreRepository _score;
        private CityRepository _city;

        public UnitOfWork(FoodDeliveryDbContext context)
        {
            this._context = context;
        }
        public IUserRepository Users => _user ?? new UserRepository(_context);
        public IRoleRepository Roles => _role ?? new RoleRepository(_context);
        public IRestaurantRepository Restaurants => _restaurant ?? new RestaurantRepository(_context);
        public IDishRepository Dishes => _dish ?? new DishRepository(_context);
        public IDishCategoryRepository DishCategories => _dishCategory ?? new DishCategoryRepository(_context);
        public IOrderRepository Orders => _order ?? new OrderRepository(_context);
        public IOrderedDishRepository OrderedDishes => _orderedDish ?? new OrderedDishRepository(_context);
        public IScoreRepository Scores => _score ?? new ScoreRepository(_context);
        public ICityRepository Cities => _city ?? new CityRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        { }
    }
}
