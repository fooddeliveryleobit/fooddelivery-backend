﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Geography;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Data.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(DbContext context)
            : base(context)
        { }


        public new async Task AddAsync(Restaurant restaurant)
        {
            restaurant.Location = new Point(restaurant.Latitude, restaurant.Longitude) { SRID = 4326 };

            var owner = await FoodDeliveryDbContext.Users
                              .SingleOrDefaultAsync(m => m.Id == restaurant.OwnerId);

            if (owner == null)
            {
                throw new InvalidUserException();
            }

            if (owner.RoleId != RoleVariantEnum.Owner)
            {
                throw new InvalidRoleException(owner.RoleId);
            }


            var city = await new CityRepository(Context).GetByIdAsync(restaurant.CityId);

            if (city == null)
            {
                throw new InvalidCityException();
            }


            await base.AddAsync(restaurant);

        }

        public async Task<IEnumerable<Restaurant>> GetAllRestaurantsByOwnerIdAsync(int ownerId)
        {
            var owner = await FoodDeliveryDbContext.Users
                              .SingleOrDefaultAsync(m => m.Id == ownerId);

            if (owner == null)
            {
                throw new InvalidUserException();
            }

            if (owner.RoleId != RoleVariantEnum.Owner)
            {
                throw new InvalidRoleException(owner.RoleId);
            }

            return await FoodDeliveryDbContext.Restaurants
                         .Where(m => m.OwnerId == ownerId)
                         .ToListAsync();
        }

        public async Task<IEnumerable<Restaurant>> GetAllAvaliableRestaurantsToCustomer(int customerId)
        {
            var customer = await new UserRepository(Context)
                                     .GetByIdAsync(customerId);


            if (customer == null)
            {
                throw new InvalidUserException();
            }

            if (customer.RoleId != RoleVariantEnum.Customer)
            {
                throw new InvalidRoleException(customer.RoleId);
            }

            return (await FoodDeliveryDbContext.Restaurants
                          .ToListAsync())
                          .Where(m => Distance.FindDistanceMetres(m.Location, customer.Location) < ValidatorProperties.MaxDistanceMeters);
        }

        public async Task<Restaurant> GetByIdWithOwnerAsync(int id)
        {

            var restaurant = await FoodDeliveryDbContext.Restaurants
                                   .Include(m => m.Owner)
                                   .SingleOrDefaultAsync(m => m.Id == id);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException();
            }

            return restaurant;
        }


        public async Task<IEnumerable<Restaurant>> GetAllRestaurantsByCityIdAsync(string cityId)
        {
            var city = await new CityRepository(Context).GetByIdAsync(cityId);

            if (city == null)
            {
                throw new InvalidCityException();
            }

            return await FoodDeliveryDbContext.Restaurants
                         .Where(m => m.CityId == cityId)
                         .ToListAsync();
        }


        public async Task<bool> DoesRestaurantBelongToUser(int restaurantId, int userId)
        {
            List<Restaurant> ownerRestaurants = (await GetAllRestaurantsByOwnerIdAsync(userId)).ToList();

            return ownerRestaurants.SingleOrDefault(m => m.Id == restaurantId) != null;
        }
        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;

    }
}
