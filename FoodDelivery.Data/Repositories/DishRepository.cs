﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(DbContext context)
            : base(context)
        { }

        public new async Task AddAsync(Dish dishToAdd)
        {
            var dishCategory = await FoodDeliveryDbContext.DishCategories
                                     .SingleOrDefaultAsync(m => m.Id == dishToAdd.IdCategory);

            if (dishCategory == null)
            {
                throw new InvalidDishCategoryException();
            }

            if (dishToAdd.Price <= 0 || dishToAdd.Price > ValidatorProperties.MaxDishCost)
            {
                throw new InvalidDishException("Wrong dish price !");
            }

            await base.AddAsync(dishToAdd);
        }

        public Task<List<Dish>> GetAllWithDishCategoryAsync()
        {
            return FoodDeliveryDbContext.Dishes
                         .Include(m => m.Category)
                         .Include(m => m.Category.Restaurant)
                         .ToListAsync();
        }

        public async Task<Dish> GetByIdWithCategoryAndRestaurantAsync(int id)
        {
            var dish = await FoodDeliveryDbContext.Dishes
                               .Include(m => m.Category)
                               .Include(m => m.Category.Restaurant)
                               .SingleOrDefaultAsync(m => m.Id == id);

            if (dish == null)
            {
                throw new InvalidDishException();
            }

            return dish;
        }

        public async Task<IEnumerable<Dish>> GetAllDishesByCategoryIdAsync(int categoryId)
        {
            var dishCategory = await FoodDeliveryDbContext.DishCategories
                                     .SingleOrDefaultAsync(m => m.Id == categoryId);

            if (dishCategory == null)
            {
                throw new InvalidDishCategoryException();
            }

            return await FoodDeliveryDbContext.Dishes
                        .Include(m => m.Category)
                        .Where(m => m.IdCategory == categoryId)
                        .ToListAsync();
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;

    }
}
