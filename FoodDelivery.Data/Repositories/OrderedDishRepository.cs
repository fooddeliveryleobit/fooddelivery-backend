﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class OrderedDishRepository : Repository<OrderedDish>, IOrderedDishRepository
    {
        public OrderedDishRepository(DbContext context)
            : base(context)
        { }


        public new async Task AddAsync(OrderedDish orderedDish)
        {

            var order = await FoodDeliveryDbContext.Orders
                              .FirstOrDefaultAsync(m => m.Id == orderedDish.OrderId);

            if (order == null)
            {
                throw new InvalidOrderException();
            }

            if (order.Status != OrderStatusEnum.New)
            {
                throw new InvalidOrderException("Can't change delivered order !");
            }


            var dish = await FoodDeliveryDbContext.Dishes
                             .SingleOrDefaultAsync(m => m.Id == orderedDish.DishId);

            if (dish == null)
            {
                throw new InvalidDishException();
            }

            var dishCategory = await FoodDeliveryDbContext.DishCategories
                                     .SingleOrDefaultAsync(m => m.Id == dish.IdCategory);


            var restaurant = await FoodDeliveryDbContext.Restaurants
                                   .SingleOrDefaultAsync(m => m.Id == dishCategory.IdRestaurant);


            var orderRestaurant = await FoodDeliveryDbContext.Restaurants
                                        .SingleOrDefaultAsync(m => m.Id == order.RestaurantId);


            var isAlreadyExist = await FoodDeliveryDbContext.OrderedDishes
                                       .FirstOrDefaultAsync(m => m.DishId == orderedDish.DishId && m.OrderId == orderedDish.OrderId);

            if (isAlreadyExist != null)
            {
                throw new InvalidOrderedDishException("Ordered dish already added !");
            }


            if (restaurant.Id != orderRestaurant.Id)
            {
                throw new InvalidDishException($"Wrong dish id = {dish.Id}");
            }

            if (orderedDish.Count < 1 || orderedDish.Count > ValidatorProperties.MaxCountOfDishes)
            {
                throw new InvalidOrderedDishException("Wrong count of dishes !");
            }

            await base.AddAsync(orderedDish);
        }

        public Task<List<OrderedDish>> GetAllOrderedDishesAsync()
        {
            return FoodDeliveryDbContext.OrderedDishes
                         .Include(m => m.Dish)
                         .Include(m => m.Order)
                         .ToListAsync();
        }

        public async Task<IEnumerable<OrderedDish>> GetAllOrderedDishesByOrderIdAsync(int orderId)
        {
            var order = await new OrderRepository(Context).GetByIdAsync(orderId);

            if (order == null)
            {
                throw new InvalidOrderException();
            }

            return await FoodDeliveryDbContext.OrderedDishes
                         .Include(m => m.Dish)
                         .Where(m => m.OrderId == orderId)
                         .ToListAsync();
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}
