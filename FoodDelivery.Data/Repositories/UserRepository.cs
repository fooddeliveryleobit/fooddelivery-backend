﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;
using FoodDelivery.Core.Exceptions;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context)
            : base(context)
        { }

        public Task<List<User>> GetAllWithRoleAsync()
        {
            return  FoodDeliveryDbContext.Users
                         .Include(m => m.Role)
                         .ToListAsync();
        }

        public async Task<User> GetByIdWithRoleAsync(int id)
        {
            var user =   await FoodDeliveryDbContext.Users
                         .Include(m => m.Role)
                         .SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
            {
                throw new InvalidUserException();
            }

            return user;
        }
      
        public new async Task AddAsync(User user)
        {
            if (await IsEmailAlreadyUsed(user.Email) == true)
            {
                throw new InvalidUserException("Email is already used!");
            }

            user.Location = new Point(user.Latitude, user.Longitude) { SRID = 4326 };

            var role = await new RoleRepository(Context).GetByIdAsync(user.RoleId);

            if (role == null)
            {
                throw new InvalidRoleException();
            }

            var city = await new CityRepository(Context).GetByIdAsync(user.CityId);

            if (city == null)
            {
                throw new InvalidCityException();
            }

            await base.AddAsync(user);

        }

        public new async Task Remove(User user)
        {
            OrderRepository orderRepository = new OrderRepository(Context);

            var customerOrders = await orderRepository.Find(m => m.CustomerId == user.Id);
             
            var deliverOrders = await orderRepository.Find(m => m.DeliverId == user.Id);


            await DeleteOrders(customerOrders);
            await DeleteOrders(deliverOrders);

            await base.Remove(user);

        }
        private async Task DeleteOrders(IEnumerable<Order> orders)
        {
            OrderRepository ordersToRemove = new OrderRepository(Context);

            foreach (var val in orders)
            {
                await ordersToRemove.Remove(val);
            }

        }

        public async Task<bool> IsDeliverBusy(int deliverId)
        {
            User deliver = await GetByIdAsync(deliverId);


            if (deliver == null)
            {
                throw new InvalidUserException();
            }

            if (!IsUserInRole(deliver, RoleVariantEnum.Deliver))
            {
                throw new InvalidRoleException(deliver.RoleId);
            }

            var HasCurrentOrder = await new OrderRepository(FoodDeliveryDbContext)
                                            .Find(m => m.DeliverId == deliver.Id && m.Status == OrderStatusEnum.AcceptedByDeliver);

            return HasCurrentOrder.Count > 0;
        }

        public static bool IsUserInRole(User user, RoleVariantEnum roleId)
        {
            if (user == null)
            {
                throw new InvalidUserException();
            }

            return user.RoleId == roleId;
        }
      
        public async Task<bool> IsEmailAlreadyUsed (string email)
        {
            var isEmailAlreadyUsed = await  FoodDeliveryDbContext.Users
                                            .FirstOrDefaultAsync(m => m.Email == email);

            return isEmailAlreadyUsed != null;
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;

    }
}
