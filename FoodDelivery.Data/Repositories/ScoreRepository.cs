﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class ScoreRepository : Repository<Score>, IScoreRepository
    {
        public ScoreRepository(DbContext context)
            :base(context)
        { }

        public new async Task AddAsync(Score score)
        {
            UserRepository userRepository = new UserRepository(Context);

            var receiver = await userRepository.GetByIdAsync(score.ReceiverId);
               
            var sender = await userRepository.GetByIdAsync(score.SenderId);

            var order = await new OrderRepository(Context).GetByIdAsync(score.OrderId);
                

            if (order == null)
            {
                throw new InvalidOrderException();
            }

            var customer = receiver?.RoleId == RoleVariantEnum.Customer ? receiver : sender;
            var deliver = receiver?.RoleId == RoleVariantEnum.Deliver ? receiver : sender;
            
            if (customer == null || sender == null)
            {
                throw new InvalidUserException();
            }


            if (customer.RoleId != RoleVariantEnum.Customer || deliver.RoleId != RoleVariantEnum.Deliver)
            {
                throw new InvalidUserException(customer, deliver);
            }

            if (order.CustomerId != customer.Id || order.DeliverId != deliver.Id)
            {
                throw new InvalidOrderException("Wrong customer/deliver !");
            }

            var IsExist = await FoodDeliveryDbContext.Scores
                                .SingleOrDefaultAsync(m => m.ReceiverId == score.ReceiverId && m.SenderId == score.SenderId && m.OrderId == score.OrderId);

            if (IsExist != null)
            {
                throw new InvalidScoreException("Restrict more than 1 score !");
            }

            if (score.Value < ValidatorProperties.MinScore || score.Value > ValidatorProperties.MaxScore)
            {
                throw new InvalidScoreException($"Wrong score value : {score.Value} !");
            }

            await base.AddAsync(score);
           
        }

        public async Task<IEnumerable<Score>> GetAllReceivedScoresByUserIdAsync(int userId)
        {

            User user = await new UserRepository(Context).GetByIdAsync(userId);

            if (user == null)
            {
                throw new InvalidUserException();
            }

            if (!UserRepository.IsUserInRole(user, RoleVariantEnum.Deliver) && !UserRepository.IsUserInRole(user, RoleVariantEnum.Customer))
            {
                throw new InvalidRoleException(user.RoleId);
            }

            return await FoodDeliveryDbContext.Scores
                         .Include(m => m.Sender)
                         .Where(m => m.ReceiverId == user.Id)
                         .ToListAsync();
        }
        public async Task<IEnumerable<Score>> GetAllSentScoresByUserIdAsync(int userId)
        {
            UserRepository userRepository = new UserRepository(Context);

            User user = await userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new InvalidUserException();
            }

            if (!UserRepository.IsUserInRole(user, RoleVariantEnum.Deliver) && !UserRepository.IsUserInRole(user, RoleVariantEnum.Customer))
            {
                throw new InvalidRoleException(user.RoleId);
            }

            return await FoodDeliveryDbContext.Scores
                         .Include(m => m.Receiver)
                         .Where(m => m.SenderId == user.Id)
                         .ToListAsync();
        }



        public Task<List<Score>> GetAllWithUsersAsync()
        {
            return FoodDeliveryDbContext.Scores
                         .Include(m => m.Receiver)
                         .Include(m => m.Sender)
                         .ToListAsync();
        }

        public async Task<Score> GetScoreByIdAsync(int senderId, int receiverId, int orderId)
        {
           var score = await  FoodDeliveryDbContext.Scores
                         .Include(m => m.Receiver)
                         .Include(m => m.Sender)
                         .SingleOrDefaultAsync(m => m.SenderId == senderId && m.ReceiverId == receiverId && m.OrderId == orderId);
      
         /*   if (score == null)
            {
                throw new InvalidScoreException();
            }
          */
            return score;
        }

        public async Task<double> GetAverageScoreByUserIdAsync(int userId)
        {
            UserRepository userRepository = new UserRepository(Context);

            User user = await userRepository.GetByIdAsync(userId);

            if (user == null)
            {
                throw new InvalidUserException();
            }

            if (!UserRepository.IsUserInRole(user, RoleVariantEnum.Deliver) && !UserRepository.IsUserInRole(user, RoleVariantEnum.Customer))
            {
                throw new InvalidRoleException(user.RoleId);
            }

            int IsScoreExisting = await FoodDeliveryDbContext.Scores
                                         .CountAsync(m => m.ReceiverId == userId);

            if (IsScoreExisting == 0)
                return Score.MaxScore;


            return await FoodDeliveryDbContext.Scores
                         .Where(m => m.ReceiverId == userId)
                         .AverageAsync(m => m.Value);
        }


        FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}
