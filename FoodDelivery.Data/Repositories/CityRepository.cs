﻿using System.Threading.Tasks;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(DbContext context)
            : base(context)
        { }

        public async new Task AddAsync(City city)
        {
            var cityToAdd = await GetByIdAsync(city.CityName);

            if (cityToAdd != null)
            {
                throw new InvalidCityException("City has already existed !");
            }

            await base.AddAsync(city);
        }
        public async Task<City> GetByIdAsync(string id)
        {
            var city = await FoodDeliveryDbContext.Cities
                              .SingleOrDefaultAsync(m => m.CityName == id);


            return city;
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}
