﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext context)
            : base(context)
        { }

        public new async Task AddAsync(Role role)
        {
            if (!Enum.IsDefined(typeof(RoleVariantEnum), role.Id))
            {
                throw new InvalidRoleException("Wrong role !");
            }

            var IsRoleExist = await FoodDeliveryDbContext.Roles
                                    .CountAsync(m => m.Id == role.Id);

            if (IsRoleExist > 0)
            {
                throw new InvalidRoleException("Role exists !");
            }


            await base.AddAsync(role);
        }

        public new async Task Remove(Role roleToDelete)
        {
            UserRepository usersToRemove = new UserRepository(Context);

            var usersToDelete = await  FoodDeliveryDbContext.Users
                                       .Where(m => m.RoleId == roleToDelete.Id)
                                       .ToListAsync();


            foreach (var val in usersToDelete)
            {
                await usersToRemove.Remove(val);
            }

            await base.Remove(roleToDelete);
        }

        public Task<Role> GetByIdAsync(RoleVariantEnum roleId)
        {
            return FoodDeliveryDbContext.Roles
                    .SingleOrDefaultAsync(m => (m.Id == roleId));
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}