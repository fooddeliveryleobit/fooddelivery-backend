﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data.Repositories
{
    public class DishCategoryRepository : Repository<DishCategory>, IDishCategoryRepository
    {
        public DishCategoryRepository(DbContext context)
            : base(context)
        { }

        public new async Task AddAsync(DishCategory dishCategoryToAdd)
        {
            var restaurant = await FoodDeliveryDbContext.Restaurants
                                          .SingleOrDefaultAsync(m => m.Id == dishCategoryToAdd.IdRestaurant);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException();
            }

            await base.AddAsync(dishCategoryToAdd);
        }

        public async Task<DishCategory> GetByIdWithRestaurantAsync(int id)
        {
            var dishCategories = await FoodDeliveryDbContext.DishCategories
                                      .Include(m => m.Restaurant)
                                      .SingleOrDefaultAsync(m => m.Id == id);

            if (dishCategories == null)
            {
                throw new InvalidDishCategoryException();
            }

            return dishCategories;

        }
        public async Task<IEnumerable<DishCategory>> GetAllDishCategoryByRestaurantId(int restaurantId)
        {
            var restaurant = await FoodDeliveryDbContext.Restaurants
                                   .SingleOrDefaultAsync(m => m.Id == restaurantId);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException();
            }

            return await FoodDeliveryDbContext.DishCategories
                         .Include(m => m.Restaurant)
                         .Where(m => m.IdRestaurant == restaurantId)
                         .ToListAsync();
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}
