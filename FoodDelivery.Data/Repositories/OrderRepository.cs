﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Api.Validators;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Geography;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Models.Repositories;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Data.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(DbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Order>> GetAllOrdersAsync()
        {
            return await FoodDeliveryDbContext.Orders
                         .Include(m => m.Restaurant)
                         .Include(m => m.Deliver)
                         .Include(m => m.Customer)
                         .ToListAsync();
        }

        public async Task<Order> GetOrderWithAdditionalInfoByIdAsync(int id)
        {

            var order = await FoodDeliveryDbContext.Orders
                             .Include(m => m.Restaurant)
                             .Include(m => m.Deliver)
                             .Include(m => m.Customer)
                             .SingleOrDefaultAsync(m => m.Id == id);

            if (order == null)
            {
                throw new InvalidOrderException();
            }

            return order;
        }

        public new async Task Remove(Order order)
        {
            var orderedDishesToDelete = await FoodDeliveryDbContext.OrderedDishes
                                              .Where(m => m.OrderId == order.Id)
                                              .ToListAsync();

            foreach (var val in orderedDishesToDelete)
            {
                FoodDeliveryDbContext.OrderedDishes.Remove(val);
            }

            await base.Remove(order);
        }


        public async new Task<Order> AddAsync(Order order)
        {
            order.Status = OrderStatusEnum.New;
            order.DeliverId = null;


            var customer = await new UserRepository(Context)
                                     .GetByIdAsync(order.CustomerId);

            if (customer == null)
            {
                throw new InvalidUserException($"Customer doesn't exist !");
            }

            if (customer.Location.X == default && customer.Location.Y == default)
            {
                throw new InvalidUserException($"Customer doesn't set destination !");
            }

            order.Location = new Point(customer.Location.X, customer.Location.Y) { SRID = 4326 };


            var restaurant = await new RestaurantRepository(Context)
                                         .GetByIdAsync(order.RestaurantId);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException($"Restaurant doesn't exist !");
            }


            if (!UserRepository.IsUserInRole(customer, RoleVariantEnum.Customer))
            {
                throw new InvalidRoleException(customer.RoleId);
            }

            await base.AddAsync(order);

            return order;
        }

        public async Task<IEnumerable<Order>> GetAllAvaliableOrdersToDeliverAsync(int deliverId)
        {
            UserRepository userRepository = new UserRepository(Context);

            User deliver = await userRepository.GetByIdAsync(deliverId);

            if (deliver == null)
            {
                throw new InvalidUserException();
            }

            if (deliver.RoleId != RoleVariantEnum.Deliver)
            {
                throw new InvalidRoleException(deliver.RoleId);
            }

            if (await userRepository.IsDeliverBusy(deliverId) == true)
            {
                return new List<Order>();
               // throw new InvalidUserException("Can't have more than 1 avaliable orders !");
            }

            return (await FoodDeliveryDbContext.Orders
                 .Include(m => m.Customer)
                 .Include(m => m.Restaurant)
                 .ToListAsync())
                 .Where(m => m.Status == OrderStatusEnum.New && Distance.FindDistanceMetres(m.Customer.Location, deliver.Location) < ValidatorProperties.MaxDistanceMeters);
        }

        public async Task<IEnumerable<Order>> GetAllUserOrdersByStatusAsync(int userId, RoleVariantEnum role, OrderStatusEnum dishStatusEnum)
        {
            User user = await new UserRepository(Context)
                                 .GetByIdAsync(userId);

            if (user == null)
            {
                throw new InvalidUserException();
            }

            if (user.RoleId != role)
            {
                throw new InvalidRoleException(user.RoleId);
            }

            if (role != RoleVariantEnum.Customer && role != RoleVariantEnum.Deliver)
            {
                throw new InvalidRoleException("Orders can have only customer or deliver !");
            }

            return await FoodDeliveryDbContext.Orders
                         .Include(m => m.Customer)
                         .Include(m => m.Deliver)
                         .Include(m => m.Restaurant)
                         .Where(m => (m.DeliverId == userId || m.CustomerId == userId) && m.Status == dishStatusEnum)
                         .ToListAsync();
        }

        public async  Task<List<Order>> GetAllOrdersByRestaurantIdAsync(int restaurantId, OrderStatusEnum status)
        {
            var restaurant = await FoodDeliveryDbContext.Restaurants
                                    .SingleOrDefaultAsync(m => m.Id == restaurantId);

            if (restaurant == null)
            {
                throw new InvalidRestaurantException();
            }

            return await FoodDeliveryDbContext.Orders
                         .Include(m => m.Restaurant)
                         .Include(m => m.Deliver)
                         .Include(m => m.Customer)
                         .Where(m => m.RestaurantId == restaurantId && m.Status == status)
                         .ToListAsync();
        }
        
        public async Task<decimal> GetTotalOrderCost(int orderId)
        {
            var orderedDishes = await new OrderedDishRepository(Context)
                                          .GetAllOrderedDishesByOrderIdAsync(orderId);

            decimal totalCost = 0;

            (orderedDishes as List<OrderedDish>).ForEach(m => { totalCost += m.Dish.Price * m.Count; });

            return totalCost + ValidatorProperties.CostForDelivery;                  
        }


        public Task<Order> GetOrderWithRestaurantById(int orderId)
        {
            return FoodDeliveryDbContext.Orders
                    .Include(m => m.Customer)
                    .Include(m => m.Deliver)
                    .Include(m => m.Restaurant)
                   .SingleOrDefaultAsync(m => m.Id == orderId);
        }

        private FoodDeliveryDbContext FoodDeliveryDbContext => Context as FoodDeliveryDbContext;
    }
}

