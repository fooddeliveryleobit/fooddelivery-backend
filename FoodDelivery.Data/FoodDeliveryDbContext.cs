﻿using FoodDelivery.Core.Models;
using FoodDelivery.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Data
{
    public class FoodDeliveryDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<DishCategory> DishCategories { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedDish> OrderedDishes { get; set; }
        public DbSet<City> Cities { get; set; }

        public FoodDeliveryDbContext(DbContextOptions <FoodDeliveryDbContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new UserConfiguration());

            builder
                .ApplyConfiguration(new RoleConfiguration());

            builder
                .ApplyConfiguration(new RestaurantConfiguration());

            builder
                .ApplyConfiguration(new DishConfiguration());

            builder
                .ApplyConfiguration(new DishCategoryConfiguration());

            builder
                .ApplyConfiguration(new OrderConfiguration());

            builder
                .ApplyConfiguration(new OrderedDishConfiguration());

            builder
                .ApplyConfiguration(new ScoreConfiguration());

            builder
                .ApplyConfiguration(new CityConfiguration());
        }
    }
}
