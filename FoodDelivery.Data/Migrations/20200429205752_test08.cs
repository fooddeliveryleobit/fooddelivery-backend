﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDelivery.Data.Migrations
{
    public partial class test08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Picture",
                table: "Dishes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Picture",
                table: "Dishes");
        }
    }
}
