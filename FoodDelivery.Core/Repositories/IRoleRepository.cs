using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IRoleRepository : IRepository<Role>
    {
        Task<Role> GetByIdAsync(RoleVariantEnum roleId);
    }

}