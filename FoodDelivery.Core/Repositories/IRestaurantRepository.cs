using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        Task<Restaurant> GetByIdWithOwnerAsync(int id);
        Task<IEnumerable<Restaurant>> GetAllRestaurantsByOwnerIdAsync(int ownerId);
        Task<IEnumerable<Restaurant>> GetAllAvaliableRestaurantsToCustomer(int customerId);
        Task<IEnumerable<Restaurant>> GetAllRestaurantsByCityIdAsync(string cityId);
    }
}