﻿using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface ICityRepository : IRepository<City>
    {
        Task<City> GetByIdAsync(string id);
    }
}
