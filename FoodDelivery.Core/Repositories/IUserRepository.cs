using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<bool> IsDeliverBusy(int deliverId);
        Task<List<User>> GetAllWithRoleAsync();
        Task<User> GetByIdWithRoleAsync(int id);
        Task<bool> IsEmailAlreadyUsed(string email);
    }

}