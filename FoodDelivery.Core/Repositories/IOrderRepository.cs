using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        new Task<Order> AddAsync(Order order);
        Task<IEnumerable<Order>> GetAllOrdersAsync();
        Task<Order> GetOrderWithAdditionalInfoByIdAsync(int id);
        Task<IEnumerable<Order>> GetAllAvaliableOrdersToDeliverAsync(int userId);
        Task<IEnumerable<Order>> GetAllUserOrdersByStatusAsync(int userId, RoleVariantEnum role, OrderStatusEnum dishStatusEnum);
        Task<List<Order>> GetAllOrdersByRestaurantIdAsync(int restaurantId, OrderStatusEnum status);
        Task<decimal> GetTotalOrderCost(int orderId);
        Task<Order> GetOrderWithRestaurantById(int orderId);
    }

}