using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IOrderedDishRepository : IRepository<OrderedDish>
    {
        Task<List<OrderedDish>> GetAllOrderedDishesAsync();
        Task<IEnumerable<OrderedDish>> GetAllOrderedDishesByOrderIdAsync(int orderId);
    }

}