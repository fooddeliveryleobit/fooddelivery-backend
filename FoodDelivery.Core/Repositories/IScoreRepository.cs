using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IScoreRepository : IRepository<Score>
    {
        Task<List<Score>> GetAllWithUsersAsync();
        Task<Score> GetScoreByIdAsync(int senderId, int receiverId, int orderId);
        Task<IEnumerable<Score>> GetAllSentScoresByUserIdAsync(int userId);
        Task<IEnumerable<Score>> GetAllReceivedScoresByUserIdAsync(int userId);
        Task<double> GetAverageScoreByUserIdAsync(int userId);
    }

}