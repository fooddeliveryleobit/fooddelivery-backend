using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IDishCategoryRepository : IRepository<DishCategory>
    {
        Task<IEnumerable<DishCategory>> GetAllDishCategoryByRestaurantId(int restaurantId);
        Task<DishCategory> GetByIdWithRestaurantAsync(int id);
    }

}