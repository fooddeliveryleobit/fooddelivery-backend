using System.Collections.Generic;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Models.Repositories
{
    public interface IDishRepository : IRepository<Dish>
    {
        Task<List<Dish>> GetAllWithDishCategoryAsync();
        Task<Dish> GetByIdWithCategoryAndRestaurantAsync(int id);
        Task<IEnumerable<Dish>> GetAllDishesByCategoryIdAsync(int categoryId);
    }

}