﻿using FoodDelivery.Core.Services;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;


namespace FoodDelivery.Core.Monitoring
{
    public class CustomAppInsightTelemetry : ICustomAppInsightTelemetry
    {
        private readonly TelemetryClient _telemetryClient;
        private readonly string _userName;
        public CustomAppInsightTelemetry(TelemetryConfiguration telemetryConfiguration)
        {
            _telemetryClient = new TelemetryClient(telemetryConfiguration);
            _userName = "admin";
        }
        
        public void TrackCustomEventTelemetry(HttpRequest Request)
        {
            string userInfo = "User : ";

            if (Request.HttpContext.User.Identity.IsAuthenticated)
                (Request.HttpContext.User.Claims as IEnumerable<Claim>).ToList()
                    .ForEach(m => userInfo += m.Type + ": " + m.Value + "; ");
            else
                userInfo += "is not authenticated";


            string dataInfo = $"Protocol = {Request.Protocol}; ";
            var evt = new EventTelemetry($"CUSTOM EVENT(1) : {userInfo} ");
            evt.Context.User.Id = _userName;
            _telemetryClient.TrackEvent(evt);
        }

        public void TrackCustomMetricTelemetry(HttpRequest Request)
        {
            var metric = new MetricTelemetry($"TEST METRIC Protocol : {Request.Protocol}", DateTime.Now.Millisecond);
           
            metric.Context.User.Id = _userName;
            _telemetryClient.TrackMetric(metric);
        }
        public void TrackCustomTraceTelemetry(string stackTrace)
        {
            var trace = new TraceTelemetry( $"TEST TRACE : {stackTrace}");

            trace.Context.User.Id = _userName;
            _telemetryClient.TrackTrace(trace);
        }
        public void TrackCustomEventTelemetryCookies(HttpRequest Request)
        {
            var evt = new EventTelemetry($"CUSTOM EVENT(2) Header keys : {JsonConvert.SerializeObject(Request.Headers.Keys)}; Protocol : {Request.Protocol}");
            evt.Context.User.Id = _userName;
            _telemetryClient.TrackEvent(evt);
        }

    
        public void TrackAllTelemetry(HttpRequest Request)
        {
            TrackCustomEventTelemetry(Request);
            TrackCustomEventTelemetryCookies(Request);
            TrackCustomMetricTelemetry(Request);
        }
    }
}
