﻿using Microsoft.AspNetCore.Http;


namespace FoodDelivery.Core.Monitoring
{
    public interface ICustomAppInsightTelemetry
    {
        void TrackCustomMetricTelemetry(HttpRequest Request);
        void TrackCustomEventTelemetry(HttpRequest Request);
        void TrackCustomEventTelemetryCookies(HttpRequest Request);
        void TrackCustomTraceTelemetry(string stackTrace);
        void TrackAllTelemetry(HttpRequest Request);
    }
}
