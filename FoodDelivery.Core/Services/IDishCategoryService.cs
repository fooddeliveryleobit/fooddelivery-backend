﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Services
{
    public interface IDishCategoryService 
    {
        Task<DishCategory> CreateDishCategoryAsync(int restaurantId, DishCategory newDishCategory);
        Task<DishCategory> UpdateDishCategoryAsync(int dishCategoryId, IDishCategoryChanges dishCategoryChanges);
        Task DeleteDishCategoryAsync(int dishCategoryId);
        Task<DishCategory> GetDishCategoryWithRestaurantByIdAsync(int categoryId);
        Task<IEnumerable<DishCategory>> GetAllDishCategoriesAsync(int restaurantId);
    }
}
