﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Service.Interfaces
{
    public interface IRoleService
    {
        Task<Role> CreateRoleAsync(Role newRole);
        Task DeleteRoleAsync(Role role);
        Task<Role> GetRoleByIdAsync(int id);
        Task<IEnumerable<Role>> GetAllRolesAsync();
    }
}
