﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Services
{
    public interface IOrderedDishService
    {
        Task<IEnumerable<OrderedDish>> CreateOrderedDishAsync(IEnumerable<OrderedDish> newOrderedDish);
        Task<IEnumerable<OrderedDish>> GetAllOrderedDishesByOrderIdAsync(int orderId);
    }
}
