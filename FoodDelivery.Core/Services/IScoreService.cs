﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Services
{
    public interface IScoreService
    {
        Task CreateNewScoreAsync(int senderId, int receiverId, int orderId, Score newScore);
        Task<List<Score>> GetAllScoresAsync();
        Task<Score> GetScoreByIdAsync(int senderId, int receiverId, int orderId);
        Task<Score> UpdateScoreAsync(Score scoreToBeUpdatedId, IScoreChanges scoreChanges);
        Task DeleteScoreAsync(Score score);

        Task<IEnumerable<Score>> GetAllRecievedScoresByRecieverId(int receiverId);
        Task<IEnumerable<Score>> GetAllSentScoresBySenderId(int receiverId);
        Task<double> GetAverageScoreByUserIdAsync(int userId);
    }
}
