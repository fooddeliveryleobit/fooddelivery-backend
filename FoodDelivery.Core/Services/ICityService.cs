﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Services
{
    public interface ICityService
    {
        Task DeleteCityAsync(string cityId);
        Task<IEnumerable<City>> CreateCityAsync(IEnumerable<City> newCity);
        Task<City> GetCityByIdAsync(string cityId);
        Task<IEnumerable<City>> GetAllCitiesAsync();
    }
}
