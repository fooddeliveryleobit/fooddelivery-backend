﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Services
{
    public interface IOrderService
    {
        Task<Order> CreateOrderAsync(Order order);
        Task<Order> UpdateOrderAsync(Order orderToBeUpdated, IOrderChanges orderChanges);
        Task DeleteOrderAsync(Order order);
        Task<Order> GetOrderByIdAsync(int orderId);
        Task<IEnumerable<Order>> GetAllOrdersAsync();

        Task<IEnumerable<OrderedDish>> GetAllOrderedDishesAsync(int orderId);

        Task<IEnumerable<Order>> GetAllAvaliableOrdersToDeliverAsync(int userId);

        Task<IEnumerable<Order>> GetAllUserOrdersByStatusAsync(int userId, RoleVariantEnum role, OrderStatusEnum dishStatusEnum);

        Task<Order> CreateNewOrderByCustomer(int customerId, int restaurantId, IEnumerable<OrderedDish> listOfDishes);
        Task DeclineOrderByCustomer(int customerId, int orderId);

        Task<Order> AcceptedOrderByDeliver(int deliverId, int orderId);
        Task<Order> FinishedOrderByDeliver(int deliverId, int orderId);
        Task<IEnumerable<Order>> GetAllOrdersByRestaurantIdAsync(int restaurantId, OrderStatusEnum status);

        Task<decimal> GetTotalOrderCost(int orderId);
        Task<Order> GetOrderWithrestaurantById(int orderId);
    }
}
