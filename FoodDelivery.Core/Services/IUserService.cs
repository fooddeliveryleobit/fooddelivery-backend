﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.PasswordHashing;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Core.Services
{
    public interface IUserService
    {
        Task<User> CreateUserAsync(User newUser, IFormFile picture );
        Task<User> UpdateUserAsync(User userToBeUpdated, IUserChanges userChanges);
        Task DeleteUserAsync(User user);

        Task<User> GetUserByIdAsync(int id);
        Task<IEnumerable<User>> GetAllUsersAsync();   
        Task<bool> IsDeliverBusy(int deliverId);

        Task<User> Authenticate(User userToAuthorize, IPasswordHasher passwordHasher);
        ClaimsIdentity GetClaims(User user);
    }
}
