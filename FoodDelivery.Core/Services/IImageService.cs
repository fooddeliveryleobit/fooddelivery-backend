﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Services
{
    public enum StorageEnum
    {
        restaurant = 1,
        dish,
        user
    }

    public interface IImageService
    {
       Task<string> UploadImageAsync(IFormFile imageToUpload, StorageEnum storageEnum);
       Task<string> UpdateImageAsync(IFormFile imageToUpload, StorageEnum storageEnum, string oldFileStringPath);
       Task DeleteFileAsync(string path);
    }
}
