﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Core.Services
{
    public interface IRestaurantService
    {
        Task<Restaurant> CreateRestaurantAsync(int ownerId, Restaurant newRestaurant, IFormFile picture);
        Task<Restaurant> UpdateRestaurantAsync(int restaurantId, IRestaurantChanges restaurantChanges);
        Task DeleteRestaurantAsync(int restaurantId);
        Task<Restaurant> GetRestaurantByIdAsync(int restaurantId);
        Task<IEnumerable<Restaurant>> GetAllRestaurantsAsync();
        Task<IEnumerable<Restaurant>> GetAllRestaurantsByOwnerIdAsync(int ownerId);
        Task<IEnumerable<Restaurant>> GetAllAvaliableRestaurantsToCustomer(int customerId);
        Task<IEnumerable<Restaurant>> GetAllRestaurantsByCityIdAsync(string cityId);
    }
}
