﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Service.Interfaces
{
    public interface IDishService
    {
        Task<Dish> CreateDishAsync(int dishCategoryId, Dish newDish, IFormFile picture);
        Task<Dish> UpdateDishAsync(int dishId, IDishChanges dishChanges);
        Task DeleteDishAsync(int dishId);
        Task<IEnumerable<Dish>> GetAllDishesAsync();
        Task<Dish> GetDishByIdAsync(int id);
        Task<IEnumerable<Dish>> GetAllDishesByCategoryIdAsync(int categoryId);
        Task<Dish> GetByIdWithCategoryAndRestaurantAsync(int id);
    }
}
