﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.PasswordHashing;
using FoodDelivery.Core.Services;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Changes
{
    public interface IUserChanges //: IChangeable<User>
    {
        Task<User> ApplyChanges(User userToUpdate, IImageService imageService, IPasswordHasher passwordHasher);
    }
}
