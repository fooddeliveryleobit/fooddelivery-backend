﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Changes
{
    public interface IRoleChanges : IChangeable<Role>
    {
    }
}
