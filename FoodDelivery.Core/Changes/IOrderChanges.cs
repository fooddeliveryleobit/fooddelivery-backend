﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Changes
{
    public interface IOrderChanges : IChangeable<Order>
    {
        int? DeliverId { get; set; }
        OrderStatusEnum Status { get; set; }
    }
}
