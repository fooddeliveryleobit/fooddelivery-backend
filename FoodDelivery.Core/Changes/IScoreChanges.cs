﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Changes
{
    public interface IScoreChanges : IChangeable<Score>
    {
    }
}
