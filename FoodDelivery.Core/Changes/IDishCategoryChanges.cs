﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Changes
{
    public interface IDishCategoryChanges : IChangeable<DishCategory>
    {
    }
}
