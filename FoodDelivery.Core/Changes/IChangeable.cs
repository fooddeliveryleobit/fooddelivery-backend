﻿

namespace FoodDelivery.Core.Changes
{
    public interface IChangeable<T> where T : class
    {
      T ApplyChanges(T entetityToCange);
    }
}
