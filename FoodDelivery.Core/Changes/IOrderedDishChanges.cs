﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Changes
{
    public interface IOrderedDishChanges : IChangeable<OrderedDish>
    {
    }
}
