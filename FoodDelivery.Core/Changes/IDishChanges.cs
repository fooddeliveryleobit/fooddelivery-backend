﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Changes
{
    public interface IDishChanges //: IChangeable<Task<Dish>>
    {
        Task<Dish> ApplyChanges(Dish DishToBeUpdated, IImageService imageService);
    }
}
