﻿using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using System.Threading.Tasks;

namespace FoodDelivery.Core.Changes
{
    public interface IRestaurantChanges // : IChangeable<Task<Restaurant>>
    {
        Task<Restaurant> ApplyChanges(Restaurant restaurantToUpdate, IImageService imageService);
    }
}
