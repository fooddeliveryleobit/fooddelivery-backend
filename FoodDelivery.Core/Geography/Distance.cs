﻿using System;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Core.Geography
{
    public static class Distance
    {
        public static double FindDistanceMetres(Point startPoint, Point endPoint)
        {

            var R = 6371e3; // metres
            var φ1 = (Math.PI / 180) * startPoint.X;
            var φ2 = (Math.PI / 180) * endPoint.X;

            var Δφ = (Math.PI / 180) * (startPoint.X - endPoint.X);
            var Δλ = (Math.PI / 180) * (startPoint.Y - endPoint.Y);

            var a = Math.Sin(Δφ / 2) * Math.Sin(Δφ / 2) +
                      Math.Cos(φ1) * Math.Cos(φ2) *
                      Math.Sin(Δλ / 2) * Math.Sin(Δλ / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return R * c; //metres
        }
    }
}
