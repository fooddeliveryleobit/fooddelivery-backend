﻿

namespace FoodDelivery.Api.Validators
{
    public class ValidatorProperties
    {
        public const int MinCountSymbolsForPassword = 8;
        public const int MaxCountSymbolsForString = 50;
        public const int MaxCountSymbolsForText = 10000;
        public const int MaxCountSymbolsForPhoneNumber = 15;
        public const string RegExForNames = @"^[A-Z]{1}[a-z]+[a-zA-z-]*$";
        public const string RegExForEmails = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        public const string RegExForPhoneNumbers = @"^[+]{1}[0-9]{10,15}$";
        public const double MaxDistanceMeters = 3000;
        public const int MaxScore = 5;
        public const int MinScore = 1;
        public const decimal CostForDelivery = 30;
        public const int MaxCountOfDishes = 50;
        public const decimal MaxDishCost = 100000;
    }
}
