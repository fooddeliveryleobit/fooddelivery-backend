

namespace FoodDelivery.Core.Models
{
    public class Score 
    {
       public int SenderId { get; set;}
       public User Sender {get; set;}
       public int ReceiverId {get; set;}
       public User Receiver {get; set;}
       public int OrderId { get; set; }
       public Order Order { get; set; }
       public double Value {get; set;}

       public const double MaxScore = 5;
    }
}