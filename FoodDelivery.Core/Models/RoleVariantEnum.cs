﻿

namespace FoodDelivery.Core.Models
{
    public enum RoleVariantEnum
    {
        Customer = 1,
        Deliver,
        Owner,
        Admin
    }

}
