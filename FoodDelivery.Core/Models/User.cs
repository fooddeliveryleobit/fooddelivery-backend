using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Core.Models
{
    public class User
    {
        public User()
        {
            RecievedScores = new Collection<Score>();

            SentScores = new Collection<Score>();

            Restaurants = new Collection<Restaurant>();

            CustomerOrders = new Collection<Order>();

            DeliverOrders = new Collection<Order>();
        }
        public int Id {get; set;}
        public string Name {get; set;}
        public string Email {get; set;}
        public string Password { get; set; }
        public City City { get; set; }
        public string CityId {get; set;}
        public Point Location { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
        public string Phone { get; set; }
        public RoleVariantEnum RoleId { get; set; }
        public Role Role {get; set;}    
        public ICollection<Order> CustomerOrders { get; set; }
        public ICollection<Order> DeliverOrders { get; set; }
        public ICollection<Restaurant> Restaurants { get; set; }
        public ICollection<Score> RecievedScores {get; set;}
        public ICollection<Score> SentScores {get; set;}


        public string Token { get; set; }
    }

}