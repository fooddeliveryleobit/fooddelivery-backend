using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FoodDelivery.Core.Models
{
    public class Order 
    {
        public Order ()
        {
            OrderedDish = new Collection<OrderedDish>();
            Scores = new Collection<Score>();
        }
        public int Id {get; set;}
        public int CustomerId { get; set; }
        public User Customer {get; set;}
        public int RestaurantId { get; set; }
        public Restaurant Restaurant {get; set;}
        public int? DeliverId { get; set; }
        public User Deliver {get; set;}
        public DateTime Date {get; set;}
        public OrderStatusEnum Status { get; set; }

        public Point Location { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public ICollection <OrderedDish> OrderedDish {get; set;}       
        public ICollection<Score> Scores { get; set; }
    }
}