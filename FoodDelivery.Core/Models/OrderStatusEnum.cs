﻿

namespace FoodDelivery.Core.Models
{
    public enum OrderStatusEnum
    {
        New = 1,
        AcceptedByDeliver,
        Finished
    }
}
