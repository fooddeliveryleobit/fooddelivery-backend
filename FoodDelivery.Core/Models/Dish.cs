using System.Collections.Generic;

namespace FoodDelivery.Core.Models
{
    public class Dish 
    {
        public int Id {get; set;}
        public int IdCategory { get; set; }
        public DishCategory Category {get; set;}
        public decimal Price {get; set;}
        public string Name { get; set; }
        public string Description {get; set;}
        public string Picture { get; set; }
        public ICollection<OrderedDish> Orders { get; set; }
    }
}