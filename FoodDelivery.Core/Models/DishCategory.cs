using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FoodDelivery.Core.Models
{
    public class DishCategory 
    {
        public DishCategory()
        {
            Dishes = new Collection <Dish> ();
        }
        public int Id {get; set;}
        public string Name {get; set;}
        public ICollection<Dish> Dishes {get; set;}
        public int IdRestaurant { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}