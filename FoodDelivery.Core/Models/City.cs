﻿using System.Collections.Generic;

namespace FoodDelivery.Core.Models
{
    public class City
    {
        public string CityName { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<Restaurant> Restaurants { get; set; }
    }
}
