using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FoodDelivery.Core.Models
{
    public class Role
    {
        public Role()
        {
            Users = new Collection<User>();
        }
        public RoleVariantEnum Id {get; set;}

        public ICollection <User> Users { get; set; }
    }
}