using System.Collections.Generic;
using System.Collections.ObjectModel;
using NetTopologySuite.Geometries;

namespace FoodDelivery.Core.Models
{
    public class Restaurant 
    { 
        public Restaurant()
        {
            Orders = new Collection<Order>();
            
            DishCategories = new Collection<DishCategory>();
        }
        public int Id {get; set;}
        public int OwnerId { get; set; }
        public User Owner {get; set;} 
        public string Name {get; set;}
        public City City { get; set; }
        public string CityId {get; set;}
        public Point Location { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
        public ICollection<DishCategory> DishCategories { get; set; }
        public ICollection <Order> Orders {get; set;}
    }
}