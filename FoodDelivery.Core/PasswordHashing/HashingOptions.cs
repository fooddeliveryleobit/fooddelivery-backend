﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.PasswordHashing
{
    public sealed class HashingOptions
    {
        public int Iterations { get; set; } 

        public HashingOptions()
        {
            Iterations = 10000;
        }
    }
}
