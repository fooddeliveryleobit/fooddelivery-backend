﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.PasswordHashing
{
    public interface IPasswordHasher
    {
        string Hash(string password);

        (bool Verified, bool NeedsUpgrade) Check(string hash, string password);
    }
}
