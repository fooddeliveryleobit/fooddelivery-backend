﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Core.Helpers
{
    public class AuthOptions
    {
        public const string ISSUER = "MyAuthServer"; // token creator 
        public const string AUDIENCE = "MyAuthClient"; // audience
        const string KEY = "PKeyq8edW7cZs-JoY3ZhWC9I3UummJ2ZWgQiv_-tBurOhT3dACS6VjNG1FBWI_8G";   // key for encrypting 
        public const int LIFETIME = 3; // lifetime = 3 hours
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
