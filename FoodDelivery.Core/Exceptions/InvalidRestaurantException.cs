﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidRestaurantException : Exception
    {
        public InvalidRestaurantException()
        : base("Restaurant doesn't exist")
        { }

        public InvalidRestaurantException(string message)
      : base(message)
        { }
        public InvalidRestaurantException(Restaurant restaurant)
        : base($"Wrong restaurant : restaurant id = {restaurant.Id}") { }
    }
}
