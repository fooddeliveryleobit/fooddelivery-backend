﻿using System;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Exceptions
{
    public class InvalidUserException : Exception
    {
        public InvalidUserException()
            : base("User doesn't exist!")
        { }

        public InvalidUserException(string message)
            : base(message)
        { }

        public InvalidUserException(User user)
        : base($"Wrong user : user id = {user.Id}")
        { }

        public InvalidUserException(User customer, User deliver)
        : base($"Wrong roles\n : customer id = {customer.Id} \n deliver id = {deliver.Id}")
        { }


    }
}
