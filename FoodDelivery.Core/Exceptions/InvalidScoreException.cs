﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidScoreException :Exception
    {
        public InvalidScoreException()
            : base("Score doesn't exist!")
        { }

        public InvalidScoreException(string message)
            : base(message)
        { }

        public InvalidScoreException(Score score)
            : base($"Wrong score : order id = {score.OrderId}\nreceiver id = {score.ReceiverId}\nsender id = {score.SenderId}")
        { }
    }
}
