﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidRoleException : Exception
    {
        public InvalidRoleException()
            :base("Role doesn't exist!")
        { }

        public InvalidRoleException(string message)
              : base(message)
        { }

        public InvalidRoleException(RoleVariantEnum roleId)
           : base($"Invalid User Role : {roleId.ToString()}")
        { }

    }
}
