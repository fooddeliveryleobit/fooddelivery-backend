﻿using System;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Exceptions
{
    public class InvalidOrderedDishException : Exception
    {
        public InvalidOrderedDishException()
        : base($"OrderedDish doesn't exist!")
        { }

        public InvalidOrderedDishException(string message)
        : base(message)
        { }

        public InvalidOrderedDishException(OrderedDish orderedDish)
        : base($"Wrong ordered dish : order id = {orderedDish.OrderId}, dish id = {orderedDish.DishId}")
        { }
    }

}
