﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidDishCategoryException : Exception
    {
        public InvalidDishCategoryException()
        : base("Dish category doesn't exist!")
        { }

        public InvalidDishCategoryException(string message)
        : base(message)
        { }

        public InvalidDishCategoryException(DishCategory dishCategory)
        : base($"Wrong dish category : dish category id = {dishCategory.Id}") { }
    }
}
