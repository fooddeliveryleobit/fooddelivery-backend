﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidDishException : Exception
    {
        public InvalidDishException()
        : base($"Dish doesn't exist!")
        { }

        public InvalidDishException(string message)
        : base(message)
        { }

        public InvalidDishException(Dish dish)
        : base($"Wrong Dish : dish id = {dish.Id}")
        { }
    }
}
