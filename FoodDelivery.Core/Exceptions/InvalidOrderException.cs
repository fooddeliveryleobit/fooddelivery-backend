﻿using System;
using FoodDelivery.Core.Models;


namespace FoodDelivery.Core.Exceptions
{
    public class InvalidOrderException : Exception
    {
        public InvalidOrderException()
        : base("Order doesn't exist!")
        { }

        public InvalidOrderException(string message)
        : base(message)
        { }
        public InvalidOrderException(Order order)
        : base($"Wrong order : order id = {order.Id}") { }
    }
}
