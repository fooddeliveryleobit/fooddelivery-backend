﻿using System;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Core.Exceptions
{
    public class InvalidCityException : Exception
    {
        public InvalidCityException()
       : base("City doesn't exist!")
        { }

        public InvalidCityException(string message)
        : base(message)
        { }

        public InvalidCityException(City city)
        : base($"Wrong city category : dish category id = {city.CityName}") { }

    }
}
