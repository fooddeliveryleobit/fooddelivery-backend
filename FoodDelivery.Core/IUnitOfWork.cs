﻿using System;
using System.Threading.Tasks;
using FoodDelivery.Core.Models.Repositories;

namespace FoodDelivery.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IRoleRepository Roles { get; }
        IRestaurantRepository Restaurants { get; }
        IDishRepository Dishes { get; }
        IDishCategoryRepository DishCategories { get; }
        IOrderRepository Orders { get; }
        IScoreRepository Scores { get; }
        IOrderedDishRepository OrderedDishes { get; }
        ICityRepository Cities { get; }
        Task<int> CommitAsync();
    }
}
