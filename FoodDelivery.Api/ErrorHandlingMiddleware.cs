﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Monitoring;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace FoodDelivery.Api
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICustomAppInsightTelemetry _customAppInsightTelemetry;

        public ErrorHandlingMiddleware(RequestDelegate next, ICustomAppInsightTelemetry customAppInsightTelemetry)
        {
            _next = next;
            _customAppInsightTelemetry = customAppInsightTelemetry;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _customAppInsightTelemetry.TrackCustomTraceTelemetry(ex.StackTrace);
                await HandleExceptionAsync(context, ex).ConfigureAwait(false);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError;

            if (ex is InvalidDishException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidScoreException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidRoleException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidUserException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidDishCategoryException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidRestaurantException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidOrderException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidOrderedDishException)
                code = HttpStatusCode.BadRequest;
            else
            if (ex is InvalidCityException)
                code = HttpStatusCode.BadRequest;
            else
               code = HttpStatusCode.BadRequest;


            var result = JsonConvert.SerializeObject(new { error = ex.Message });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
