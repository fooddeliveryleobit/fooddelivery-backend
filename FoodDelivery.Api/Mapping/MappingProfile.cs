﻿using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Core.Models;
using FoodDelivery.Service.Changes;

namespace FoodDelivery.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Domain to Resource
            CreateMap<Role, RoleResource>();
            CreateMap<User, UserResource>()
                .ForMember(lat => lat.Latitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.X : default))
                .ForMember(lon => lon.Longitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.Y : default))
                .ForMember(pass => pass.Password, m => m.MapFrom(u => ""));

            CreateMap<Restaurant, RestaurantResource>()
                .ForMember(lat => lat.Latitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.X : default(double)))
                .ForMember(lon => lon.Longitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.Y : default(double)));


            CreateMap<DishCategory, DishCategoryResource>();
            CreateMap<Dish, DishResource>();
            CreateMap<Order, OrderResource>()
                .ForMember(lat => lat.Latitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.X : default))
                .ForMember(lon => lon.Longitude, m => m.MapFrom(u => (u.Location != null)
                                                               ? u.Location.Y : default));
            CreateMap<OrderedDish, OrderedDishResource>();  
            CreateMap<Score, ScoreResource>();
            CreateMap<City, CityResource>();



            //Resource to Domain
            CreateMap<RoleResource, Role>();
            CreateMap<UserResource, User>();
            CreateMap<RestaurantResource, Restaurant>();
            CreateMap<DishCategoryResource, DishCategory>();
            CreateMap<DishResource, Dish>();
            CreateMap<OrderResource, Order>();
            CreateMap<OrderedDishResource, OrderedDish>();
            CreateMap<ScoreResource, Score>();
            CreateMap<CityResource, City>();
            CreateMap<UserSignInResource, User>();



            CreateMap<SaveUserResource, User>();
            CreateMap<SaveRoleResource, Role>();
            CreateMap<SaveRestaurantResource, Restaurant>();
            CreateMap<SaveDishCategoryResource, DishCategory>();
            CreateMap<SaveDishResource, Dish>();
            CreateMap<SaveOrderResource, Order>();
            CreateMap<SaveOrderedDishResource, OrderedDish>();
            CreateMap<SaveScoreResource, Score>();
            CreateMap<SaveCityResource, City>();


            CreateMap<UpdateUserResource, User>();
            CreateMap<UpdateRoleResource, Role>();
            CreateMap<UpdateRestaurantResource, Restaurant>();
            CreateMap<UpdateDishCategoryResource, DishCategory>();
            CreateMap<UpdateDishResource, Dish>();
            CreateMap<UpdateOrderResource, Order>();
            CreateMap<UpdateOrderedDishResource, OrderedDish>();
            CreateMap<UpdateScoreResource, Score>();



            CreateMap<SaveUserResource, UserChanges>();
            CreateMap<SaveRoleResource, RoleChanges>();
            CreateMap<SaveRestaurantResource, RestaurantChanges>();
            CreateMap<SaveDishCategoryResource, DishCategoryChanges>();
            CreateMap<SaveDishResource, DishChanges>();
            CreateMap<SaveOrderResource, OrderChanges>();
            CreateMap<SaveOrderedDishResource, OrderedDishChanges>();
            CreateMap<SaveScoreResource, ScoreChanges>();
           

            CreateMap<UpdateUserResource, UserChanges>();
            CreateMap<UpdateRoleResource, RoleChanges>();
            CreateMap<UpdateRestaurantResource, RestaurantChanges>();

            CreateMap<UpdateDishCategoryResource, DishCategoryChanges>();
            CreateMap<UpdateDishResource, DishChanges>();
            CreateMap<UpdateOrderResource, OrderChanges>();
            CreateMap<UpdateOrderedDishResource, OrderedDishChanges>();
            CreateMap<UpdateScoreResource, ScoreChanges>();


        }
    }
}
