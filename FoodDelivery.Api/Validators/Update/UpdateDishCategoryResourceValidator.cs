﻿using FluentValidation;
using FoodDelivery.Api.UpdateResource;

namespace FoodDelivery.Api.Validators.Update
{
    public class UpdateDishCategoryResourceValidator : AbstractValidator<UpdateDishCategoryResource>
    {
        public UpdateDishCategoryResourceValidator()
        {
            RuleFor(m => m.Name)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString);
        }
    }
}
