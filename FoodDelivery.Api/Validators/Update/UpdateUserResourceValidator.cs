﻿using FluentValidation;
using FoodDelivery.Api.UpdateResource;

namespace FoodDelivery.Api.Validators.Update
{
    public class UpdateUserResourceValidator : AbstractValidator<UpdateUserResource>
    {
        public UpdateUserResourceValidator()
        {
            RuleFor(a => a.Name)
                .MaximumLength(50)
                .Matches(@"^[\p{L} \.\-]+$").WithMessage("Must be a name.");


            RuleFor(a => a.Password)
                .MinimumLength(ValidatorProperties.MinCountSymbolsForPassword);


            RuleFor(a => a.CityId)
                .MaximumLength(50)
                .Matches(@"^[\p{L} \.\-]+$").WithMessage("Must be a city name.");

            RuleFor(a => a.Phone)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForPhoneNumber)
                .Matches(ValidatorProperties.RegExForPhoneNumbers).WithMessage("Must be an phone number.");

        }
    }
}
