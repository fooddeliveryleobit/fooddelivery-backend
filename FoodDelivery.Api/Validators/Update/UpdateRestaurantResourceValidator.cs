﻿using FluentValidation;
using FoodDelivery.Api.UpdateResource;

namespace FoodDelivery.Api.Validators.Update
{
    public class UpdateRestaurantResourceValidator : AbstractValidator<UpdateRestaurantResource>
    {
        public UpdateRestaurantResourceValidator()
        {
            RuleFor(m => m.CityId)
                  .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                  .Matches(ValidatorProperties.RegExForNames).WithMessage("Must be a city name.");

            RuleFor(m => m.Name)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString);
        }
    }
    
}
