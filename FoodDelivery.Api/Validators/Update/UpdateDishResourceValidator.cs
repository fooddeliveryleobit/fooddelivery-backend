﻿using FluentValidation;
using FoodDelivery.Api.UpdateResource;

namespace FoodDelivery.Api.Validators.Update
{
    public class UpdateDishResourceValidator : AbstractValidator<UpdateDishResource>
    {
        public UpdateDishResourceValidator()
        {
            RuleFor(m => m.Description)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForText);
        }
    }
}
