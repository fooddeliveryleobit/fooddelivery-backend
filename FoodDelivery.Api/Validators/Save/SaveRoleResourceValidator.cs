﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveRoleResourceValidator : AbstractValidator<SaveRoleResource>
    {
        public SaveRoleResourceValidator()
        {
            RuleFor(m => m.Id)
                .NotEmpty();
        }
    }
}
