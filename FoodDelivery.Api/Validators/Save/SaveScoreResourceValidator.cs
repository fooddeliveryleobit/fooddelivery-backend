﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveScoreResourceValidator : AbstractValidator<SaveScoreResource>
    {
        public SaveScoreResourceValidator()
        {
            RuleFor(m => m.Value)
                .NotEmpty();
        }
    }
}
