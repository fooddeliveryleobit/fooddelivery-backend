﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveCityResourceValidator : AbstractValidator<SaveCityResource>
    {
        public SaveCityResourceValidator()
        {
            RuleFor(a => a.CityName)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                .Matches(ValidatorProperties.RegExForNames).WithMessage("Must be a city name.");

        }
    }
}