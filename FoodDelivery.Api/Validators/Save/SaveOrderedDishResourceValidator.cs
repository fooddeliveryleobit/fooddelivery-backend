﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveOrderedDishResourceValidator : AbstractValidator<SaveOrderedDishResource>
    {
        public SaveOrderedDishResourceValidator()
        {
            RuleFor(m => m.Count)
                .NotEmpty();
        }
    }
}
