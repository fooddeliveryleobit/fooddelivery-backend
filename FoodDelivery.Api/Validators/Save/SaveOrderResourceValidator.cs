﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveOrderResourceValidator : AbstractValidator<SaveOrderResource>
    {
        public SaveOrderResourceValidator()
        {

           RuleFor(m => m.Date)
                  .NotEmpty();
            
        }
    }
}
