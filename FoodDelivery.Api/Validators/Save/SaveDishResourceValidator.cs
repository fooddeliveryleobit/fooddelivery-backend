﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveDishResourceValidator : AbstractValidator<SaveDishResource>
    {
        public SaveDishResourceValidator()
        {
            RuleFor(m => m.Description)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForText)
                .NotEmpty();

            RuleFor(m => m.Price)
                .NotEmpty();

            RuleFor(m => m.Name)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString);

        }
    }
}
