﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveUserResourceValidator : AbstractValidator<SaveUserResource>
    {
        public SaveUserResourceValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                .Matches(ValidatorProperties.RegExForNames).WithMessage("Must be a name.");

            RuleFor(a => a.Email)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                .Matches(ValidatorProperties.RegExForEmails).WithMessage("Must be an email.");

            RuleFor(a => a.Password)
                .NotEmpty()
                .MinimumLength(ValidatorProperties.MinCountSymbolsForPassword);

            RuleFor(a => a.Phone)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForPhoneNumber)
                .Matches(ValidatorProperties.RegExForPhoneNumbers).WithMessage("Must be an phone number.");

            RuleFor(a => a.CityId)
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                .Matches(ValidatorProperties.RegExForNames).WithMessage("Must be a city name.");


            RuleFor(a => a.RoleId)
                .NotEmpty();


        }
    }
}
