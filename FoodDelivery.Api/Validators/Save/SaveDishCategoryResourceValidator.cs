﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveDishCategoryResourceValidator : AbstractValidator<SaveDishCategoryResource>
    {
        public SaveDishCategoryResourceValidator()
        {
            RuleFor(m => m.Name)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString);
        }
    }
}
