﻿using FluentValidation;
using FoodDelivery.Api.SaveResources;

namespace FoodDelivery.Api.Validators.Save
{
    public class SaveRestaurantResourceValidator : AbstractValidator<SaveRestaurantResource>
    {
        public SaveRestaurantResourceValidator()
        {

            RuleFor(m => m.CityId)
                    .MaximumLength(ValidatorProperties.MaxCountSymbolsForString)
                    .Matches(ValidatorProperties.RegExForNames).WithMessage("Must be a city name.");

            RuleFor(m => m.Name)
                .NotEmpty()
                .MaximumLength(ValidatorProperties.MaxCountSymbolsForString);

            RuleFor(m => m.Latitude)
                .NotEmpty();

            RuleFor(m => m.Longitude)
                .NotEmpty();
        }
    }
}
