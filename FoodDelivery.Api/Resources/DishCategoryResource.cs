﻿

namespace FoodDelivery.Api.Resources
{
    public class DishCategoryResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IdRestaurant { get; set; }
        public RestaurantResource Restaurant { get; set; }
    }
}
