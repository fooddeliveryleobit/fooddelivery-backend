﻿

namespace FoodDelivery.Api.Resources
{
    public class ScoreResource
    {
        public int SenderId { get; set; }
        public UserResource Sender { get; set; }
        public int ReceiverId { get; set; }
        public UserResource Receiver { get; set; }
        public int OrderId { get; set; }
        public OrderResource Order { get; set; }
        public double Value { get; set; }
    }
}
