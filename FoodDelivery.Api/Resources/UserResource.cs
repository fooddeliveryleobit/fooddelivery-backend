﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Api.Resources
{
    public class UserResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
        public string Phone { get; set; }
        public RoleVariantEnum RoleId { get; set; }
        public RoleResource Role { get; set; }
        public string Token { get; set; }
    }
}
