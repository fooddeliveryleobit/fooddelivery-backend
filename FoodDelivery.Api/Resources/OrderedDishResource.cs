﻿

namespace FoodDelivery.Api.Resources
{
    public class OrderedDishResource
    {
        public int OrderId { get; set; }
        public OrderResource Order { get; set; }
        public int DishId { get; set; }
        public DishResource Dish { get; set; }
        public int Count { get; set; }
    }
}
