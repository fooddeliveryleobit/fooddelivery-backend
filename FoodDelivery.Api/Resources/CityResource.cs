﻿

namespace FoodDelivery.Api.Resources
{
    public class CityResource
    {
        public string CityName { get; set; }
    }
}
