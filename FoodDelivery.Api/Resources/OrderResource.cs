﻿using System;
using FoodDelivery.Core.Models;

namespace FoodDelivery.Api.Resources
{
    public class OrderResource
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public UserResource Customer { get; set; }
        public int RestaurantId { get; set; }
        public RestaurantResource Restaurant { get; set; }
        public int? DeliverId { get; set; }
        public UserResource Deliver { get; set; }
        public DateTime Date { get; set; }
        public OrderStatusEnum Status { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
