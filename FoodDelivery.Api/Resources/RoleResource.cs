﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Api.Resources
{
    public class RoleResource
    {
        public RoleVariantEnum Id { get; set; }
    }
}
