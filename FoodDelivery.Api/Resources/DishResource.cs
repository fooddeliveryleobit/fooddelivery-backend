﻿

namespace FoodDelivery.Api.Resources
{
    public class DishResource
    {
        public int Id { get; set; }
        public int IdCategory { get; set; }
        public DishCategoryResource Category { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
    }
}
