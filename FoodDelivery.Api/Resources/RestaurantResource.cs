﻿

namespace FoodDelivery.Api.Resources
{
    public class RestaurantResource
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public UserResource Owner { get; set; }
        public string Name { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
    }
}
