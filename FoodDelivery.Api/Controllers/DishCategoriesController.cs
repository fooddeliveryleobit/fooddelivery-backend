﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Api.Validators.Update;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class DishCategoriesController : ControllerBase
    {
        private readonly IDishCategoryService _dishCategoryService;
        private readonly IRestaurantService _restaurantService;
        private readonly IMapper _mapper;

        public DishCategoriesController(IDishCategoryService dishCategoryService, IRestaurantService restaurantService, IMapper mapper)
        {
            _restaurantService = restaurantService;
            _dishCategoryService = dishCategoryService;
            _mapper = mapper;
        }


        [AllowAnonymous]
        [HttpGet("[action]/{restaurantId}")]
        public async Task<ActionResult<IEnumerable<DishCategoryResource>>> GetAllDishCategories(int restaurantId)
        {
            var dishCategories = await _dishCategoryService.GetAllDishCategoriesAsync(restaurantId).ConfigureAwait(false);

            if (dishCategories == null)
                return NotFound();

            var dishCategoriesResource = _mapper.Map<IEnumerable<DishCategory>, IEnumerable<DishCategoryResource>>(dishCategories);

            return Ok(dishCategoriesResource);
        }

        [AllowAnonymous]
        [HttpGet("{dishCategoryId}")]
        public async Task<ActionResult<DishCategoryResource>> GetDishCategoryById(int dishCategoryId)
        {

            var dishCategory = await _dishCategoryService.GetDishCategoryWithRestaurantByIdAsync(dishCategoryId).ConfigureAwait(false);

            if (dishCategory == null)
                return NotFound();


            var dishCategoryResource = _mapper.Map<DishCategory, DishCategoryResource>(dishCategory);

            return Ok(dishCategoryResource);
        }



        [Authorize(Roles = "Admin, Owner")]
        [HttpPost("{restaurantId}")]
        public async Task<ActionResult<DishCategoryResource>> CreateDishCategory(int restaurantId, [FromBody] SaveDishCategoryResource saveDishCategoryResource)
        {
            if (await DoesRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);


            var validator = new SaveDishCategoryResourceValidator();
            var validationResult = await validator.ValidateAsync(saveDishCategoryResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();

            var dishToCreate = _mapper.Map<SaveDishCategoryResource, DishCategory>(saveDishCategoryResource);

            var newDishCategory = await _dishCategoryService.CreateDishCategoryAsync(restaurantId, dishToCreate).ConfigureAwait(false);

            if (newDishCategory == null)
                return NotFound();

            var dishCategory = await _dishCategoryService.GetDishCategoryWithRestaurantByIdAsync(newDishCategory.Id).ConfigureAwait(false);

            var dishCategoryResource = _mapper.Map<DishCategory, DishCategoryResource>(dishCategory);

            return Ok(dishCategoryResource);
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpDelete("{dishCategoryId}")]
        public async Task<ActionResult> DeleteDishCategory(int dishCategoryId)
        {
            if (await DoesDishCategoryBelongToUserOrAdmin(dishCategoryId).ConfigureAwait(false) == false)
                return StatusCode(403);

            await _dishCategoryService.DeleteDishCategoryAsync(dishCategoryId).ConfigureAwait(false);
   
            return NoContent();
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpPut("{dishCategoryId}")]
        public async Task<ActionResult<DishCategoryResource>> UpdateDishCategory(int dishCategoryId, [FromBody] UpdateDishCategoryResource updateDishCategoryResource)
        {
            if (await DoesDishCategoryBelongToUserOrAdmin(dishCategoryId).ConfigureAwait(false) == false)
                return StatusCode(403);


            var validator = new UpdateDishCategoryResourceValidator();
            var validationResult = await validator.ValidateAsync(updateDishCategoryResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();

            var dishCategory = _mapper.Map<UpdateDishCategoryResource, DishCategoryChanges>(updateDishCategoryResource);

            await _dishCategoryService.UpdateDishCategoryAsync(dishCategoryId, dishCategory).ConfigureAwait(false);

            var updatedDishCategory = await _dishCategoryService.GetDishCategoryWithRestaurantByIdAsync(dishCategoryId).ConfigureAwait(false);

            var updatedDishCategoryResource = _mapper.Map<DishCategory, DishCategoryResource>(updatedDishCategory);

            return Ok(updatedDishCategoryResource);
        }

        #region Authorization checks

        private async Task<bool> DoesRestaurantBelongToUserOrAdmin(int restaurantId)
        {
            if (User.IsInRole("Admin") == true)
                return true;


            var restaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);

            var userIdClaim = User.FindFirst("Id");


            if (restaurant == null || userIdClaim == null || restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != userIdClaim.Value)
                return false;

            return true;
        }

        private async Task<bool> DoesDishCategoryBelongToUserOrAdmin(int dishCategoryId)
        {
            if (User.IsInRole("Admin") == true)
                return true;

            var dishCategory = await _dishCategoryService.GetDishCategoryWithRestaurantByIdAsync(dishCategoryId).ConfigureAwait(false);

            var userIdClaim = User.FindFirst("Id");

            if (dishCategory == null || userIdClaim == null  || dishCategory.Restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != userIdClaim.Value)
                return false;

            return true;
        }
        #endregion
    }
}