﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class OrderedDishesController : ControllerBase
    {
        private readonly IOrderedDishService _orderedDishService;
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrderedDishesController(IOrderedDishService orderedDishService, IOrderService orderService, IMapper mapper)
        {
            _orderedDishService = orderedDishService;
            _orderService = orderService;
            _mapper = mapper;
        }


        [Authorize(Roles = "Admin, Customer, Deliver, Owner")]
        [HttpGet("{orderId}")]
        public async Task<ActionResult<IEnumerable<OrderedDishResource>>> GetAllOrderedDishesByOrderId(int orderId)
        {
            if (await DoesUserBelongToOrder(orderId).ConfigureAwait(false) == false)
                return StatusCode(403);


            var orderedDishes = await _orderedDishService.GetAllOrderedDishesByOrderIdAsync(orderId).ConfigureAwait(false);

            if (orderedDishes == null)
                return NotFound();

            var orderedDishesResource = _mapper.Map<IEnumerable<OrderedDish>, IEnumerable<OrderedDishResource>>(orderedDishes);

            return Ok(orderedDishesResource);
        }



        #region Authorization checks
        private async Task<bool> DoesUserBelongToOrder(int orderId)
        {
            if (User.IsInRole("Admin") == true)
                return true;


            var order = await _orderService.GetOrderWithrestaurantById(orderId).ConfigureAwait(false);
            var userIdClaim = Int32.Parse(User.FindFirst("Id").Value, CultureInfo.CurrentCulture);

            if (order == null || ((order.DeliverId != null && order.DeliverId != userIdClaim) && order.CustomerId != userIdClaim && order.Restaurant.OwnerId != userIdClaim))
                return false;

            return true;
        }
        #endregion
    }
}