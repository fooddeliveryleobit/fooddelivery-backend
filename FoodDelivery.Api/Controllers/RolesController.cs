﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Core.Models;
using FoodDelivery.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;
        public RolesController(IRoleService roleService, IMapper mapper)
        {
            this._roleService = roleService;
            this._mapper = mapper;
        }
        
        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<RoleResource>>> GetAllRoles()
        {
            var roles = await _roleService.GetAllRolesAsync().ConfigureAwait(false);

            if (roles == null)
                return NotFound();

            var roleResources = _mapper.Map<IEnumerable<Role>, IEnumerable<RoleResource>>(roles);

            return Ok(roleResources);
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("")]
        public async Task<ActionResult<RoleResource>> CreateRole([FromBody] SaveRoleResource saveRoleResource)
        {
            var validator = new SaveRoleResourceValidator();
            var validationResult = await validator.ValidateAsync(saveRoleResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            var roleToCreate = _mapper.Map<SaveRoleResource, Role>(saveRoleResource);

            var newRole = await _roleService.CreateRoleAsync(roleToCreate).ConfigureAwait(false);

            var role = await _roleService.GetRoleByIdAsync((int)newRole.Id).ConfigureAwait(false);

            var roleResource = _mapper.Map<Role, RoleResource>(role);

            return roleResource;
        }


        [Authorize(Roles = "Admin")]
        [HttpDelete("{roleId}")]
        public async Task<ActionResult> DeleteRole(int roleId)
        {
            var roleToDelete = await _roleService.GetRoleByIdAsync(roleId).ConfigureAwait(false);

            if (roleToDelete == null)
                return NotFound();

            await _roleService.DeleteRoleAsync(roleToDelete).ConfigureAwait(false);

            return NoContent();
        }
    }
}