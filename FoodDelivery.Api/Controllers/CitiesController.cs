﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class CitiesController : Controller
    {
        private readonly ICityService _cityService;
        private readonly IMapper _mapper;
        public CitiesController(ICityService cityService, IMapper mapper)
        {
            this._cityService = cityService;
            this._mapper = mapper;
        }


        [Authorize(Roles = "Admin")]
        [HttpPost("")]
        public async Task<ActionResult<IEnumerable<CityResource>>> CreateCity([FromBody]  IEnumerable<SaveCityResource> cities)
        {
            var validator = new SaveCityResourceValidator();

            foreach (var val in cities)
            {
                var validationResult = await validator.ValidateAsync(val).ConfigureAwait(false);

                if (validationResult.IsValid == false)
                {
                    return BadRequest();
                }
            }
   

            var citiesToCreate = _mapper.Map<IEnumerable<SaveCityResource>, IEnumerable<City>>(cities);

            var newCities = await _cityService.CreateCityAsync(citiesToCreate).ConfigureAwait(false);

            var citiesResource = _mapper.Map<IEnumerable<City>, IEnumerable<CityResource>>(newCities);

            return Ok(citiesResource);
        }


        [Authorize(Roles = "Admin")]
        [HttpDelete("{cityId}")]
        public async Task<ActionResult<IEnumerable<CityResource>>> DeleteCityById(string cityId)
        {
           await _cityService.DeleteCityAsync(cityId).ConfigureAwait(false);

           return NoContent();
        }


        [AllowAnonymous]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<CityResource>>> GetAllCitiesAsync()
        {
            var cities = await _cityService.GetAllCitiesAsync().ConfigureAwait(false);

            if (cities == null)
                return NotFound();


            var citiesResource = _mapper.Map<IEnumerable<City>, IEnumerable<CityResource>>(cities);

            return Ok(citiesResource);
        }
    }
}