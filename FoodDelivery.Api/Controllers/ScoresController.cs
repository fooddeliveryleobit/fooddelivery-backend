﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Api.Validators.Update;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class ScoresController : Controller
    {
        private readonly IScoreService _scoreService;
        private readonly IMapper _mapper;
        public ScoresController(IScoreService scoreService, IMapper mapper)
        {
            _scoreService = scoreService;
            _mapper = mapper;  
        }


        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<ScoreResource>>> GetAllScores()
        {
            var scores = await _scoreService.GetAllScoresAsync().ConfigureAwait(false);


            if (scores == null)
                return NotFound();

            var scoresResource = _mapper.Map<IEnumerable<Score>, IEnumerable<ScoreResource>>(scores);

            return Ok(scoresResource);
        }


        [AllowAnonymous]
        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<double>> GetUserScore(int userId)
        {
            double score = await _scoreService.GetAverageScoreByUserIdAsync(userId).ConfigureAwait(false);

            return Ok(score);
        }
        

        [Authorize(Roles = "Admin, Customer, Deliver")]
        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<ScoreResource>>> GetAllReceivedScoresById(int userId)
        {
            if (IsCurrentUserOrAdmin(userId) == false)
                return StatusCode(403);


            var receivedScores = await _scoreService.GetAllRecievedScoresByRecieverId(userId).ConfigureAwait(false);

            if (receivedScores == null)
                return NotFound();

            var receivedScoresResource = _mapper.Map<IEnumerable<Score>, IEnumerable<ScoreResource>>(receivedScores);

            return Ok(receivedScoresResource);
        }


        [Authorize(Roles = "Admin, Customer, Deliver")]
        [HttpGet("[action]/{userId}")]
        public async Task<ActionResult<IEnumerable<ScoreResource>>> GetAllSentScoresById(int userId)
        {
            if (IsCurrentUserOrAdmin(userId) == false)
                return StatusCode(403);


            var sentScores = await _scoreService.GetAllSentScoresBySenderId(userId).ConfigureAwait(false);

            if (sentScores == null)
                return NotFound();

            var sentScoresResource = _mapper.Map<IEnumerable<Score>, IEnumerable<ScoreResource>>(sentScores);

            return Ok(sentScoresResource);
        }


        [AllowAnonymous]
        [HttpGet("{senderId}/{receiverId}/{orderId}")]
        public async Task<ActionResult<ScoreResource>> GetScoreById(int senderId, int receiverId, int orderId)
        {
            var score = await _scoreService.GetScoreByIdAsync(senderId, receiverId, orderId).ConfigureAwait(false);

          //  if (score == null)
            //    return NotFound();

            var scoreResource = _mapper.Map<Score, ScoreResource>(score);

            return Ok(scoreResource ?? new ScoreResource());
        }


        [Authorize(Roles = "Admin, Customer, Deliver")]
        [HttpPost("{senderId}/{receiverId}/{orderId}")]
        public async Task<ActionResult<ScoreResource>> CreateScore(int senderId, int receiverId, int orderId, [FromBody] SaveScoreResource saveScoreResource)
        {
            if (IsCurrentUserOrAdmin(senderId) == false)
                return StatusCode(403);


            var validator = new SaveScoreResourceValidator();
            var validationResult = await validator.ValidateAsync(saveScoreResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();


            var scoreToAdd = _mapper.Map<SaveScoreResource, Score>(saveScoreResource);

            await _scoreService.CreateNewScoreAsync(senderId, receiverId, orderId, scoreToAdd).ConfigureAwait(false);

            var newScore = await _scoreService.GetScoreByIdAsync(senderId, receiverId, orderId).ConfigureAwait(false);

            var newScoreResource = _mapper.Map<Score, ScoreResource>(newScore);

            return Ok(newScoreResource);
        }


        [Authorize(Roles = "Admin, Customer, Deliver")]
        [HttpPut("{senderId}/{receiverId}/{orderId}")]
        public async Task<ActionResult<ScoreResource>> UpdateScore(int senderId, int receiverId, int orderId, [FromBody] UpdateScoreResource updateScoreResource)
        {
            if (IsCurrentUserOrAdmin(senderId) == false)
                return StatusCode(403);


            var validator = new UpdateScoreResourceValidator();
            var validationResult = await validator.ValidateAsync(updateScoreResource).ConfigureAwait(false);

            if (senderId < 1 || receiverId < 1 || orderId < 1 || !validationResult.IsValid)
                return BadRequest(validationResult.Errors);


            

            var scoreToBeUpdated = await _scoreService.GetScoreByIdAsync(senderId, receiverId, orderId).ConfigureAwait(false);   

            if (scoreToBeUpdated == null)
            {
                 return NotFound();
            }

            var scoreChanges = _mapper.Map<UpdateScoreResource, ScoreChanges>(updateScoreResource);

            await _scoreService.UpdateScoreAsync(scoreToBeUpdated, scoreChanges).ConfigureAwait(false);


            var updatedScore = await _scoreService.GetScoreByIdAsync(senderId, receiverId, orderId).ConfigureAwait(false);
            var updatedScoreResource = _mapper.Map<Score, ScoreResource>(updatedScore);

            return Ok(updatedScoreResource);
        }


        [Authorize(Roles = "Admin, Customer, Deliver")]
        [HttpDelete("{senderId}/{receiverId}/{orderId}")]
        public async Task<ActionResult> DeleteScore(int senderId, int receiverId, int orderId)
        {
            if (IsCurrentUserOrAdmin(senderId) == false)
                return StatusCode(403);


            if (senderId < 1 || receiverId < 1 || orderId < 1)
                return BadRequest();

            var scoreToDelete = await _scoreService.GetScoreByIdAsync(senderId, receiverId, orderId).ConfigureAwait(false);

            if (scoreToDelete == null)
                return NotFound();

            await _scoreService.DeleteScoreAsync(scoreToDelete).ConfigureAwait(false);

            return NoContent();
        }



        #region Authorization checks
        private bool IsCurrentUserOrAdmin(int userId)
        {
            if (User.IsInRole("Admin"))
                return true;

            var UserIdClaim = User.FindFirst("Id");

            if (UserIdClaim == null || userId.ToString(CultureInfo.CurrentCulture) != UserIdClaim.Value)
                return false;

            return true;
        }
        #endregion
    }

}