﻿using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.PasswordHashing;
using FoodDelivery.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;


namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher _passwordHasher;
        public AccountController(IUserService userService, IPasswordHasher passwordHasher, IMapper mapper)
        {
            this._userService = userService;
            this._mapper = mapper;
            this._passwordHasher = passwordHasher;
        }


        [AllowAnonymous]
        [HttpPost("")]  
        public async Task<ActionResult<UserResource>> Authenticate([FromBody] UserSignInResource signIn)
        {
            var userSignInData = _mapper.Map<UserSignInResource, User>(signIn);

            var user = await _userService.Authenticate(userSignInData, _passwordHasher).ConfigureAwait(false);

            if (user == null)
                return BadRequest(new { message = "Email or password is incorrect" });

            var userResource = _mapper.Map<User, UserResource>(user);

            return Ok(userResource);

        }
    }
}