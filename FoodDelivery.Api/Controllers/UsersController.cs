﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Api.Validators.Update;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using System.Globalization;
using Newtonsoft.Json;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Monitoring;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ICustomAppInsightTelemetry _customAppInsightTelemetry;
        public UsersController(IUserService userService, IMapper mapper, ICustomAppInsightTelemetry customAppInsightTelemetry)
        {
            _userService = userService;
            _mapper = mapper;
            _customAppInsightTelemetry = customAppInsightTelemetry;
        }


        #region Default actions


        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<UserResource>>> GetAllUser()
        {
            _customAppInsightTelemetry.TrackAllTelemetry(Request);

            var users = await _userService.GetAllUsersAsync().ConfigureAwait(false);


            if (users == null)
                return NotFound();

            var userResources = _mapper.Map<IEnumerable<User>, IEnumerable<UserResource>>(users);

            return Ok(userResources);
        }
       

        [AllowAnonymous]
        [HttpPost("")]
        public async Task<ActionResult<UserResource>> CreateUser()
        {
            if (!Request.Form.ContainsKey("user"))
                throw new InvalidUserException();

            #region get data from form
            string userData = Request.Form["user"];

            SaveUserResource saveUserResource = JsonConvert.DeserializeObject<SaveUserResource>(userData);

            if (saveUserResource == null)
                throw new InvalidUserException();


            if (Request.Form.Files.Count < 1)
                throw new InvalidUserException();


            var postedFile = Request.Form.Files[0];

            #endregion

            #region validation
            var validator = new SaveUserResourceValidator();
            var validationResult = await validator.ValidateAsync(saveUserResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);
            #endregion
            
            var userToCreate = _mapper.Map<SaveUserResource, User>(saveUserResource);

            var newUser = await _userService.CreateUserAsync(userToCreate, postedFile).ConfigureAwait(false);

            var user = await _userService.GetUserByIdAsync(newUser.Id).ConfigureAwait(false);

            var userResource = _mapper.Map<User, UserResource>(user);

            return Ok(userResource);
        }

        [AllowAnonymous]     
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserResource>> GetUserById(int userId)
        {
            _customAppInsightTelemetry.TrackAllTelemetry(Request);

            var user = await _userService.GetUserByIdAsync(userId).ConfigureAwait(false);

            if (user == null)
                return NotFound();


            var userResource = _mapper.Map<User, UserResource>(user);

            return Ok(userResource);
        }


       
        [Authorize(Roles = "Admin, Deliver, Customer, Owner")]
        [HttpPut("{userId}")]
        public async Task<ActionResult<UserResource>> UpdateUser(int userId)
        {
            if (IsCurrentUserOrAdmin(userId) == false)
                return StatusCode(403);

            _customAppInsightTelemetry.TrackAllTelemetry(Request);

            #region get data from form
            if (!Request.Form.ContainsKey("user"))
                throw new InvalidUserException();

            string restaurantData = Request.Form["user"];


            UpdateUserResource updateUserResource = JsonConvert.DeserializeObject<UpdateUserResource>(restaurantData);

            if (updateUserResource == null)
                throw new InvalidUserException();

            #endregion

            #region validation
            var validator = new UpdateUserResourceValidator();
            var validationResult = await validator.ValidateAsync(updateUserResource).ConfigureAwait(false);


            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            #endregion

            var userToBeUpdated = await _userService.GetUserByIdAsync(userId).ConfigureAwait(false);

            if (userToBeUpdated == null)
                return NotFound();


            var userChanges = _mapper.Map<UpdateUserResource, UserChanges>(updateUserResource);

            if (Request.Form.Files.Count == 1)
            {
                var postedFile = Request.Form.Files[0];
                userChanges.Picture = postedFile;
            }


            await _userService.UpdateUserAsync(userToBeUpdated, userChanges).ConfigureAwait(false);


            var updatedUser = await _userService.GetUserByIdAsync(userId).ConfigureAwait(false);
            var updatedUserResource = _mapper.Map<User, UserResource>(updatedUser);

            return Ok(updatedUserResource);
        }


        [Authorize(Roles = "Admin, Deliver, Customer, Owner")]
        [HttpDelete("{userId}")]
        public async Task<ActionResult> DeleteUser(int userId)
        {
            if (IsCurrentUserOrAdmin(userId) == false)
                return StatusCode(403);


            var user = await _userService.GetUserByIdAsync(userId).ConfigureAwait(false);

            if (user == null)
                return NotFound();

            await _userService.DeleteUserAsync(user).ConfigureAwait(false);

            return NoContent();
        }
        #endregion



        #region Authorization checks
        private bool IsCurrentUserOrAdmin(int userId)
        {
            if (User.IsInRole("Admin") == true)
                return true;

            var userIdClaim = User.FindFirst("Id");

            if (userIdClaim == null)
                return false;


            return userIdClaim.Value == userId.ToString(CultureInfo.CurrentCulture);   
        }
        #endregion
    }
}
