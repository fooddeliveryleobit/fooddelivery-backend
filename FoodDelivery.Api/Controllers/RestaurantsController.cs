﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Api.Validators.Update;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FoodDelivery.Api.Controllers
{

    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class RestaurantsController : ControllerBase
    {
        private readonly IRestaurantService _restaurantService;
        private readonly IMapper _mapper;
        private readonly IImageService _imageService;
        public RestaurantsController(IRestaurantService restaurantService, IImageService imageService, IMapper mapper)
        {
            _restaurantService = restaurantService;
            _imageService = imageService;
            _mapper = mapper;
        }
        #region Default actions

        [AllowAnonymous]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<RestaurantResource>>> GetAllRestaurants()
        {
            var restaurants = await _restaurantService.GetAllRestaurantsAsync().ConfigureAwait(false);

            if (restaurants == null)
                return NotFound();

            var restaurantsResource = _mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantResource>> (restaurants);

            return Ok(restaurantsResource);
        }
        #endregion


        [Authorize(Roles = "Admin, Owner")]
        [HttpPost("{ownerId}")]
        public async Task<ActionResult<RestaurantResource>> CreateRestaurant(int ownerId)
        {
            if (IsCurrentUserOrAdmin(ownerId) == false)
                return StatusCode(403);

            #region get data from form
            if (!Request.Form.ContainsKey("restaurant"))
                throw new InvalidRestaurantException();

            string restaurantData = Request.Form["restaurant"];

            SaveRestaurantResource  saveRestaurantResource = JsonConvert.DeserializeObject<SaveRestaurantResource>(restaurantData);
            
            if (saveRestaurantResource == null)
                throw new InvalidRestaurantException();


            if (Request.Form.Files.Count < 1)
                throw new InvalidRestaurantException();


            var postedFile = Request.Form.Files[0];
            #endregion

            #region validation
            var validator = new SaveRestaurantResourceValidator();
            var validationResult = await validator.ValidateAsync(saveRestaurantResource).ConfigureAwait(false);


            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);
            #endregion


            var restaurantToCreate = _mapper.Map<SaveRestaurantResource, Restaurant>(saveRestaurantResource);

            var newRestaurant = await _restaurantService.CreateRestaurantAsync(ownerId, restaurantToCreate, postedFile).ConfigureAwait(false);

            var restaurant = await _restaurantService.GetRestaurantByIdAsync(newRestaurant.Id).ConfigureAwait(false);

            var restaurantService = _mapper.Map<Restaurant, RestaurantResource>(restaurant);

            return Ok(restaurantService);
        }



        [HttpDelete("{restaurantId}")]
        [Authorize(Roles = "Admin, Owner")]
        public async Task<ActionResult> DeleteRestaurant(int restaurantId)
        {
            if (await DoesRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);


            await _restaurantService.DeleteRestaurantAsync(restaurantId).ConfigureAwait(false);

            return NoContent();
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpPut("{restaurantId}")]
        public async Task<ActionResult<RestaurantResource>> UpdateRestaurant(int restaurantId)
        {
            if (await DoesRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);

            #region get data from form
            if (!Request.Form.ContainsKey("restaurant"))
                throw new InvalidRestaurantException();

            string restaurantData = Request.Form["restaurant"];
              
            UpdateRestaurantResource updateRestaurantResource = JsonConvert.DeserializeObject<UpdateRestaurantResource>(restaurantData);

            if (updateRestaurantResource == null)
                throw new InvalidRestaurantException();
            #endregion


            #region validation
            var validator = new UpdateRestaurantResourceValidator();
            var validationResult = await validator.ValidateAsync(updateRestaurantResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();
            #endregion

            RestaurantChanges restaurantChanges = _mapper.Map<UpdateRestaurantResource, RestaurantChanges>(updateRestaurantResource);

          
            if (Request.Form.Files.Count == 1)
            {
                var postedFile = Request.Form.Files[0];
                restaurantChanges.Picture = postedFile;
               
            }
                

            await _restaurantService.UpdateRestaurantAsync(restaurantId, restaurantChanges).ConfigureAwait(false);

            var updatedRestaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);
            var updatedRestaurantResource = _mapper.Map<Restaurant, RestaurantResource>(updatedRestaurant);

            return Ok(updatedRestaurantResource);
        }
    

        [AllowAnonymous]
        [HttpGet("{restaurantId}")]
        public async Task<ActionResult<RestaurantResource>> GetRestaurantById(int restaurantId)
        {
            var restaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);

            if (restaurant == null)
                return NotFound();

            var restaurantResource = _mapper.Map<Restaurant, RestaurantResource>(restaurant);
            return Ok(restaurantResource);
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpGet("[action]/{restaurantId}")]
        public async Task<ActionResult<RestaurantResource>> GetOwnerRestaurantById(int restaurantId)
        {

            if (await DoesRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);


            var restaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);

            if (restaurant == null)
                return NotFound();

            var restaurantResource = _mapper.Map<Restaurant, RestaurantResource>(restaurant);
            return Ok(restaurantResource);
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpGet("[action]/{ownerId}")]
        public async Task<ActionResult<IEnumerable<RestaurantResource>>> GetAllRestaurantsByOwnerId(int ownerId)
        {
            if (IsCurrentUserOrAdmin(ownerId) == false)
                return StatusCode(403);


            var restaurant = await _restaurantService.GetAllRestaurantsByOwnerIdAsync(ownerId).ConfigureAwait(false);

            if (restaurant == null)
                return NotFound();

            var restaurantResource = _mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantResource>>(restaurant);
            return Ok(restaurantResource);
        }


        [Authorize(Roles = "Admin, Customer")]
        [HttpGet("[action]/{customerId}")]
        public async Task<ActionResult<IEnumerable<RestaurantResource>>> GetAllAvaliableRestaurantsToCustomer(int customerId)
        {
            if (IsCurrentUserOrAdmin(customerId) == false)
                return StatusCode(403);

            var restaurant = await _restaurantService.GetAllAvaliableRestaurantsToCustomer(customerId).ConfigureAwait(false);

            if (restaurant == null)
                return NotFound();

            var restaurantResource = _mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantResource>>(restaurant);
            return Ok(restaurantResource);
        }


        [AllowAnonymous]
        [HttpGet("[action]/{cityId}")]
        public async Task<ActionResult<IEnumerable<RestaurantResource>>> GetAllRestaurantsByCityId(string cityId)
        {
            var restaurants = await _restaurantService.GetAllRestaurantsByCityIdAsync(cityId).ConfigureAwait(false);

            if (restaurants == null)
                return NotFound();

            var restaurantsResource = _mapper.Map<IEnumerable<Restaurant>, IEnumerable<RestaurantResource>>(restaurants);

            return Ok(restaurantsResource);
        }




        #region Authorization checks
        private bool IsCurrentUserOrAdmin(int userId)
        {
            if (User.IsInRole("Admin"))
                return true;

            var UserIdClaim = User.FindFirst("Id");

            if (UserIdClaim == null || userId.ToString(CultureInfo.CurrentCulture) != UserIdClaim.Value)
                return false;

            return true;
        }

        private async Task<bool> DoesRestaurantBelongToUserOrAdmin(int restaurantId)
        {
            if (User.IsInRole("Admin"))
                return true;

            var UserIdClaim = User.FindFirst("Id");

            var restaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);

            if (UserIdClaim == null || restaurant == null || restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != UserIdClaim.Value)
                return false;

            return true;
        }
        #endregion
    }
}