﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Core.Changes;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IRestaurantService _restaurantService;
        private readonly IMapper _mapper;
        public OrdersController(IOrderService orderService, IRestaurantService restaurantService, IMapper mapper)
        {
            _orderService = orderService;
            _restaurantService = restaurantService;
            _mapper = mapper;
        }


        #region Default actions
        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllOrders()
        {
            var orders = await _orderService.GetAllOrdersAsync().ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Customer, Deliver, Owner")]
        [HttpGet("{orderId}")]
        public async Task<ActionResult<OrderResource>> GetOrderById(int orderId)
        {
            var order = await _orderService.GetOrderByIdAsync(orderId).ConfigureAwait(false);

            if (order == null)
                return NotFound();

            if (await DoesUserBelongToOrder(order).ConfigureAwait(false) == false)
                return StatusCode(403);

            var orderResource = _mapper.Map<Order, OrderResource>(order);

            return Ok(orderResource);
        }


        [Authorize(Roles = "Admin, Customer, Deliver, Owner")]
        [HttpGet("[action]/{orderId}")]
        public async Task<ActionResult<decimal>> GetTotalOrderCost(int orderId)
        {
            var order = await _orderService.GetOrderByIdAsync(orderId).ConfigureAwait(false);

            if (order == null)
                return NotFound();

            if (await DoesUserBelongToOrder(order).ConfigureAwait(false) == false)
                return StatusCode(403);


            decimal price = await _orderService.GetTotalOrderCost(orderId).ConfigureAwait(false);


            return Ok(price);
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public ActionResult<decimal> DeliveryPrice()
        {
            return ValidatorProperties.CostForDelivery;
        }


        #endregion

        #region For Owners
        [Authorize(Roles = "Admin, Owner")]
        [HttpGet("[action]/{restaurantId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllFinishedOrdersRestaurantOrders(int restaurantId)
        {
            if (await IsCurrentRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);

            var orders = await _orderService.GetAllOrdersByRestaurantIdAsync(restaurantId, OrderStatusEnum.Finished).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpGet("[action]/{restaurantId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllAcceptedByDeliverOrdersRestaurantOrders(int restaurantId)
        {
            if (await IsCurrentRestaurantBelongToUserOrAdmin(restaurantId).ConfigureAwait(false) == false)
                return StatusCode(403);

            var orders = await _orderService.GetAllOrdersByRestaurantIdAsync(restaurantId, OrderStatusEnum.AcceptedByDeliver).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }

        #endregion

        #region For Customers
        #region AllCustomerOrders by orderStatus



        [Authorize(Roles = "Admin, Customer")]
        [HttpGet("[action]/{customerId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllNewCustomerOrders(int customerId)
        {
            if (IsCurrentUserOrAdmin(customerId, RoleVariantEnum.Customer) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllUserOrdersByStatusAsync(customerId, RoleVariantEnum.Customer, OrderStatusEnum.New).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Customer")]
        [HttpGet("[action]/{customerId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllAcceptedByDeliverCustomerOrders(int customerId)
        {
            if (IsCurrentUserOrAdmin(customerId, RoleVariantEnum.Customer) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllUserOrdersByStatusAsync(customerId, RoleVariantEnum.Customer, OrderStatusEnum.AcceptedByDeliver).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Customer")]
        [HttpGet("[action]/{customerId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllFinishedByDeliverCustomerOrders(int customerId)
        {
            if (IsCurrentUserOrAdmin(customerId, RoleVariantEnum.Customer) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllUserOrdersByStatusAsync(customerId, RoleVariantEnum.Customer, OrderStatusEnum.Finished).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }
        #endregion

        [Authorize(Roles = "Admin, Customer")]
        [HttpPost("[action]/{customerId}/{restaurantId}")]
        public async Task<ActionResult<OrderResource>> CreateOrderByCustomer(int customerId, int restaurantId, [FromBody] IEnumerable<SaveOrderedDishResource> dishes)
        {
            if (IsCurrentUserOrAdmin(customerId, RoleVariantEnum.Customer) == false)
                return StatusCode(403);


            SaveOrderedDishResourceValidator valiator = new SaveOrderedDishResourceValidator();
            foreach (var val in dishes)
            {
                var validationResult = await valiator.ValidateAsync(val).ConfigureAwait(false);

                if (validationResult.IsValid == false)
                {
                    return BadRequest();
                }
            }

            var dishesToAdd = _mapper.Map<IEnumerable<SaveOrderedDishResource>, IEnumerable<OrderedDish>>(dishes);

            var newOrder = await _orderService.CreateNewOrderByCustomer(customerId, restaurantId, dishesToAdd).ConfigureAwait(false);

            var newOrderResource = _mapper.Map<Order, OrderResource>(newOrder);

            return Ok(newOrderResource);
        }

        [Authorize(Roles = "Admin, Customer")]
        [HttpDelete("[action]/{customerId}/{orderId}")]
        public async Task<ActionResult> DeclineOrderByCustomer(int customerId, int orderId)
        {
            if (IsCurrentUserOrAdmin(customerId, RoleVariantEnum.Customer) == false)
                return StatusCode(403);


            await _orderService.DeclineOrderByCustomer(customerId, orderId).ConfigureAwait(false);

            return NoContent();
        }
        #endregion

        #region For Delivers
        #region AllDeliverOrders by orderStatus

        [Authorize(Roles = "Admin, Deliver")]
        [HttpGet("[action]/{deliverId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAcceptedByDeliverDeliverOrder(int deliverId)
        {
            if (IsCurrentUserOrAdmin(deliverId, RoleVariantEnum.Deliver) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllUserOrdersByStatusAsync(deliverId, RoleVariantEnum.Deliver, OrderStatusEnum.AcceptedByDeliver).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Deliver")]
        [HttpGet("[action]/{deliverId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllFinishedByDeliverDeliverOrders(int deliverId)
        {
            if (IsCurrentUserOrAdmin(deliverId, RoleVariantEnum.Deliver) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllUserOrdersByStatusAsync(deliverId, RoleVariantEnum.Deliver, OrderStatusEnum.Finished).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }
        #endregion


        [Authorize(Roles = "Admin, Deliver")]
        [HttpGet("[action]/{deliverId}")]
        public async Task<ActionResult<IEnumerable<OrderResource>>> GetAllAvaliableOrdersToDeliver(int deliverId)
        {
            if (IsCurrentUserOrAdmin(deliverId, RoleVariantEnum.Deliver) == false)
                return StatusCode(403);


            var orders = await _orderService.GetAllAvaliableOrdersToDeliverAsync(deliverId).ConfigureAwait(false);

            if (orders == null)
                return NotFound();

            var ordersResource = _mapper.Map<IEnumerable<Order>, IEnumerable<OrderResource>>(orders);

            return Ok(ordersResource);
        }


        [Authorize(Roles = "Admin, Deliver")]
        [HttpPut("[action]/{deliverId}/{orderId}")]
        public async Task<ActionResult<OrderResource>> AcceptOrderByDeliver(int deliverId, int orderId)
        {
            if (IsCurrentUserOrAdmin(deliverId, RoleVariantEnum.Deliver) == false)
                return StatusCode(403);


            var updatedOrder = await _orderService.AcceptedOrderByDeliver(deliverId, orderId).ConfigureAwait(false);

            var updatedOrderResource = _mapper.Map<Order, OrderResource>(updatedOrder);

            return Ok(updatedOrderResource);
        }


        [Authorize(Roles = "Admin, Deliver")]
        [HttpPut("[action]/{deliverId}/{orderId}")]
        public async Task<ActionResult<OrderResource>> FinishedOrderByDeliver(int deliverId, int orderId)
        {
            if (IsCurrentUserOrAdmin(deliverId, RoleVariantEnum.Deliver) == false)
                return StatusCode(403);


            var finishedOrder = await _orderService.FinishedOrderByDeliver(deliverId, orderId).ConfigureAwait(false);

            var finishedOrderResource = _mapper.Map<Order, OrderResource>(finishedOrder);

            return Ok(finishedOrderResource);
        }
        #endregion




        #region Authorization checks
        private bool IsCurrentUserOrAdmin(int userId, RoleVariantEnum role)
        {
            if (User.IsInRole("Admin") == true)
                return true;


            if (User.IsInRole(role.ToString()) == true)
            {
                var userIdClaim = User.FindFirst("Id");

                if (userIdClaim == null || userIdClaim.Value != userId.ToString(CultureInfo.CurrentCulture))
                    return false;

                return true;
            }

            return false;
        }

        private async Task<bool> DoesUserBelongToOrder(Order order)
        {
            if (User.IsInRole("Admin") == true)
                return true;


            if (User.IsInRole(RoleVariantEnum.Owner.ToString()) == true || User.IsInRole(RoleVariantEnum.Customer.ToString()) == true || User.IsInRole(RoleVariantEnum.Deliver.ToString()) == true)
            {
                var customerIdClaim = User.FindFirst("Id");

                var orderRestaurantOwner = (await _orderService.GetOrderWithrestaurantById(order.Id).ConfigureAwait(false)).Restaurant.OwnerId;
                if (customerIdClaim == null || (order.CustomerId.ToString(CultureInfo.CurrentCulture) != customerIdClaim.Value &&
                    (order.DeliverId != null && order.DeliverId.ToString() != customerIdClaim.Value) && 
                    orderRestaurantOwner.ToString(CultureInfo.CurrentCulture) != customerIdClaim.Value))

                    return false;

                return true;
            }

            return false;
        }

        private async Task<bool> IsCurrentRestaurantBelongToUserOrAdmin(int restaurantId)
        {
            if (User.IsInRole("Admin") == true)
                return true;

            if (User.IsInRole(RoleVariantEnum.Owner.ToString()) == true)
            {
                var ownerIdClaim = User.FindFirst("Id");

                var restaurant = await _restaurantService.GetRestaurantByIdAsync(restaurantId).ConfigureAwait(false);

                if (restaurant == null || ownerIdClaim == null || restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != ownerIdClaim.Value)
                    return false;

                return true;
            }

            return false;
        }
        #endregion
    }
}