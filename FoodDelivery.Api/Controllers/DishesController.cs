﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using AutoMapper;
using FoodDelivery.Api.Resources;
using FoodDelivery.Api.SaveResources;
using FoodDelivery.Api.UpdateResource;
using FoodDelivery.Api.Validators.Save;
using FoodDelivery.Api.Validators.Update;
using FoodDelivery.Core.Exceptions;
using FoodDelivery.Core.Models;
using FoodDelivery.Core.Services;
using FoodDelivery.Service.Changes;
using FoodDelivery.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FoodDelivery.Api.Controllers
{
    [Authorize]
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("api/[controller]")]
    public class DishesController : ControllerBase
    {
        private readonly IDishService _dishService;
        private readonly IDishCategoryService _dishCategoryService;
        private readonly IMapper _mapper;

        public DishesController(IDishService dishService, IDishCategoryService dishCategoryService, IMapper mapper)
        {
            _dishService = dishService;
            _mapper = mapper;
            _dishCategoryService = dishCategoryService;
        }
      

        [AllowAnonymous]
        [HttpGet("{dishId}")]
        public async Task<ActionResult<DishResource>> GetDishById(int dishId)
        {
            var dish = await _dishService.GetDishByIdAsync(dishId).ConfigureAwait(false);

            if (dish == null)
                return NotFound();

            var dishResource = _mapper.Map<Dish, DishResource>(dish);

            return Ok(dishResource);
        }
    

        [Authorize(Roles = "Admin, Owner")]
        [HttpPost("{dishCategoryId}")]
        public async Task<ActionResult<DishResource>> CreateDish(int dishCategoryId)
        {
            if (await DoesDishCategoryBelongToUserOrAdmin(dishCategoryId).ConfigureAwait(false) == false)
                return StatusCode(403);

            #region get data from form
            if (!Request.Form.ContainsKey("dish"))
                throw new InvalidDishException();

            string dishData = Request.Form["dish"];

            SaveDishResource saveDishResource = JsonConvert.DeserializeObject<SaveDishResource>(dishData);

            if (saveDishResource == null)
                throw new InvalidDishException();


            if (Request.Form.Files.Count < 1)
                throw new InvalidDishException();


            var postedFile = Request.Form.Files[0];
            #endregion

            #region validation
            var validator = new SaveDishResourceValidator();
            var validationResult = await validator.ValidateAsync(saveDishResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();
            #endregion

            var dishToAdd = _mapper.Map<SaveDishResource, Dish>(saveDishResource);

            var dish = await _dishService.CreateDishAsync(dishCategoryId, dishToAdd, postedFile).ConfigureAwait(false);

            var newDish = await _dishService.GetDishByIdAsync(dish.Id).ConfigureAwait(false);

            var newDishResource = _mapper.Map<Dish, DishResource>(newDish);

            return Ok(newDishResource);
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpDelete("{dishId}")]
        public async Task<ActionResult> DeleteDish(int dishId)
        {
            if (await DoesDishBelongToUserOrAdmin(dishId).ConfigureAwait(false) == false)
                return StatusCode(403);


            await _dishService.DeleteDishAsync(dishId).ConfigureAwait(false);

            return NoContent();
        }


        [Authorize(Roles = "Admin, Owner")]
        [HttpPut("{dishId}")]
        public async Task<ActionResult<DishResource>> UpdateDish(int dishId)
        {
            if (await DoesDishBelongToUserOrAdmin(dishId).ConfigureAwait(false) == false)
                return StatusCode(403);

            #region get data from form
            if (!Request.Form.ContainsKey("dish"))
                throw new InvalidRestaurantException();

            string dishData = Request.Form["dish"];

            UpdateDishResource updateDishCategoryResource = JsonConvert.DeserializeObject<UpdateDishResource>(dishData);

            if (updateDishCategoryResource == null)
                throw new InvalidDishException();
            #endregion

            #region validation
            var validator = new UpdateDishResourceValidator();
            var validationResult = await validator.ValidateAsync(updateDishCategoryResource).ConfigureAwait(false);

            if (!validationResult.IsValid)
                return BadRequest();
            #endregion


            var dishChanges = _mapper.Map<UpdateDishResource, DishChanges>(updateDishCategoryResource);


            if (Request.Form.Files.Count == 1)
            {
                var postedFile = Request.Form.Files[0];
                dishChanges.Picture = postedFile;

            }


            await _dishService.UpdateDishAsync(dishId, dishChanges).ConfigureAwait(false);

            var dish = await _dishService.GetDishByIdAsync(dishId).ConfigureAwait(false);

            var dishResource = _mapper.Map<Dish, DishResource>(dish);

            return Ok(dishResource);
        }


        [AllowAnonymous]
        [HttpGet("[action]/{dishCategoryId}")]
        public async Task<ActionResult<IEnumerable<DishResource>>> GetAllDishesByDishCategory(int dishCategoryId)
        {
            var dishes = await _dishService.GetAllDishesByCategoryIdAsync(dishCategoryId).ConfigureAwait(false);

            if (dishes == null)
                return NotFound();

            var dishesResource = _mapper.Map<IEnumerable<Dish>, IEnumerable<DishResource>>(dishes);

            return Ok(dishesResource);
        }


        [AllowAnonymous]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<DishResource>>> GetAllDishes()
        {
            var dishes = await _dishService.GetAllDishesAsync().ConfigureAwait(false);

            if (dishes == null)
                return NotFound();

            var dishesResource = _mapper.Map<IEnumerable<Dish>, IEnumerable<DishResource>>(dishes);

            return Ok(dishesResource);
        }

        #region Authorization checks

        private async Task<bool> DoesDishBelongToUserOrAdmin(int dishId)
        {
            if (User.IsInRole("Admin") == true)
                return true;

            var dish = await _dishService.GetByIdWithCategoryAndRestaurantAsync(dishId).ConfigureAwait(false);

            var ownerIdClaim = User.FindFirst("Id");

            if (dish == null || dish.Category.Restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != ownerIdClaim.Value)
                return false;

            return true;
        }

        private async Task<bool> DoesDishCategoryBelongToUserOrAdmin(int dishCategoryId)
        {
            if (User.IsInRole("Admin") == true)
                return true;

            var dishcategory = await _dishCategoryService.GetDishCategoryWithRestaurantByIdAsync(dishCategoryId).ConfigureAwait(false);

            var ownerIdClaim = User.FindFirst("Id");

            if (dishcategory == null || dishcategory.Restaurant.OwnerId.ToString(CultureInfo.CurrentCulture) != ownerIdClaim.Value)
                return false;

            return true;
        }
        #endregion
    }
}