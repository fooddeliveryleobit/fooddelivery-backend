﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateDishCategoryResource
    {
        public string Name { get; set; }
    }
}
