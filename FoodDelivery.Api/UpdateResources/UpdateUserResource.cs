﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateUserResource
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
        public string Phone { get; set; }
    }
}
