﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateRestaurantResource
    {
        public string Name { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
    }
}
