﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateDishResource
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}
