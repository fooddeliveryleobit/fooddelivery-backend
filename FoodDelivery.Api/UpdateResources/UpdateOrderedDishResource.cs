﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateOrderedDishResource
    {
        public int Count { get; set; }
    }
}
