﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateScoreResource
    {
        public double Value { get; set; }
    }
}
