﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateOrderResource
    {
        public int? DeliverId { get; set; }
        public OrderStatusEnum Status { get; set; }
    }
}
