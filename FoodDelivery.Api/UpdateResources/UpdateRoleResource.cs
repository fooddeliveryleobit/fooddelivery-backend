﻿

namespace FoodDelivery.Api.UpdateResource
{
    public class UpdateRoleResource
    {
        public bool IsDelivered { get; set; }
    }
}
