﻿

namespace FoodDelivery.Api.SaveResources
{
    public class SaveDishResource
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}
