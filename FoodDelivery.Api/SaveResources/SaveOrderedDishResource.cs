﻿

namespace FoodDelivery.Api.SaveResources
{
    public class SaveOrderedDishResource
    {
        public int DishId { get; set; }
        public int Count { get; set; }
    }
}
