﻿using FoodDelivery.Core.Models;


namespace FoodDelivery.Api.SaveResources
{
    public class SaveUserResource
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CityId { get; set; }
        public string Phone { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
        public RoleVariantEnum RoleId { get; set; }
    }
}
