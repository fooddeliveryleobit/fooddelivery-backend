﻿using FoodDelivery.Core.Models;

namespace FoodDelivery.Api.SaveResources
{
    public class SaveRoleResource
    {
        public RoleVariantEnum Id { get; set; }
    }
}
