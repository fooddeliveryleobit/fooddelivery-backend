﻿

using Microsoft.AspNetCore.Http;

namespace FoodDelivery.Api.SaveResources
{
    public class SaveRestaurantResource
    {
        public string Name { get; set; }
        public string CityId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Picture { get; set; }
    }
}
