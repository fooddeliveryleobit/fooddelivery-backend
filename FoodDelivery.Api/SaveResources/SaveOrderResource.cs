﻿using System;

namespace FoodDelivery.Api.SaveResources
{
    public class SaveOrderResource
    {
        public int CustomerId { get; set; }
        public int RestaurantId { get; set; }
        public DateTime Date { get; set; }
    }
}
