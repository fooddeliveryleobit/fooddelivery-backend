using AutoMapper;
using FoodDelivery.Core;
using FoodDelivery.Core.Services;
using FoodDelivery.Data;
using FoodDelivery.Service.Interfaces;
using FoodDelivery.Service.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using FoodDelivery.Core.Helpers;
using FoodDelivery.Core.PasswordHashing;
using FoodDelivery.Service.PasswordHashing;
using Microsoft.Extensions.Logging;
using FoodDelivery.Core.Monitoring;

namespace FoodDelivery.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "AllowOrigin";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                builder =>
                {
                    builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                      
                            ValidateIssuer = true,              
                            ValidIssuer = AuthOptions.ISSUER,                
                            ValidateAudience = true,                   
                            ValidAudience = AuthOptions.AUDIENCE,                  
                            ValidateLifetime = true,            
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),                     
                            ValidateIssuerSigningKey = true,
                        };
                    });


            services.AddApplicationInsightsTelemetry();


            services.AddMvc();


            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<FoodDeliveryDbContext>(options => options.UseSqlServer(connection, x => x.UseNetTopologySuite()));

            //var connection = @"Server =.\SQLEXPRESS; Database = FoodDelivery_Test; Trusted_Connection = True; ";
            //services.AddDbContext<FoodDeliveryDbContext>(options => options.UseSqlServer(connection, x => x.UseNetTopologySuite()));

            //var connection = @"Host=localhost;Database=FoodDelivery;Username=postgres;Password=7372";
            //services.AddDbContext<FoodDeliveryDbContext>(options => options.UseNpgsql(connection, x => x.UseNetTopologySuite()));



            services.AddControllers();

            #region Dependency injection
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IRestaurantService, RestaurantService>();
            services.AddTransient<IDishCategoryService, DishCategoryService>();
            services.AddTransient<IDishService, DishService>();
            services.AddTransient<IOrderedDishService, OrderedDishService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IScoreService, ScoreService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IPasswordHasher, PasswordHasher>();
            services.AddTransient<IImageService, ImageService>();
            services.AddTransient<ICustomAppInsightTelemetry, CustomAppInsightTelemetry>();
            #endregion

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "FoodDelivery_Test", Version = "v1" });
            });

            services.AddAutoMapper(typeof(Startup));



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
           
            app.UseHttpsRedirection();

            app.UseRouting();


            app.UseCors(MyAllowSpecificOrigins);

            
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            
            app.UseAuthentication();
            
            app.UseAuthorization();


            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "FoodDelivery_Test V1");
            });
           
        }
    }
}
